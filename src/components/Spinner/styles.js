import { StyleSheet } from 'react-native';

export const COLOR = {
  DARK: "#040207",
  PANTOME: '#ff6f61',
  LIGHT: "#ffffff",
  BLACK: "#000",
  GRAY: "#9A9A9A",
  LIGHT_GRAY: "#ffffff",
  DANGER: "#FF5370",
  RED: "#800000",
  WHITE: "#FFF",
  CYAN: "#09818F"
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.WHITE,
    marginTop: 20
  },
  content: {
    flex: 1
  },
  header: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    width: '100%',
    zIndex: 100
  },
  signin: {
    color: "red",
    fontWeight: 'bold',
    fontSize: 50,
    textAlign: 'left',
    margin: 10,

  },
  emailaddress: {
    fontSize: 22,
    textAlign: 'left',
    color: '#000000',
    marginTop: 10,
    marginLeft: 30,

  },

  emailAddressDialog: {
    fontSize: 18,
    textAlign: 'left',
    color: '#000000',
    marginTop: 10,
    marginLeft: 10,

  },

  completetext: {
    fontSize: 25,
    textAlign: 'left',
    color: '#000000',
    marginTop: 10,
    marginLeft: 30,


  },
  card: {
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    height: 150,
    flexDirection: 'row',
    alignItems: 'center',
    shadowOpacity: 1.5,
    shadowColor: COLOR.CYAN
  },

  personalInfoCard: {
    flex: 1,
    marginLeft: 20,
    marginRight: 20,
    marginTop: 10,
    minHeight: 200,
    flexDirection: 'row',
    alignItems: 'center',
    shadowOpacity: 1.5,
    shadowColor: COLOR.CYAN
  },
  dialogChangeInfoCard: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    height: 50,
    flexDirection: 'row',
    alignItems: 'center',
    shadowOpacity: 1.5,
    shadowColor: COLOR.CYAN
  },

  forgetpass: {
    marginTop: 20,
    backgroundColor: 'transparent'

  },
  carditemt: {
    textAlign: 'left',
    marginLeft: 10,
    color: '#000000',
  },
  textInput: {
    height: 50
  },
  buttonsignin: {
    justifyContent: 'center',
    backgroundColor: '#09818F',
    height: 60,
    width: '83%',
    marginTop: 10,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  buttonSave: {
    justifyContent: 'center',
    backgroundColor: '#09818F',
    height: 60,
    width: '83%',
    marginTop: 20,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },

  buttonYesDialog: {
    justifyContent: 'center',
    backgroundColor: '#09818F',
    height: 38,
    width: '48%',
    marginTop: 20,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },

  buttonCancelDialog: {
    justifyContent: 'center',
    borderWidth: 2,
    borderColor: '#09818F',
    height: 38,
    width: '48%',
    marginTop: 20,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },

  buttonrigster: {

    justifyContent: 'center',
    backgroundColor: '#ffffff',
    height: 60,
    width: 250,
    marginTop: 20,
    borderWidth: 2,
    borderColor: '#09818F',
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,

  },
  buttonrigsterfpass: {

    justifyContent: 'center',
    backgroundColor: '#ffffff',
    height: 50,
    width: 200,

    marginTop: 2,
    borderWidth: 2,
    borderColor: '#151B54',
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 0,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 30,

  },
  buttontextwhite: {
    fontSize: 15,
    color: "white",
    textAlign: 'center',
  },
  buttonSaveText: {
    fontSize: 15,
    color: "white",
    textAlign: 'center',
  },
  buttonCancelText: {
    fontSize: 15,
    color: "#09818F",
    textAlign: 'center',
  },
  buttontextblack: {
    fontSize: 15,
    color: "#09818F",
    textAlign: 'center',
  },
  centerview: {

    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,


  },
  centerviewl: {

    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 30,

  },
  centerviewfind: {

    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    marginTop: 5,

  },
  centerviewf: {

    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 30,


  },
  loginbuttonsignin: {
    justifyContent: 'center',
    backgroundColor: '#151B54',
    height: 60,
    marginLeft: 70,
    marginRight: 70,

    marginTop: 10,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 0,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 30,
  },
  loginbuttonsignup: {
    justifyContent: 'center',
    backgroundColor: '#ffffff',
    height: 60,
    marginLeft: 70,
    marginRight: 70,

    marginTop: 15,
    borderWidth: 2,
    borderColor: '#151B54',
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 0,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 30,
  },
  buttonhireCandidate: {
    justifyContent: 'center',
    backgroundColor: COLOR.RED,
    height: 60,
    width: 250,

    marginTop: 10,
    borderBottomLeftRadius: 30,
    borderBottomRightRadius: 0,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 30,

  },
  buttonjobsearch: {
    justifyContent: 'center',
    backgroundColor: 'red',
    height: 60,
    width: 250,

    marginTop: 10,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 0,


  },
});

export default styles;