import { StyleSheet,Dimensions } from 'react-native';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
export const COLOR = {

    BLUE: "#1e244a", // email box color
    RED: "#ea4335",
    YELLOW: "#fff673",
    LIGHTBLUE:"#0096da",
    GREEN:"#58d263"

};

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'#ffffff'

       
      },
      eventcontainer: {
        flex: 1,
        backgroundColor:'#F8F9F9'
      // marginLeft:10,
      // marginRight:10
      },
      cardView: {
        width: width-20,  
        height: height / 2 - 100 ,
        marginTop:30
      },
      eventImgSize: {
        minHeight: 120,
        width:width - 20
      },
      inputView: {
        width : width-20, 
       
        justifyContent:'center',
        height:60,
        //alignItems: 'center',
       
        borderWidth:1,
        paddingLeft:10,
        marginVertical:10
 
    },
    inputView1: {
      width : width-20, 
      justifyContent:'center',
      minHeight:100,
      //alignItems: 'center',
      borderWidth:1,
      paddingLeft:10,
      marginVertical:10

  },

  ViewImage: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: "5%",
 },

  textInputContainer: {
    // position: "absolute",
    alignSelf: "center",
    top: 20,
    backgroundColor: "transparent",
    height: (width * 0.15) + 20,
    width: width / 1.1,
},

  imageContainer: {
  // position: "absolute",
  alignSelf: "center",
  marginTop: "10%",
  justifyContent:'center',
  backgroundColor: "transparent",
  height: (width * 0.15) + 25,
  width: width / 1.1,
  borderWidth:.8,
  borderColor:"gray",
  borderRadius:5
},

imageContainer2: {
  // position: "absolute",
  alignSelf: "center",
  marginTop: "10%",
  justifyContent:'center',
  backgroundColor: "transparent",
  height: (width * 0.15) + 25,
  width: width / 1.1,
  borderWidth:.8,
  borderColor:"gray",
  borderRadius:5
},
imageContainer3: {
  // position: "absolute",
  alignSelf: "center",
  marginTop: "10%",
  justifyContent:'center',
  backgroundColor: "transparent",
  height: (width * 0.15) + 25,
  width: width / 1.1,
  borderWidth:.8,
  borderColor:"gray",
  borderRadius:5
},
// textInputContainer: {
//   // position: "absolute",
//   alignSelf: "center",
//   alignItems:"center",
//   top: "10%",
//   backgroundColor: "transparent",
//   height: (width * 0.15) + 20,
//   width: width / 1.1,
//   marginLeft:10,
//   marginRight:10
// },
  textInputStyle: {
    position: "absolute",
    top: 10,
    left:5,
    height: (width * 0.15),
    width: width / 1.1,
    borderColor: 'rgba(204,204,204,1)',
    borderWidth: 1,
    borderRadius: 5,
    padding: 10,
    fontSize: 18,
},

floatingText: {
    position: "absolute",
     backgroundColor: "white",
    color: "#012b72",
    fontWeight: "normal",
    fontSize: 16,
    // height: 20,
    top: -2,
    left: 10,
    paddingLeft: 3,
    paddingRight: 3,
},
floatingImage: {
  position: "absolute",
   backgroundColor: "white",
  color: "#012b72",
  height: 10,
  width:15,
  bottom: 0,
  right: 10,
  paddingLeft: 3,
  paddingRight: 3,
},

View3: {
  borderRadius: 7,
  height: "15%", alignSelf: 'center', top:5,
 width: width / 2.0, marginTop: "15%",
  justifyContent: 'center', backgroundColor: '#012b72'
},
View4: {
  borderRadius: 20,
  height: 40, alignSelf: 'center', 
 width: width / 2.0, marginTop: "10%", 
  justifyContent: 'center', backgroundColor: '#012b72', 
},

View5: {
  borderRadius: 20,
  height: 40, alignSelf: 'center', top:5,
 width: width / 2.0, marginTop: "10%",
  justifyContent: 'center', backgroundColor: '#00b9f5', 
},

SignText: {
  // marginTop:-5,
  fontSize: 20, color: '#fff',
  textAlign: 'center', 
  alignSelf:'center',
  marginTop:30,
},
 
   
});