import * as React from 'react'
import {
    StyleSheet,
    Text,
    Dimensions,
    View,
    Image,
    AsyncStorage, ImageBackground, TextInput, TouchableOpacity, SafeAreaView, AlertIOS, ToastAndroid, ScrollView, FlatList, Platform
} from 'react-native';
import { Footer, Card } from 'native-base';
import AddEventActions from '../../actions/ActionAddEvent'
import * as shape from 'd3-shape'
import { connect } from 'react-redux';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import styles, { COLOR } from './styles';
import { Actions } from 'react-native-router-flux';
import AppHeader from '../../components/AppHeader'
import DatePicker from 'react-native-datepicker'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import ImagePicker from 'react-native-image-picker';

var token = ""

class Single extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            vanue: props.cityname,
            lat: props.lat,
            long: props.lng,
            start_date: '',
            end_date: '',
            title: '',
            description: '',
            adhar_front_image:null,
            adhar_front_image_path:'',

            adhar_back_image:null,
            adhar_back_image_path:'',

            dlicencef_image:null,
            dlicencef_image_path:'',

            dlicenceb_image:null,
            dlicenceb_image_path:'',


            pimage:null,
            pimage_path:'',

            name:'',
            email:'',
            contact:'',
            id:''



        };
        console.log(props)
    };

    openSearchModal() {
        Actions.SearchPlaces("AddEvent")
    }
    selectPhotoTappedAadhar_f() {
        ImagePicker.showImagePicker((response) => {
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                console.log('source',source)
                console.log('source',response.uri)
                this.uploadImageAf(response.uri)

                this.setState({
                    adhar_front_image: response.uri
                });

                
               // this.Image()
            }
        });
    }
    selectPhotoTappedAdhar_b() {
        ImagePicker.showImagePicker((response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                this.uploadImageAb(response.uri )

                this.setState({
                    adhar_back_image: response.uri
                });
                console.log("hsdjyafsduya")
               // this.Image()
            }
        });
    }
    selectPhotoTappedDriving_l_f() {
        ImagePicker.showImagePicker((response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                this.uploadImagedrivel_f(response.uri )

                this.setState({
                    dlicencef_image: response.uri
                });
                console.log("hsdjyafsduya")
               // this.Image()
            }
        });
    }
    selectPhotoTappedDriving_l_b() {
        ImagePicker.showImagePicker((response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                this.uploadImagedrivel_b(response.uri )

                this.setState({
                    dlicenceb_image: response.uri
                });
                console.log("hsdjyafsduya")
               // this.Image()
            }
        });
    }
    selectPhotoTappedpimage() {
        ImagePicker.showImagePicker((response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                this.uploadImagePimage(response.uri )

                this.setState({
                    pimage: response.uri
                });
                console.log("hsdjyafsduya")
            }
        });
    }

    validate = () => {
        if (this.state.name == "" || this.state.name == null || this.state.name == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter name'); },
                android: () => { ToastAndroid.show('Please enter name', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.email == "" || this.state.email == null || this.state.email == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter email'); },
                android: () => { ToastAndroid.show('Please enter email', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.contact == "" || this.state.contact == null || this.state.contact == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter contact'); },
                android: () => { ToastAndroid.show('Please enter contact', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.adhar_front_image_path == "" || this.state.adhar_front_image_path == null || this.state.adhar_front_image_path == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload Aadhar Card'); },
                android: () => { ToastAndroid.show('Please upload Aadhar Card', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.dlicenceb_image_path == "" || this.state.dlicenceb_image_path == null || this.state.dlicenceb_image_path == undefined) {
            Platform.select({ 
                ios: () => { AlertIOS.alert('Please upload DrivingLicense'); },
                android: () => { ToastAndroid.show('Please upload DrivingLicense', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.dlicencef_image_path == "" || this.state.dlicencef_image_path == null || this.state.dlicencef_image_path == undefined) {
            Platform.select({ 
                ios: () => { AlertIOS.alert('Please upload DrivingLicense'); },
                android: () => { ToastAndroid.show('Please DrivingLicense', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.pimage_path == "" || this.state.pimage_path == null || this.state.pimage_path == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload Photo'); },
                android: () => { ToastAndroid.show('Please upload Photo', ToastAndroid.SHORT); }
            })();
            return false;
        }
       
        else {
            this.applyForPAN()
        }
    }
    async uploadImageAf(image) {
        console.log('imagef',image)

        var file = {
            uri: image,
            type: 'image/png',
            name: 'profile_image'
          };
          var data = new FormData();
          data.append('file', file);
         
   
        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/upload_documents?type=aadhar_card', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body:data
        }).then((res) => res.json())
            .then(res => {
                console.log('',res)
                this.setState({
                    adhar_front_image_path: res.path
                });
                //console.log('path',adhar_front_image_path)

            })
            .catch((e) => {
                console.log(e)
                Platform.select({ 
                    ios: () => { AlertIOS.alert('Uploading Failed'); },
                    android: () => { ToastAndroid.show('Uploading Failed', ToastAndroid.SHORT); }
                })();
            });

    };
    async uploadImageAb(image) {
        console.log('imagef',image)

        var file = {
            uri: image,
            type: 'image/png',
            name: 'profile_image'
          };
          var data = new FormData();
          data.append('file', file);
         
   
        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/upload_documents?type=aadhar_card', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body:data
        }).then((res) => res.json())
            .then(res => {
                console.log('',res)
                this.setState({
                    adhar_back_image_path: res.path
                });
               
            })
            .catch((e) => {
                console.log(e)
                Platform.select({ 
                    ios: () => { AlertIOS.alert('Uploading Failed'); },
                    android: () => { ToastAndroid.show('Uploading Failed', ToastAndroid.SHORT); }
                })();
            });

    };
    async uploadImagedrivel_f(image) {
        console.log('imagef',image)

        var file = {
            uri: image,
            type: 'image/png',
            name: 'profile_image'
          };
          var data = new FormData();
          data.append('file', file);
         
   
        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/upload_documents?type=driving_licence', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body:data
        }).then((res) => res.json())
            .then(res => {
                console.log('',res)
                this.setState({
                    dlicencef_image_path: res.path
                });
               
            })
            .catch((e) => {
                console.log(e)
                Platform.select({ 
                    ios: () => { AlertIOS.alert('Uploading Failed'); },
                    android: () => { ToastAndroid.show('Uploading Failed', ToastAndroid.SHORT); }
                })();
            });

    };
    async uploadImagedrivel_b(image) {
        console.log('imagef',image)

        var file = {
            uri: image,
            type: 'image/png',
            name: 'profile_image'
          };
          var data = new FormData();
          data.append('file', file);
         
   
        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/upload_documents?type=driving_licence', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body:data
        }).then((res) => res.json())
            .then(res => {
                console.log('',res)
                this.setState({
                    dlicenceb_image_path: res.path
                });
               
            })
            .catch((e) => {
                console.log(e)
                Platform.select({ 
                    ios: () => { AlertIOS.alert('Uploading Failed'); },
                    android: () => { ToastAndroid.show('Uploading Failed', ToastAndroid.SHORT); }
                })();
            });

    };
    async uploadImagePimage(image) {
        console.log('imagef',image)

        var file = {
            uri: image,
            type: 'image/png',
            name: 'profile_image'
          };
          var data = new FormData();
          data.append('file', file);
         
   
        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/upload_documents?type=image', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body:data
        }).then((res) => res.json())
            .then(res => {
                console.log('',res)
                this.setState({
                    pimage_path: res.path
                });
               
            })
            .catch((e) => {
                console.log(e)
                Platform.select({ 
                    ios: () => { AlertIOS.alert('Uploading Failed'); },
                    android: () => { ToastAndroid.show('Uploading Failed', ToastAndroid.SHORT); }
                })();
            });

    };

    AddEvent() {
        const params = { vanue, lat, long, start_date, end_date, title, description } = this.state
        this.props.AddeventApi(params);
        Actions.Home()
    }
    componentWillMount(){
        // this.setState({id:AsyncStorage.getItem('@agent_id')})
        this.getData()
        
      }
      getData = async () => {
          try {
            const value = await AsyncStorage.getItem('@storage_Key')
            if(value !== null) {
              // value previously stored
              console.log('aid',value)
              this.setState({
                id: value
            });            }
          } catch(e) {
            // error reading value
            console.log('error',e)
          }
        }  
    async applyForPAN() {
        console.log(this.state.id,this.state.adhar_front_image_path,this.state.dlicencef_image_path,
            this.state.dlicenceb_image_path,this.state.pimage_path,this.state.adhar_back_image_path,this.state.name,
            this.state.email,this.state.contact )
        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/applyForPAN', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "agent_id": this.state.id,
                "documents": [{
                    "aadhar_front": this.state.adhar_front_image_path,
                    "aadhar_back": this.state.adhar_back_image_path,
                    "d_licence_front" : this.state.dlicencef_image_path,
                    "d_licence_back"  :  this.state.dlicenceb_image_path,
                    "image"			  :  this.state.pimage_path,
                     "name": this.state.name,
                    "email": this.state.email,
                    "mobile":this.state.contact
                }
              
            ]

            })
        }).then((res) => res.json())
            .then(res => {
                console.log(res)
                if (res.result === "true" || res.result === true) {
                    Platform.select({
                        ios: () => { AlertIOS.alert(res.msg); },
                        android: () => { ToastAndroid.show(res.msg, ToastAndroid.SHORT); }
                    })();
                    this.props.navigation.navigate("Home")
                   
                } else {
                    alert("error")
                    
                }
            })
            .catch((e) => {
                console.log(e)
            });

    };

    render() {

        return (
            <SafeAreaView style={styles.container}>
                <AppHeader
                    goBack={() => this.props.navigation.goBack()}
                    addevent={() => this.validate()}
                    leftImg={require('./../../components/Images/assets/icn_back.png')}
                    // rightImg={require('./../../components/Images/checked.png')}
                    headerTitle="Single"
                />

                <ScrollView bounces={false} style={{ top: 0, }}>

                    <KeyboardAwareScrollView
                        style={{ backgroundColor: 'transparent' }}
                        resetScrollToCoords={{ x: 0, y: 0 }}
                        contentContainerStyle={{ flex: 1 }}
                        scrollEnabled={true}
                        bounces={false}
                    >
                        <View style={{ flex: 1, }}>

                            <View style={styles.textInputContainer} >

                                <TextInput style={styles.textInputStyle}
                                    placeholder={'Enter Your Name'}
                                    placeholderTextColor={'gray'}
                                    returnKeyType={'default'}
                                    autoCapitalize={'none'}
                                    underlineColorAndroid={'transparent'}
                                    onChangeText={(name) => this.setState({ name })}
                                    editable={this.state.TextInputDisableStatus}
                                    value={this.state.name}
                                />
                                <Text style={styles.floatingText}>
                                    Enter Name
                            </Text>
                            </View>

                            <View style={styles.textInputContainer} >

                                <TextInput style={styles.textInputStyle}
                                    placeholder={'Enter Your Email'}
                                    placeholderTextColor={'gray'}
                                    returnKeyType={'default'}
                                    autoCapitalize={'none'}
                                    underlineColorAndroid={'transparent'}
                                    onChangeText={(email) => this.setState({ email })}
                                    editable={this.state.TextInputDisableStatus}
                                    value={this.state.email}
                                />
                                <Text style={styles.floatingText}>
                                    Enter Email
                            </Text>
                            </View>

                            <View style={styles.textInputContainer} >

                                <TextInput style={styles.textInputStyle}
                                    placeholder={'Enter Your Contact'}
                                    placeholderTextColor={'gray'}
                                    returnKeyType={'default'}
                                    autoCapitalize={'none'}
                                    underlineColorAndroid={'transparent'}
                                    onChangeText={(contact) => this.setState({ contact })}
                                    editable={this.state.TextInputDisableStatus}
                                    value={this.state.contact}
                                />
                                <Text style={styles.floatingText}>
                                    Enter Contact
                                    </Text>

                            </View>

                            <View style={styles.imageContainer} >

                                <Text style={styles.floatingText}>
                                    Upload Your AddharCard
                                    </Text>

                                <View style={{ flexDirection: 'row' }}>

                                    <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop: "2%", position: 'absolute', left: 20 }} >
                                      

                                        {
                                            this.state.adhar_front_image
                                                ? <TouchableOpacity onPress={() =>  this.selectPhotoTappedAadhar_f()}>
                                                    <Image source={{ uri: this.state.adhar_front_image }} style={{ width: 100, height: 50, borderRadius: 2 }} />
                                                </TouchableOpacity>
                                                : <TouchableOpacity style={styles.icon} onPress={() =>  this.selectPhotoTappedAadhar_f()}>
                                                    <Image
                                                        resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                        // source={{ uri: this.state.sendImageUrl }} />
                                                        source={require('./../../Images/camra.png')} />
                                                </TouchableOpacity>
                                        }


                                    </View>

                                    <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop: "2%", position: 'absolute', right: 100 }} >
                                    {
                                            this.state.adhar_back_image
                                                ? <TouchableOpacity onPress={() =>  this.selectPhotoTappedAdhar_b()}>
                                                    <Image source={{ uri: this.state.adhar_back_image }} style={{ width: 100, height: 50, borderRadius: 2,marginRight:20 }} />
                                                </TouchableOpacity>
                                                : <TouchableOpacity style={styles.icon} onPress={() =>  this.selectPhotoTappedAdhar_b()}>
                                                    <Image
                                                        resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                        // source={{ uri: this.state.sendImageUrl }} />
                                                        source={require('./../../Images/camra.png')} />
                                                </TouchableOpacity>
                                        }
                                    </View>

                                </View>

                            </View>

                            <View style={styles.imageContainer2} >

                                <Text style={styles.floatingText}>
                                 Upload Your DrivingLicense
                                    </Text>

                                <View style={{ flexDirection: 'row' }}>

                                    <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop: "2%", position: 'absolute', left: 20 }} >
                                      

                                        {
                                            this.state.dlicencef_image
                                                ? <TouchableOpacity onPress={() =>  this.selectPhotoTappedDriving_l_f()}>
                                                    <Image source={{ uri: this.state.dlicencef_image }} style={{ width: 100, height: 50, borderRadius: 2 }} />
                                                </TouchableOpacity>
                                                : <TouchableOpacity style={styles.icon} onPress={() =>  this.selectPhotoTappedDriving_l_f()}>
                                                    <Image
                                                        resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                        // source={{ uri: this.state.sendImageUrl }} />
                                                        source={require('./../../Images/camra.png')} />
                                                </TouchableOpacity>
                                        }


                                    </View>

                                    <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop: "2%", position: 'absolute', right: 100 }} >
                                    {
                                            this.state.dlicenceb_image
                                                ? <TouchableOpacity onPress={() =>  this.selectPhotoTappedDriving_l_b()}>
                                                    <Image source={{ uri: this.state.dlicenceb_image }} style={{ width: 100, height: 50, borderRadius: 2,marginRight:20 }} />
                                                </TouchableOpacity>
                                                : <TouchableOpacity style={styles.icon} onPress={() =>  this.selectPhotoTappedDriving_l_b()}>
                                                    <Image
                                                        resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                        // source={{ uri: this.state.sendImageUrl }} />
                                                        source={require('./../../Images/camra.png')} />
                                                </TouchableOpacity>
                                        }
                                    </View>

                                </View>

                            </View>

                            <View style={styles.imageContainer3} >

                                <Text style={styles.floatingText}>
                                    Upload Your Photo
                                </Text>
                                <View style={{ height: 15, width: 10, justifyContent: 'center', alignSelf: 'center', marginTop: "7%", alignItems: 'center' }} >
                                {
                                            this.state.pimage
                                                ? <TouchableOpacity onPress={() =>  this.selectPhotoTappedpimage()}>
                                                    <Image source={{ uri: this.state.pimage }} style={{ width: 100, height: 50, borderRadius: 2,marginBottom:6 }} />
                                                </TouchableOpacity>
                                                : <TouchableOpacity style={styles.icon} onPress={() =>  this.selectPhotoTappedpimage()}>
                                                    <Image
                                                        resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                        // source={{ uri: this.state.sendImageUrl }} />
                                                        source={require('./../../Images/camra.png')} />
                                                </TouchableOpacity>
                                        }
                                </View>
                            </View>


                            <View style={styles.View4}>
                                             <TouchableOpacity onPress={() => { this.validate() }}>
                                                <Text style={styles.SignText}>Upload</Text>
                                             </TouchableOpacity>
                                         </View>

                                         <View style={styles.View5}>
                                             <TouchableOpacity onPress={() => { this.props.navigation.navigate("Home") }}>
                                                 <Text style={styles.SignText}>Proceed To Pay</Text>
                                             </TouchableOpacity>
                                         </View>
                            <View style={{ height: 20 }} />
                        </View>
                    </KeyboardAwareScrollView>
                </ScrollView>
            </SafeAreaView>


        );
    }
}


export default (Single)
//     render() {

//         return (
//             <SafeAreaView style={styles.container}>
//                 <AppHeader
//                     goBack={() => this.props.navigation.goBack()}
//                     addevent={() => this.validate()}
//                     leftImg={require('./../../components/Images/assets/icn_back.png')}
//                     // rightImg={require('./../../components/Images/checked.png')}
//                     headerTitle="Single"
//                 />

//                 <ScrollView bounces={false} style={{ top: 0, }}>

//                                 <KeyboardAwareScrollView
//                                     style={{ backgroundColor: 'transparent' }}
//                                     resetScrollToCoords={{ x: 0, y: 0 }}
//                                     contentContainerStyle={{ flex: 1 }}
//                                     scrollEnabled={true}
//                                     bounces={false}
//                                 >
//                                     <View style={{ flex: 1,  }}>

//                                         <View style={styles.textInputContainer} >

//                                             <TextInput style={styles.textInputStyle}
//                                                 placeholder={'Enter Your Name'}
//                                                 placeholderTextColor={'gray'}
//                                                 returnKeyType={'default'}
//                                                 autoCapitalize={'none'}
//                                                 underlineColorAndroid={'transparent'}
//                                                 onChangeText={(email) => this.setState({ name })}
//                                                 editable={this.state.TextInputDisableStatus}
//                                                 value={this.state.name}
//                                             />
//                                             <Text style={styles.floatingText}>
//                                                 Enter Name
//                             </Text>
//                                         </View>

//                                         <View style={styles.textInputContainer} >

//                                             <TextInput style={styles.textInputStyle}
//                                                 placeholder={'Enter Your Email'}
//                                                 placeholderTextColor={'gray'}
//                                                 returnKeyType={'default'}
//                                                 autoCapitalize={'none'}
//                                                 underlineColorAndroid={'transparent'}
//                                                 onChangeText={(email) => this.setState({ email })}
//                                                 editable={this.state.TextInputDisableStatus}
//                                                 value={this.state.email}
//                                             />
//                                             <Text style={styles.floatingText}>
//                                                 Enter Email
//                             </Text>
//                                         </View>

//                                         <View style={styles.textInputContainer} >

//                                             <TextInput style={styles.textInputStyle}
//                                                 placeholder={'Enter Your Contact'}
//                                                 placeholderTextColor={'gray'}
//                                                 returnKeyType={'default'}
//                                                 autoCapitalize={'none'}
//                                                 underlineColorAndroid={'transparent'}
//                                                 onChangeText={(email) => this.setState({ contact })}
//                                                 editable={this.state.TextInputDisableStatus}
//                                                 value={this.state.contact}
//                                             />
//                                             <Text style={styles.floatingText}>
//                                                 Enter Contact
//                                     </Text>

//                                         </View>


//                                         <View style={styles.imageContainer} >

//                                             <Text style={styles.floatingText}>
//                                                 Upload Your AddharCard
//                                     </Text>
                                           
//                                          <View style={{flexDirection:'row',  }}>
                                    
//                                     <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop:"5%", position:'absolute', left:20 }} >
//                                         <TouchableOpacity onPress={() => { this.selectPhotoTapped() }}>
//                                                         <Image
//                                                             resizeMode="center" style={{alignSelf:'center', tintColor:'gray',}}
//                                                             // source={{ uri: this.state.sendImageUrl }} />
//                                                             source={require('./../../Images/camra.png')} />
//                                          </TouchableOpacity>
//                                     </View>
    
//                                     <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop:"5%", position:'absolute', right:20 }} >
//                                         <TouchableOpacity onPress={() => { this.selectPhotoTapped() }}>
//                                                         <Image
//                                                             resizeMode="center" style={{alignSelf:'center', tintColor:'gray',}}
//                                                             // source={{ uri: this.state.sendImageUrl }} />
//                                                             source={require('./../../Images/camra.png')} />
//                                          </TouchableOpacity>
//                                     </View>
    
//                                    </View>
                                           
//                                         </View>

//                                         <View style={styles.imageContainer2} >

//                                             <Text style={styles.floatingText}>
//                                                 Upload Your DrivingLicense
//                                     </Text>

//                                     <View style={{flexDirection:'row',  }}>
                                    
//                                     <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop:"5%", position:'absolute', left:20 }} >
//                                         <TouchableOpacity onPress={() => { this.selectPhotoTapped() }}>
//                                                         <Image
//                                                             resizeMode="center" style={{alignSelf:'center', tintColor:'gray',}}
//                                                             // source={{ uri: this.state.sendImageUrl }} />
//                                                             source={require('./../../Images/camra.png')} />
//                                          </TouchableOpacity>
//                                     </View>
    
//                                     <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop:"5%", position:'absolute', right:20 }} >
//                                         <TouchableOpacity onPress={() => { this.selectPhotoTapped() }}>
//                                                         <Image
//                                                             resizeMode="center" style={{alignSelf:'center', tintColor:'gray',}}
//                                                             // source={{ uri: this.state.sendImageUrl }} />
//                                                             source={require('./../../Images/camra.png')} />
//                                          </TouchableOpacity>
//                                     </View>
    
//                                    </View> 
//                                         </View>

//                                         <View style={styles.imageContainer3} >

//                                             <Text style={styles.floatingText}>
//                                                 Upload Your Photo
//                                 </Text>
//                                 <View style={{ height: 15, width: 10, justifyContent: 'center', alignSelf:'center',  marginTop:"5%",alignItems:'center' }} >
//                                 <TouchableOpacity onPress={() => { this.selectPhotoTapped() }}>
//                                                     <Image
//                                                         resizeMode="center" style={{alignSelf:'center', tintColor:'gray',}}
//                                                         // source={{ uri: this.state.sendImageUrl }} />
//                                                         source={require('./../../Images/camra.png')} />
//                                                 </TouchableOpacity>
//                                             </View> 
//                                         </View>


//                                         <View style={styles.View4}>
//                                             <TouchableOpacity onPress={() => { this.props.navigation.navigate("Home") }}>
//                                                 <Text style={styles.SignText}>Upload</Text>
//                                             </TouchableOpacity>
//                                         </View>

//                                         <View style={styles.View5}>
//                                             <TouchableOpacity onPress={() => { this.props.navigation.navigate("Home") }}>
//                                                 <Text style={styles.SignText}>Proceed To Pay</Text>
//                                             </TouchableOpacity>
//                                         </View>
//                                         <View style={{ height: 20 }} />
//                                     </View>
//                                 </KeyboardAwareScrollView>
//                             </ScrollView>
//             </SafeAreaView>
//         );
//     }
// }


//export default (Single)