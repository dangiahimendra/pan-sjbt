import * as React from 'react'
import {
    StyleSheet,
    Text,
    Dimensions,
    View,
    Image,
    CheckBox,
    
    AsyncStorage, ImageBackground, TextInput, TouchableOpacity, SafeAreaView, AlertIOS, ToastAndroid, ScrollView, FlatList, Platform
} from 'react-native';

import { Footer, Card, Content, Tab, Tabs, Header } from 'native-base';

import styles, { COLOR } from './styles';
import { Actions } from 'react-native-router-flux';
import AppHeader from '../../components/AppHeader'

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import ImagePicker from 'react-native-image-picker';

var token = ""
// import NEW from './../containers/NEW'        
// import Duplicate from './../containers/Duplicate'        

class PAN extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            vanue: props.cityname,
            lat: props.lat,
            long: props.lng,
            start_date: '',
            end_date: '',
            title: '',
            description: '',
            adhar_front_image: null,
            adhar_front_image_path: '',

            adhar_back_image: null,
            adhar_back_image_path: '',

            dlicencef_image: null,
            signature_image_path: '',

            dlicenceb_image: null,
            other_image_path: '',
            pimage: null,
            pimage_path: '',
            name: '',
            email: '',
            contact: '',
            id: '',
            count: '',
            formsMulti: [],
            formsMultiforDublicate: [],

            uploadimg: false,
            addf: false,
            addfd: false,
            checked_d:false,
            checked_p:false
        };
        this.addForms = this.addForms.bind(this);
        this.selectPhotoTapped = this.selectPhotoTapped.bind(this);
        this.uploadImage = this.uploadImage.bind(this);
        // this.fillForm1 = this.fillForm1.bind(this)

    };
    getData1 = async () => {
        try {
            const value = await AsyncStorage.getItem('@storage_Email')
            this.setState({
                email: 'xyz@gmail.com'
            })
            if (value !== null) {
                // value previously stored
                console.log('email', value)
                this.setState({
                    email: 'xyz@gmail.com'
                })
            }
        } catch (e) {
            // error reading value
            console.log('error', e)
        }
    }

    openSearchModal() {
        Actions.SearchPlaces("AddEvent")
    }

    selectPhotoTapped(uploadType, checkValue, index) {
        ImagePicker.showImagePicker((response) => {
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                console.log('source', source)
                console.log('source', response.uri)

                this.uploadImage(response.uri, uploadType, checkValue, index)


            }
        });
    }
    async uploadImage(image, utype, cvalue, index) {
        console.log('imagef', image)

        var file = {
            uri: image,
            type: 'image/png',
            name: 'profile_image'
        };
        var data = new FormData();
        data.append('file', file);


        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/upload_documents?type=' + utype, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body: data
        }).then((res) => res.json())
            .then(res => {
                console.log('', res)
                if (cvalue == 'aadhar_f') {

                    this.state.formsMulti[index].aadhar_front = res.data
                    this.setState({
                        uploadimg: true
                    });

                    console.log('adharf', this.state.formsMulti[index].aadhar_front)
                }
                if (cvalue == 'aadhar_b') {
                    this.state.formsMulti[index].aadhar_back = res.data
                    this.setState({
                        uploadimg: true
                    });
                    console.log('adharb', this.state.formsMulti[index].aadhar_back)

                }
                if (cvalue == 'photo') {
                    this.state.formsMulti[index].image = res.data
                    this.setState({
                        uploadimg: true
                    });
                    console.log('photo', this.state.formsMulti[index].image)

                }
                if (cvalue == 'sign') {
                    this.state.formsMulti[index].signature = res.data
                    this.setState({
                        uploadimg: true
                    });
                    console.log('sign', this.state.formsMulti[index].signature)

                }
                if (cvalue == 'other_f') {
                    this.state.formsMulti[index].others_front = res.data
                    this.setState({
                        uploadimg: true
                    });
                    console.log('other_f', this.state.formsMulti[index].others_front)

                }
                if (cvalue == 'other_b') {
                    this.state.formsMulti[index].others_back = res.data
                    this.setState({
                        uploadimg: true
                    });
                    console.log('other_b', this.state.formsMulti[index].others_back)

                }
                if (cvalue == 'd_aadhar_f') {
                    this.state.formsMultiforDublicate[index].aadhar_front = res.data
                    this.setState({
                        uploadimg: true
                    });
                    console.log('d_aadhar_f', this.state.formsMultiforDublicate[index].aadhar_front)

                }
                if (cvalue == 'd_aadhar_b') {
                    this.state.formsMultiforDublicate[index].aadhar_back = res.data
                    this.setState({
                        uploadimg: true
                    });
                    console.log('d_aadhar_b', this.state.formsMultiforDublicate[index].aadhar_back)


                }
                if (cvalue == 'd_photo') {
                    this.state.formsMultiforDublicate[index].image = res.data
                    this.setState({
                        uploadimg: true
                    });
                    console.log('d_photo', this.state.formsMultiforDublicate[index].image)


                }
                if (cvalue == 'd_sign') {
                    this.state.formsMultiforDublicate[index].signature = res.data
                    this.setState({
                        uploadimg: true
                    });
                    console.log('d_sign', this.state.formsMultiforDublicate[index].signature)


                }
                if (cvalue == 'd_other_f') {
                    this.state.formsMultiforDublicate[index].others_front = res.data
                    this.setState({
                        uploadimg: true
                    });
                    console.log('d_other_f', this.state.formsMultiforDublicate[index].others_front)

                }
                if (cvalue == 'd_other_b') {
                    this.state.formsMultiforDublicate[index].others_back = res.data
                    this.setState({
                        uploadimg: true
                    });
                    console.log('d_other_b', this.state.formsMultiforDublicate[index].others_back)

                }

                Platform.select({
                    ios: () => { AlertIOS.alert('Upload Successful'); },
                    android: () => { ToastAndroid.show('Upload Successful', ToastAndroid.SHORT); }
                })();
            })
            .catch((e) => {
                console.log(e)
                Platform.select({
                    ios: () => { AlertIOS.alert('Upload Failed'); },
                    android: () => { ToastAndroid.show('Upload Failed', ToastAndroid.SHORT); }
                })();
            });

    };



    validate_next = () => {
        // if (this.state.name == "" || this.state.name == null || this.state.name == undefined) {
        //     Platform.select({
        //         ios: () => { AlertIOS.alert('Please enter name'); },
        //         android: () => { ToastAndroid.show('Please enter name', ToastAndroid.SHORT); }
        //     })();
        //     return false;
        // }
        if (this.state.email == "" || this.state.email == null || this.state.email == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter email'); },
                android: () => { ToastAndroid.show('Please enter email', ToastAndroid.SHORT); }
            })();
            return false;
        }
        
        if (this.state.contact == "" || this.state.contact == null || this.state.contact == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter contact'); },
                android: () => { ToastAndroid.show('Please enter contact', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.adhar_front_image_path == "" || this.state.adhar_front_image_path == null || this.state.adhar_front_image_path == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload Aadhar Card'); },
                android: () => { ToastAndroid.show('Please upload Aadhar Card', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.adhar_back_image_path == "" || this.state.adhar_back_image_path == null || this.state.adhar_back_image_path == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload Aadhar Card'); },
                android: () => { ToastAndroid.show('Please upload Aadhar Card', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.signature_image_path == "" || this.state.signature_image_path == null || this.state.signature_image_path == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload Aadhar Card'); },
                android: () => { ToastAndroid.show('Please upload Aadhar Card', ToastAndroid.SHORT); }
            })();
            return false;
        }


        if (this.state.pimage_path == "" || this.state.pimage_path == null || this.state.pimage_path == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload Photo'); },
                android: () => { ToastAndroid.show('Please upload Photo', ToastAndroid.SHORT); }
            })();
            return false;
        }
        var text = this.state.email;
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(text) === false) {
            Platform.select({
                ios: () => { AlertIOS.alert('Email is Not Correct'); },
                android: () => { ToastAndroid.show('Email is Not Correct', ToastAndroid.SHORT); }
            })();
            return false;
        }
        var val = this.state.mobile_number
        let regn = /^\d{10}$/;

        if (regn.test(val) === false) {
            // value is ok, use it

            Platform.select({
                ios: () => { AlertIOS.alert('Please enter mobile number'); },
                android: () => { ToastAndroid.show('Please enter mobile number', ToastAndroid.SHORT); }
            })
                ();
            return false;
            // ();
            // number.focus()
            // return false
        }

        else {
            this.applyForPAN()
        }
    }





    AddEvent() {
        const params = { vanue, lat, long, start_date, end_date, title, description } = this.state
        this.props.AddeventApi(params);
        Actions.Home()
    }

    componentWillMount() {

        this.getData();
        this.addForms();
        this.addformsMultiforDublicate()
        this.getData1();


    }
    addForms() {
        let formData = {
            "pan_number"        :"",
            "aadhar_front": "",
            "aadhar_back": "",
            "others_front"       : "",
            "others_back"       : "",
            "signature": "",
            "image": "",
            "name": "",
            "email": "",
            "mobile": ""
        }

        this.state.formsMulti.push(formData);


        console.log('datalist', this.state.formsMulti)


    }
    addformsMultiforDublicate() {
        let formData = {
            "pan_number"        :"",
            "aadhar_front": "",
            "aadhar_back": "",
            "others_front"       : "",
            "others_back"       : "",
            "signature": "",
            "image": "",
            "name": "",
            "email": "",
            "mobile": ""
        }

        this.state.formsMultiforDublicate.push(formData);


        console.log('datalist duplicate', this.state.formsMultiforDublicate)


    }
    addForms2() {
        console.log('addf2', this.state.formsMulti.email);

        let formData = {
            "pan_number"        :"",
            "aadhar_front": "",
            "aadhar_back": "",
            "others_front"       : "",
            "others_back"       : "",
            "signature": "",
            "image": "",
            "name": "",
            "email": "",
            "mobile": ""
        }

        this.state.formsMulti.push(formData);

        Platform.select({
            ios: () => { AlertIOS.alert('New form created'); },
            android: () => { ToastAndroid.show('New form created', ToastAndroid.SHORT); }
        })();
        this.setState({
            addf: true
        });
        console.log('datalist', this.state.formsMulti)


    }
    addformsMultiforDublicate2() {
        console.log('addf2', this.state.formsMulti.email);

        let formData = {
            "pan_number":"",
            "aadhar_front": "",
            "aadhar_back": "",
            "others_front"       : "",
            "others_back"       : "",
            "signature": "",
            "image": "",
            "name": "",
            "email": "",
            "mobile": ""
        }

        this.state.formsMultiforDublicate.push(formData);

        Platform.select({
            ios: () => { AlertIOS.alert('New form created'); },
            android: () => { ToastAndroid.show('New form created', ToastAndroid.SHORT); }
        })();
        this.setState({
            addfd: true
        });
        console.log('datalist duplicate', this.state.formsMultiforDublicate)


    }
    fun(index) {
        var checked = this.state.formsMulti;
        var values = checked.indexOf(index)
        checked.splice(values, 1);
        this.setState({formsMulti: checked});
        console.log('remove',this.state.formsMulti)
      }
      fund(index) {
        var checked = this.state.formsMultiforDublicate;
        var values = checked.indexOf(index)
        checked.splice(values, 1);
        this.setState({formsMultiforDublicate: checked});
        console.log('remove',this.state.formsMultiforDublicate)
      }
    removePeople(e) {
        var array = this.state.formsMulti; // make a separate copy of the array
        var index = array.indexOf(e.target.value)
        if (index !== -1) {
          array.splice(index, 1);
          this.setState({formsMulti: array});
        }
        console.log('datalist remove', this.state.formsMulti)

      }
    getData = async () => {
        try {
            const value = await AsyncStorage.getItem('@storage_Key')
            if (value !== null) {
                // value previously stored
                console.log('aid', value)
                this.setState({
                    id: value
                });
            }
        } catch (e) {
            // error reading value
            console.log('error', e)
        }
    }
    fillForm1(value, staticvaiue, index) {
        console.log('fillform', value + " " + staticvaiue + " " + index);

        this.state.formsMulti[index].email = value;



    }
    fillForm2(value, staticvaiue, index) {
        console.log(value + " " + index);

        this.state.formsMulti[index].mobile = value;

        console.log(this.state.formsMulti);


    }

    validate_pan = () => {
        if(this.state.formsMulti.length!=0){
        console.log('length', this.state.formsMulti.length)
        const count=this.state.formsMulti.length-1
        console.log('length', count)


        
        if (this.state.formsMulti[count].mobile == "" || this.state.formsMulti[count].mobile == null || this.state.formsMulti[count].mobile == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter contact'); },
                android: () => { ToastAndroid.show('Please enter contact', ToastAndroid.SHORT); }
            })();
            return false;
        }
        // const regp = /^[0]?[789]\d{9}$/;
        // if (regp.test(this.state.formsMulti[count].mobile) === false ) {
        //     Platform.select({
        //         ios: () => { AlertIOS.alert('Please enter valid contact'); },
        //         android: () => { ToastAndroid.show('Please enter valid contact', ToastAndroid.SHORT); }
        //     })();
        //     return false;
        // }
        
      
        if (this.state.formsMulti[count].email == "" || this.state.formsMulti[count].email == null ||this.state.formsMulti[count].email == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter email'); },
                android: () => { ToastAndroid.show('Please enter email', ToastAndroid.SHORT); }
            })();
            return false;
        }
        let regep = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (regep.test(this.state.formsMulti[count].email) === false) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter valid email'); },
                android: () => { ToastAndroid.show('Please enter valid email', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.formsMulti[count].aadhar_front == "" || this.state.formsMulti[count].aadhar_front == null || this.state.formsMulti[count].aadhar_front == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload Aadhar Card (front)'); },
                android: () => { ToastAndroid.show('Please upload Aadhar Card (front)', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.formsMulti[count].image == "" || this.state.formsMulti[count].image == null || this.state.formsMulti[count].image== undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload photo'); },
                android: () => { ToastAndroid.show('Please upload photo', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.formsMulti[count].signature == "" || this.state.formsMulti[count].signature == null || this.state.formsMulti[count].signature == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload signature'); },
                android: () => { ToastAndroid.show('Please upload signature', ToastAndroid.SHORT); }
            })();
            return false;
        }

        else {
            this.addForms2()
        }
    }
    }
    validate_pan_next = () => {
        if(this.state.formsMulti.length!=0){
        console.log('length', this.state.formsMulti.length)
        const count=this.state.formsMulti.length-1
        console.log('length', count)


        
        if (this.state.formsMulti[count].mobile == "" || this.state.formsMulti[count].mobile == null || this.state.formsMulti[count].mobile == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter contact'); },
                android: () => { ToastAndroid.show('Please enter contact', ToastAndroid.SHORT); }
            })();
            return false;
        }
        // const regp = /^[0]?[789]\d{9}$/;
        // if (regp.test(this.state.formsMulti[count].mobile) === false ) {
        //     Platform.select({
        //         ios: () => { AlertIOS.alert('Please enter valid contact'); },
        //         android: () => { ToastAndroid.show('Please enter valid contact', ToastAndroid.SHORT); }
        //     })();
        //     return false;
        // }
        
      
        if (this.state.formsMulti[count].email == "" || this.state.formsMulti[count].email == null ||this.state.formsMulti[count].email == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter email'); },
                android: () => { ToastAndroid.show('Please enter email', ToastAndroid.SHORT); }
            })();
            return false;
        }
        let regep = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (regep.test(this.state.formsMulti[count].email) === false) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter valid email'); },
                android: () => { ToastAndroid.show('Please enter valid email', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.formsMulti[count].aadhar_front == "" || this.state.formsMulti[count].aadhar_front == null || this.state.formsMulti[count].aadhar_front == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload Aadhar Card (front)'); },
                android: () => { ToastAndroid.show('Please upload Aadhar Card (front)', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.formsMulti[count].image == "" || this.state.formsMulti[count].image == null || this.state.formsMulti[count].image== undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload photo'); },
                android: () => { ToastAndroid.show('Please upload photo', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.formsMulti[count].signature == "" || this.state.formsMulti[count].signature == null || this.state.formsMulti[count].signature == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload signature'); },
                android: () => { ToastAndroid.show('Please upload signature', ToastAndroid.SHORT); }
            })();
            return false;
        }

        else {
            this.termaccept_pan()
        }
    }
    }
    validate_dublicate = () => {
        if(this.state.formsMultiforDublicate.length!=0){
        const countp=this.state.formsMultiforDublicate.length-1
        console.log('length', countp)
        if (this.state.formsMultiforDublicate[countp].pan_number == "" || this.state.formsMultiforDublicate[countp].pan_number == null ||this.state.formsMultiforDublicate[countp].pan_number == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter PAN number'); },
                android: () => { ToastAndroid.show('Please PAN number', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.formsMultiforDublicate[countp].mobile == "" || this.state.formsMultiforDublicate[countp].mobile == null || this.state.formsMultiforDublicate[countp].mobile == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter contact'); },
                android: () => { ToastAndroid.show('Please enter contact', ToastAndroid.SHORT); }
            })();
            return false;
        }
       
        // const regd = /^[0]?[789]\d{9}$/;
        // if (regd.test(this.state.formsMultiforDublicate[count].mobile) === false ) {
        //     Platform.select({
        //         ios: () => { AlertIOS.alert('Please enter valid contact'); },
        //         android: () => { ToastAndroid.show('Please enter valid contact', ToastAndroid.SHORT); }
        //     })();
        //     return false;
        // }

        if (this.state.formsMultiforDublicate[countp].email == "" || this.state.formsMultiforDublicate[countp].email == null ||this.state.formsMultiforDublicate[countp].email == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter email'); },
                android: () => { ToastAndroid.show('Please enter email', ToastAndroid.SHORT); }
            })();
            return false;
        }
        let reged = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reged.test(this.state.formsMultiforDublicate[count].email) === false) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter valid email'); },
                android: () => { ToastAndroid.show('Please enter valid email', ToastAndroid.SHORT); }
            })();
            return false;
        }
        
       
        if (this.state.formsMultiforDublicate[countp].aadhar_front == "" || this.state.formsMultiforDublicate[countp].aadhar_front == null || this.state.formsMultiforDublicate[countp].aadhar_front == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload Aadhar Card (front)'); },
                android: () => { ToastAndroid.show('Please upload Aadhar Card (front)', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.formsMultiforDublicate[countp].image == "" || this.state.formsMultiforDublicate[countp].image == null || this.state.formsMultiforDublicate[countp].image== undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload photo'); },
                android: () => { ToastAndroid.show('Please photo', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.formsMultiforDublicate[countp].signature == "" || this.state.formsMultiforDublicate[countp].signature == null || this.state.formsMultiforDublicate[countp].signature == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload signature'); },
                android: () => { ToastAndroid.show('Please upload signature', ToastAndroid.SHORT); }
            })();
            return false;
        }

        else {
            this.addformsMultiforDublicate2()
        }
    }
    }
    validate_dublicate_next = () => {
        if(this.state.formsMultiforDublicate.length!=0){
        const countp=this.state.formsMultiforDublicate.length-1
        console.log('length', countp)
        if (this.state.formsMultiforDublicate[countp].pan_number == "" || this.state.formsMultiforDublicate[countp].pan_number == null ||this.state.formsMultiforDublicate[countp].pan_number == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter PAN number'); },
                android: () => { ToastAndroid.show('Please PAN number', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.formsMultiforDublicate[countp].mobile == "" || this.state.formsMultiforDublicate[countp].mobile == null || this.state.formsMultiforDublicate[countp].mobile == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter contact'); },
                android: () => { ToastAndroid.show('Please enter contact', ToastAndroid.SHORT); }
            })();
            return false;
        }
       
        // const regd = /^[0]?[789]\d{9}$/;
        // if (regd.test(this.state.formsMultiforDublicate[count].mobile) === false ) {
        //     Platform.select({
        //         ios: () => { AlertIOS.alert('Please enter valid contact'); },
        //         android: () => { ToastAndroid.show('Please enter valid contact', ToastAndroid.SHORT); }
        //     })();
        //     return false;
        // }

        if (this.state.formsMultiforDublicate[countp].email == "" || this.state.formsMultiforDublicate[countp].email == null ||this.state.formsMultiforDublicate[countp].email == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter email'); },
                android: () => { ToastAndroid.show('Please enter email', ToastAndroid.SHORT); }
            })();
            return false;
        }
        let reged = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reged.test(this.state.formsMultiforDublicate[count].email) === false) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter valid email'); },
                android: () => { ToastAndroid.show('Please enter valid email', ToastAndroid.SHORT); }
            })();
            return false;
        }
        
       
        if (this.state.formsMultiforDublicate[countp].aadhar_front == "" || this.state.formsMultiforDublicate[countp].aadhar_front == null || this.state.formsMultiforDublicate[countp].aadhar_front == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload Aadhar Card (front)'); },
                android: () => { ToastAndroid.show('Please upload Aadhar Card (front)', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.formsMultiforDublicate[countp].image == "" || this.state.formsMultiforDublicate[countp].image == null || this.state.formsMultiforDublicate[countp].image== undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload photo'); },
                android: () => { ToastAndroid.show('Please photo', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.formsMultiforDublicate[countp].signature == "" || this.state.formsMultiforDublicate[countp].signature == null || this.state.formsMultiforDublicate[countp].signature == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload signature'); },
                android: () => { ToastAndroid.show('Please upload signature', ToastAndroid.SHORT); }
            })();
            return false;
        }

        else {
            this.termaccept_dublicate()
        }
    }
    }

    async applyForPAN() {
        console.log(this.state.id, this.state.adhar_front_image_path, this.state.dlicencef_image_path,
            this.state.dlicenceb_image_path, this.state.pimage_path, this.state.adhar_back_image_path, this.state.name,
            this.state.email, this.state.contact)
        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/applyForPAN', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "agent_id": this.state.id,
                "amount": "410",
                "comment": "Demo demo demo demo",
                "documents": [{
                    "aadhar_front": this.state.adhar_front_image_path,
                    "aadhar_back": this.state.adhar_back_image_path,
                    "d_licence_front": this.state.other_image_path,
                    "d_licence_back": "",
                    "signature": this.state.signature_image_path,
                    "image": this.state.pimage_path,
                    "name": this.state.name,
                    "email": this.state.email,
                    "mobile": this.state.contact
                }

                ]

            })
        }).then((res) => res.json())
            .then(res => {
                console.log(res)
                if (res.result === "true" || res.result === true) {
                    Platform.select({
                        ios: () => { AlertIOS.alert(res.msg); },
                        android: () => { ToastAndroid.show(res.msg, ToastAndroid.SHORT); }
                    })();
                    this.props.navigation.navigate("Home")

                } else {
                    alert("error")

                }
            })
            .catch((e) => {
                console.log(e)
            });

    };
    termaccept_pan(){
        if(this.state.checked_p){
            this.props.navigation.navigate("Payment", { data: this.state.formsMulti })
            this.setState({
                checked_p: false
            });
            console.log('panc',this.state.checked_p)
        }else{
            Platform.select({
                ios: () => { AlertIOS.alert('Please accept terms and conditions.'); },
                android: () => { ToastAndroid.show('Please accept terms and conditions.', ToastAndroid.SHORT); }
            })(); 
        }
    }
    termaccept_dublicate(){
        if(this.state.checked_d){
            this.props.navigation.navigate("Payment", { data: this.state.formsMultiforDublicate })
            this.setState({
                checked_d: false
            });
            console.log('dup',this.state.checked_d)

        }else{
            Platform.select({
                ios: () => { AlertIOS.alert('Please accept terms and conditions.'); },
                android: () => { ToastAndroid.show('Please accept terms and conditions.', ToastAndroid.SHORT); }
            })(); 
        }
    }
    render() {
        console.log('uploadimg', this.state.uploadimg)
        console.log('addf', this.state.addf)
        console.log('addfd', this.state.addfd)

        return (
            <SafeAreaView style={styles.container}>
                <AppHeader
                    goBack={() => this.props.navigation.goBack()}
                    addevent={() => this.validate()}
                    leftImg={require('./../../components/Images/assets/icn_back.png')}
                    // rightImg={require('./../../components/Images/checked.png')}
                    headerTitle="PAN"
                />

                <View style={styles.eventcontainer} >

                    <Tabs tabBarUnderlineStyle={{
                        backgroundColor: '#1e244a',
                        height: 0.8
                    }}>

                        <Tab heading="NEW[49A]"
                            activeTextStyle={{ color: '#fff', fontWeight: 'bold' }}
                            textStyle={{ color: 'gray', }}
                            tabStyle={{ backgroundColor: "#fff" }}
                            activeTabStyle={{ backgroundColor: "#012b72" }}>
                            <ScrollView bounces={false} style={{ top: 0, }}>
                                <FlatList
                                    data={this.state.formsMulti}
                                    extraData={this.state}
                                    renderItem={({ item, index }) => (

                                        <KeyboardAwareScrollView
                                            style={{ backgroundColor: 'transparent' }}
                                            resetScrollToCoords={{ x: 0, y: 0 }}
                                            contentContainerStyle={{ flex: 1 }}
                                            scrollEnabled={true}
                                            bounces={false}
                                        >

                                            <View style={{ flex: 1, borderWidth: 1, borderColor: '#000000', margin: 6 }}>
                                                <View style={{alignSelf: 'flex-end',padding:6}}>
                                            <TouchableOpacity style={styles.icon} onPress={() => this.fun( index)}>
                                                                        <Image
                                                                            resizeMode="center" style={{  tintColor: 'black', height: 20, width: 20 }}
                                                                            source={require('./../../Images/cancel1.png')} />
                                                                    </TouchableOpacity>
                                                                    </View>
                                                <Text style={{ color: 'black', justifyContent: 'center', alignItems: 'center', textAlign: 'center', marginTop: 5 }}> FORM {index + 1}</Text>
                                               
                                                {/* <View style={styles.textInputContainer} >

                                                <TextInput style={styles.textInputStyle}
                                                    placeholder={'Enter Your Name'}
                                                    placeholderTextColor={'gray'}
                                                    returnKeyType={'default'}
                                                    autoCapitalize={'none'}
                                                    underlineColorAndroid={'transparent'}
                                                    onChangeText={(value) => this.fillForm1(value, index)}
                                                    value={this.state.formsMulti.name}
                                                    editable={this.state.TextInputDisableStatus}
                                                />
                                                <Text style={styles.floatingText}>
                                                    Enter Name
                            </Text>
                                            </View> */}
                                                <View style={styles.textInputContainer} >

                                                    <TextInput style={styles.textInputStyle}
                                                        placeholder={'Enter Your Contact'}
                                                        placeholderTextColor={'gray'}
                                                        returnKeyType={'default'}
                                                        keyboardType='phone-pad'
                                                        maxLength={10}
                                                        autoCapitalize={'none'}
                                                        underlineColorAndroid={'transparent'}
                                                        onChangeText={(mobile) => { this.state.formsMulti[index].mobile = mobile }}
                                                        editable={this.state.TextInputDisableStatus}
                                                        value={this.state.formsMulti.mobile}
                                                    />
                                                    <Text style={styles.floatingText}>
                                                        Enter Contact*
</Text>

                                                </View>
                                                <View style={styles.textInputContainer} >

                                                    <TextInput style={styles.textInputStyle}
                                                        placeholder={'Enter Your Email'}
                                                        placeholderTextColor={'gray'}
                                                        returnKeyType={'default'}
                                                        autoCapitalize={'none'}
                                                        underlineColorAndroid={'transparent'}
                                                        onChangeText={(email) => { this.state.formsMulti[index].email = email }}

                                                        editable={true}
                                                        //defaultValue={this.state.email=this.state.formsMulti[index].email}
                                                         value={this.state.formsMulti.email}

                                                    />
                                                    <Text style={styles.floatingText}>
                                                        Enter Email*
                            </Text>
                                                </View>



                                                <View style={styles.imageContainer} >

                                                    <Text style={styles.floatingText1}>
                                                        Upload Your AadharCard*
                                    </Text>

                                                    <View style={{ flexDirection: 'row' }}>

                                                        <View style={{ height: 15, width: 100, justifyContent: 'center', marginTop: "2%", position: 'absolute', left: 5 }} >

                                                            {
                                                                this.state.formsMulti[index].aadhar_front

                                                                    ?
                                                                    <TouchableOpacity onPress={() => this.selectPhotoTapped("aadhar_card", "aadhar_f", index)}>
                                                                        <Image source={{ uri: this.state.formsMulti[index].aadhar_front }} style={{ width: 100, height: 50, borderRadius: 2 }} />
                                                                    </TouchableOpacity>
                                                                    : <TouchableOpacity style={styles.icon} onPress={() => this.selectPhotoTapped("aadhar_card", "aadhar_f", index)}>
                                                                        <Image
                                                                            resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', height: 50, width: 50 }}
                                                                            source={require('./../../Images/camra.png')} />
                                                                    </TouchableOpacity>
                                                            }

                                                        </View>
                                                        <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop: "8%", position: 'absolute', left: 37 }} >

                                                            <Text style={{ width: 60, color: 'black', }}>Front</Text>
                                                        </View>


                                                        <View style={{ height: 15, width: 100, justifyContent: 'center', marginTop: "2%", position: 'absolute', right: 5 }} >
                                                            {
                                                                this.state.formsMulti[index].aadhar_back
                                                                    ? <TouchableOpacity onPress={() => this.selectPhotoTapped("aadhar_card", "aadhar_b", index)}>
                                                                        <Image source={{ uri: this.state.formsMulti[index].aadhar_back }} style={{ width: 100, height: 50, borderRadius: 2 }} />
                                                                    </TouchableOpacity>
                                                                    : <TouchableOpacity style={styles.icon} onPress={() => this.selectPhotoTapped("aadhar_card", "aadhar_b", index)}>
                                                                        <Image
                                                                            resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                                            // source={{ uri: this.state.sendImageUrl }} />
                                                                            source={require('./../../Images/camra.png')} />
                                                                    </TouchableOpacity>
                                                            }

                                                        </View>
                                                        <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop: "8%", position: 'absolute', right: 90 }} >

                                                            <Text style={{ width: 100, color: 'black', }}>Back [optional]</Text>
                                                        </View>


                                                    </View>

                                                </View>



                                                <View style={styles.imageContainer3} >

                                                    <Text style={styles.floatingText1}>
                                                        Upload Your Photo*
                                </Text>
                                                    <View style={{ height: 15, width: 10, justifyContent: 'center', alignSelf: 'center', marginTop: "7%", alignItems: 'center' }} >
                                                        {
                                                            this.state.formsMulti[index].image
                                                                ? <TouchableOpacity onPress={() => this.selectPhotoTapped("image", "photo", index)}>
                                                                    <Image source={{ uri: this.state.formsMulti[index].image }} style={{ width: 100, height: 50, borderRadius: 2, marginBottom: 6 }} />
                                                                </TouchableOpacity>
                                                                : <TouchableOpacity style={styles.icon} onPress={() => this.selectPhotoTapped("image", "photo", index)}>
                                                                    <Image
                                                                        resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                                        // source={{ uri: this.state.sendImageUrl }} />
                                                                        source={require('./../../Images/camra.png')} />
                                                                </TouchableOpacity>
                                                        }
                                                    </View>
                                                </View>
                                                <View style={styles.imageContainer3} >

                                                    <Text style={styles.floatingText1}>
                                                        Upload Your Signature*
                                </Text>
                                                    <View style={{ height: 15, width: 10, justifyContent: 'center', alignSelf: 'center', marginTop: "7%", alignItems: 'center' }} >
                                                        {
                                                            this.state.formsMulti[index].signature
                                                                ? <TouchableOpacity onPress={() => this.selectPhotoTapped("signature", "sign", index)}>
                                                                    <Image source={{ uri: this.state.formsMulti[index].signature }} style={{ width: 100, height: 50, borderRadius: 2, marginBottom: 6 }} />
                                                                </TouchableOpacity>
                                                                : <TouchableOpacity style={styles.icon} onPress={() => this.selectPhotoTapped("signature", "sign", index)}>
                                                                    <Image
                                                                        resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                                        // source={{ uri: this.state.sendImageUrl }} />
                                                                        source={require('./../../Images/camra.png')} />
                                                                </TouchableOpacity>
                                                        }
                                                    </View>
                                                </View>
                                                <View style={styles.imageContainer3} >

                                                    <Text style={styles.floatingText1}>
                                                        Upload Other Documents [optional]
                                </Text>
                                                    <View style={{ flexDirection: 'row' }}>

                                                        <View style={{ height: 15, width: 100, justifyContent: 'center', marginTop: "2%", position: 'absolute', left: 5 }} >

                                                            {
                                                                this.state.formsMulti[index].others_front

                                                                    ?
                                                                    <TouchableOpacity onPress={() => this.selectPhotoTapped("others_front", "other_f", index)}>
                                                                        <Image source={{ uri: this.state.formsMulti[index].others_front }} style={{ width: 100, height: 50, borderRadius: 2 }} />
                                                                    </TouchableOpacity>
                                                                    : <TouchableOpacity style={styles.icon} onPress={() => this.selectPhotoTapped("others_front", "other_f", index)}>
                                                                        <Image
                                                                            resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', height: 50, width: 50 }}
                                                                            source={require('./../../Images/camra.png')} />
                                                                    </TouchableOpacity>
                                                            }

                                                        </View>
                                                        <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop: "8%", position: 'absolute', left: 37 }} >

                                                            <Text style={{ width: 60, color: 'black', }}>Front</Text>
                                                        </View>


                                                        <View style={{ height: 15, width: 100, justifyContent: 'center', marginTop: "2%", position: 'absolute', right: 5 }} >
                                                            {
                                                                this.state.formsMulti[index].others_back
                                                                    ? <TouchableOpacity onPress={() => this.selectPhotoTapped("others_back", "other_b", index)}>
                                                                        <Image source={{ uri: this.state.formsMulti[index].others_back }} style={{ width: 100, height: 50, borderRadius: 2 }} />
                                                                    </TouchableOpacity>
                                                                    : <TouchableOpacity style={styles.icon} onPress={() => this.selectPhotoTapped("others_back", "other_b", index)}>
                                                                        <Image
                                                                            resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                                            // source={{ uri: this.state.sendImageUrl }} />
                                                                            source={require('./../../Images/camra.png')} />
                                                                    </TouchableOpacity>
                                                            }

                                                        </View>
                                                        <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop: "8%", position: 'absolute', right: 60 }} >

                                                            <Text style={{ width: 60, color: 'black', }}>Back</Text>
                                                        </View>


                                                    </View>

                                                </View>



                                                <View style={{ height: 20 }} />
                                            </View>
                                        </KeyboardAwareScrollView>
                                    )}
                                    keyExtractor={(item, index) => index}
                                />
                                <View style={{ flexDirection: 'row' }}>
                            <CheckBox
                                value={this.state.checked_p}
                                onValueChange={() => this.setState({ checked_p: !this.state.checked_p })}
                            />
                            <Text style={{ marginTop: 5 ,color:'black'}}> Accept Terms & Conditions</Text>
                        </View>

                                <View style={styles.View4}>
                                    <TouchableOpacity onPress={() => { this.validate_pan() }}>
                                        <Text style={styles.SignText}>Add more form</Text>
                                    </TouchableOpacity>
                                </View>

                                {/* <View style={styles.View4}>
                        <TouchableOpacity onPress={() => { this.applyForPAN() }}>
                            <Text style={styles.SignText}>Upload</Text>
                        </TouchableOpacity>
                    </View> */}

                                <View style={styles.View5}>
                                    <TouchableOpacity onPress={() => { this.validate_pan_next() }}>
                                        <Text style={styles.SignText}>Next</Text>
                                    </TouchableOpacity>
                                </View>
                            </ScrollView>
                        </Tab>

                        <Tab heading="Change Correction[Duplicate]"
                            activeTextStyle={{ color: '#fff', fontWeight: 'bold' }}
                            textStyle={{ color: 'gray', }}
                            tabStyle={{ backgroundColor: "#fff" }}
                            activeTabStyle={{ backgroundColor: "#012b72" }}>
                            <ScrollView bounces={false} style={{ top: 0, }}>
                                <FlatList
                                    data={this.state.formsMultiforDublicate}
                                    extraData={this.state}
                                    renderItem={({ item, index }) => (

                                        <KeyboardAwareScrollView
                                            style={{ backgroundColor: 'transparent' }}
                                            resetScrollToCoords={{ x: 0, y: 0 }}
                                            contentContainerStyle={{ flex: 1 }}
                                            scrollEnabled={true}
                                            bounces={false}
                                        >

                                            <View style={{ flex: 1, borderWidth: 1, borderColor: '#000000', margin: 6 }}>
                                            <View style={{alignSelf: 'flex-end',padding:6}}>
                                            <TouchableOpacity style={styles.icon} onPress={() => this.fund( index)}>
                                                                        <Image
                                                                            resizeMode="center" style={{  tintColor: 'black', height: 20, width: 20 }}
                                                                            source={require('./../../Images/cancel1.png')} />
                                                                    </TouchableOpacity>
                                                                    </View>
                                                <Text style={{ color: 'black', justifyContent: 'center', alignItems: 'center', textAlign: 'center', marginTop: 5 }}> FORM {index + 1}</Text>

                                                <View style={styles.textInputContainer} >

                                                <TextInput style={styles.textInputStyle}
                                                    placeholder={'Enter PAN No.'}
                                                    placeholderTextColor={'gray'}
                                                    returnKeyType={'default'}
                                                    autoCapitalize={'none'}
                                                    underlineColorAndroid={'transparent'}
                                                    onChangeText={(pan_number) => { this.state.formsMultiforDublicate[index].pan_number = pan_number }}
                                                    value={this.state.formsMulti.pan_number}
                                                    editable={this.state.TextInputDisableStatus}
                                                />
                                                <Text style={styles.floatingText}>
                                                    Enter PAN No.
                            </Text>
                                            </View>
                                                <View style={styles.textInputContainer} >

                                                    <TextInput style={styles.textInputStyle}
                                                        placeholder={'Enter Your Contact'}
                                                        placeholderTextColor={'gray'}
                                                        returnKeyType={'default'}
                                                        autoCapitalize={'none'}
                                                        keyboardType='phone-pad'
                                                        maxLength={10}
                                                        underlineColorAndroid={'transparent'}
                                                        onChangeText={(mobile) => { this.state.formsMultiforDublicate[index].mobile = mobile }}
                                                        editable={this.state.TextInputDisableStatus}
                                                        value={this.state.formsMultiforDublicate.mobile}
                                                    />
                                                    <Text style={styles.floatingText}>
                                                        Enter Contact*
</Text>

                                                </View>
                                                <View style={styles.textInputContainer} >

                                                    <TextInput style={styles.textInputStyle}
                                                        placeholder={'Enter Your Email'}
                                                        placeholderTextColor={'gray'}
                                                        returnKeyType={'default'}
                                                        autoCapitalize={'none'}
                                                        underlineColorAndroid={'transparent'}
                                                        onChangeText={(email) => { this.state.formsMultiforDublicate[index].email = email }}

                                                        editable={this.state.TextInputDisableStatus}
                                                        value={this.state.formsMultiforDublicate.email}
                                                    />
                                                    <Text style={styles.floatingText}>
                                                        Enter Email*
                            </Text>
                                                </View>



                                                <View style={styles.imageContainer} >

                                                    <Text style={styles.floatingText1}>
                                                        Upload Your AadharCard*
                                    </Text>

                                                    <View style={{ flexDirection: 'row' }}>
                                                        {/* <View style={{ flexDirection: 'row' }}> */}

                                                        <View style={{ height: 15, width: 100, justifyContent: 'center', marginTop: "2%", position: 'absolute', left: 5 }} >

                                                            {
                                                                this.state.formsMultiforDublicate[index].aadhar_front

                                                                    ?
                                                                    <TouchableOpacity onPress={() => this.selectPhotoTapped("aadhar_card", "d_aadhar_f", index)}>
                                                                        <Image source={{ uri: this.state.formsMultiforDublicate[index].aadhar_front }} style={{ width: 100, height: 50, borderRadius: 2 }} />
                                                                    </TouchableOpacity>
                                                                    : <TouchableOpacity style={styles.icon} onPress={() => this.selectPhotoTapped("aadhar_card", "d_aadhar_f", index)}>
                                                                        <Image
                                                                            resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                                            source={require('./../../Images/camra.png')} />
                                                                    </TouchableOpacity>
                                                            }

                                                        </View>
                                                        {/* <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop: "8%", position: 'absolute', left: 60 }} >
                                                   {this.state.uploadimg?
<Text style={{ width: 60, color: 'black', }}>upload sucess</Text>
:
<Text style={{ width: 60, color: 'black', }}>upload faild</Text>
                                                   }
</View>
</View> */}
                                                        <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop: "8%", position: 'absolute', left: 37 }} >

                                                            <Text style={{ width: 60, color: 'black', }}>Front</Text>
                                                        </View>


                                                        <View style={{ height: 15, width: 100, justifyContent: 'center', marginTop: "2%", position: 'absolute', right: 5 }} >
                                                            {
                                                                this.state.formsMultiforDublicate[index].aadhar_back
                                                                    ? <TouchableOpacity onPress={() => this.selectPhotoTapped("aadhar_card", "d_aadhar_b", index)}>
                                                                        <Image source={{ uri: this.state.formsMultiforDublicate[index].aadhar_back }} style={{ width: 100, height: 50, borderRadius: 2, marginRight: 20 }} />
                                                                    </TouchableOpacity>
                                                                    : <TouchableOpacity style={styles.icon} onPress={() => this.selectPhotoTapped("aadhar_card", "d_aadhar_b", index)}>
                                                                        <Image
                                                                            resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                                            // source={{ uri: this.state.sendImageUrl }} />
                                                                            source={require('./../../Images/camra.png')} />
                                                                    </TouchableOpacity>
                                                            }

                                                        </View>
                                                        <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop: "8%", position: 'absolute', right:90 }} >

                                                            <Text style={{ width: 110, color: 'black', }}>Back [optional]</Text>
                                                        </View>


                                                    </View>

                                                </View>



                                                <View style={styles.imageContainer3} >

                                                    <Text style={styles.floatingText1}>
                                                        Upload Your Photo*
                                </Text>
                                                    <View style={{ height: 15, width: 10, justifyContent: 'center', alignSelf: 'center', marginTop: "7%", alignItems: 'center' }} >
                                                        {
                                                            this.state.formsMultiforDublicate[index].image
                                                                ? <TouchableOpacity onPress={() => this.selectPhotoTapped("image", "d_photo", index)}>
                                                                    <Image source={{ uri: this.state.formsMultiforDublicate[index].image }} style={{ width: 100, height: 50, borderRadius: 2, marginBottom: 6 }} />
                                                                </TouchableOpacity>
                                                                : <TouchableOpacity style={styles.icon} onPress={() => this.selectPhotoTapped("image", "d_photo", index)}>
                                                                    <Image
                                                                        resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                                        // source={{ uri: this.state.sendImageUrl }} />
                                                                        source={require('./../../Images/camra.png')} />
                                                                </TouchableOpacity>
                                                        }
                                                    </View>
                                                </View>
                                                <View style={styles.imageContainer3} >

                                                    <Text style={styles.floatingText1}>
                                                        Upload Your Signature*
                                </Text>
                                                    <View style={{ height: 15, width: 10, justifyContent: 'center', alignSelf: 'center', marginTop: "7%", alignItems: 'center' }} >
                                                        {
                                                            this.state.formsMultiforDublicate[index].signature
                                                                ? <TouchableOpacity onPress={() => this.selectPhotoTapped("signature", "d_sign", index)}>
                                                                    <Image source={{ uri: this.state.formsMultiforDublicate[index].signature }} style={{ width: 100, height: 50, borderRadius: 2, marginBottom: 6 }} />
                                                                </TouchableOpacity>
                                                                : <TouchableOpacity style={styles.icon} onPress={() => this.selectPhotoTapped("signature", "d_sign", index)}>
                                                                    <Image
                                                                        resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                                        // source={{ uri: this.state.sendImageUrl }} />
                                                                        source={require('./../../Images/camra.png')} />
                                                                </TouchableOpacity>
                                                        }
                                                    </View>
                                                </View>
                                                <View style={styles.imageContainer3} >

                                                    <Text style={styles.floatingText1}>
                                                        Upload Other Documents [optional]
                                </Text>
                                                    <View style={{ flexDirection: 'row' }}>

                                                        <View style={{ height: 15, width: 100, justifyContent: 'center', marginTop: "2%", position: 'absolute', left: 5 }} >

                                                            {
                                                                this.state.formsMultiforDublicate[index].others_front

                                                                    ?
                                                                    <TouchableOpacity onPress={() => this.selectPhotoTapped("others_front", "d_other_f", index)}>
                                                                        <Image source={{ uri: this.state.formsMultiforDublicate[index].others_front }} style={{ width: 100, height: 50, borderRadius: 2 }} />
                                                                    </TouchableOpacity>
                                                                    : <TouchableOpacity style={styles.icon} onPress={() => this.selectPhotoTapped("others_front", "d_other_f", index)}>
                                                                        <Image
                                                                            resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', height: 50, width: 50 }}
                                                                            source={require('./../../Images/camra.png')} />
                                                                    </TouchableOpacity>
                                                            }

                                                        </View>
                                                        <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop: "8%", position: 'absolute', left: 37 }} >

                                                            <Text style={{ width: 60, color: 'black', }}>Front</Text>
                                                        </View>


                                                        <View style={{ height: 15, width: 100, justifyContent: 'center', marginTop: "2%", position: 'absolute', right: 5 }} >
                                                            {
                                                                this.state.formsMultiforDublicate[index].others_back
                                                                    ? <TouchableOpacity onPress={() => this.selectPhotoTapped("others_back", "d_other_b", index)}>
                                                                        <Image source={{ uri: this.state.formsMultiforDublicate[index].others_back }} style={{ width: 100, height: 50, borderRadius: 2 }} />
                                                                    </TouchableOpacity>
                                                                    : <TouchableOpacity style={styles.icon} onPress={() => this.selectPhotoTapped("others_back", "d_other_b", index)}>
                                                                        <Image
                                                                            resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                                            // source={{ uri: this.state.sendImageUrl }} />
                                                                            source={require('./../../Images/camra.png')} />
                                                                    </TouchableOpacity>
                                                            }

                                                        </View>
                                                        <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop: "8%", position: 'absolute', right: 60 }} >

                                                            <Text style={{ width: 60, color: 'black', }}>Back</Text>
                                                        </View>


                                                    </View>

                                                </View>


                                                <View style={{ height: 20 }} />
                                            </View>
                                        </KeyboardAwareScrollView>
                                    )}
                                    keyExtractor={(item, index) => index}
                                />
                                <View style={{ flexDirection: 'row' }}>
                            <CheckBox
                                value={this.state.checked_d}
                                onValueChange={() => this.setState({ checked_d: !this.state.checked_d })}
                            />
                            <Text style={{ marginTop: 5 ,color:'black'}}> Accept Terms & Conditions</Text>
                        </View>

                                <View style={styles.View4}>
                                    <TouchableOpacity onPress={() => { this.validate_dublicate() }}>
                                        <Text style={styles.SignText}>Add more form</Text>
                                    </TouchableOpacity>
                                </View>

                                {/* <View style={styles.View4}>
                        <TouchableOpacity onPress={() => { this.applyForPAN() }}>
                            <Text style={styles.SignText}>Upload</Text>
                        </TouchableOpacity>
                    </View> */}

                                <View style={styles.View5}>
                                    <TouchableOpacity onPress={() => { this.validate_dublicate_next() }}>
                                        <Text style={styles.SignText}>Next</Text>
                                    </TouchableOpacity>
                                </View>
                            </ScrollView>
                        </Tab>

                    </Tabs>
                </View>

            </SafeAreaView>
        );
    }
}


export default (PAN)