import React, { Component } from 'react';
import { WebView, SafeAreaView, StatusBar, BackHandler, View } from 'react-native';
import styles from './styles';
import AppHeader from '../../components/AppHeader'

export default class Paymentweb extends Component {

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', () => {
            this.props.navigation.goBack();
            return true
        });
    }

    render() {

        return (

            <SafeAreaView style={styles.container}>
                <StatusBar
                    hidden={false}
                    barStyle={'light-content'}
                    animated={true}
                />

<AppHeader
                    goBack={() => this.props.navigation.goBack()}
                    addevent={() => this.validate()}
                    leftImg={require('./../../components/Images/assets/icn_back.png')}
                    // rightImg={require('./../../components/Images/checked.png')}
                    headerTitle="Payment"
                />
                <View style={styles.container}>
                    <WebView
                        style={styles.WebViewStyle}
                        source={{ uri: 'https://paytm.com/' }}
                        javaScriptEnabled={true}
                        domStorageEnabled={true} />
                </View>

            </SafeAreaView >

        );
    }
}