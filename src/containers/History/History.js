import * as React from 'react'
import {
    StyleSheet,
    Text,
    Dimensions,
    View,
    Image,
     ImageBackground, TextInput, TouchableOpacity, SafeAreaView, AlertIOS, ToastAndroid, ScrollView, FlatList, Platform
} from 'react-native';
import { Footer, Card, Content, Tab, Tabs, Header } from 'native-base';
import AddEventActions from '../../actions/ActionAddEvent'
import * as shape from 'd3-shape'
import { connect } from 'react-redux';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import styles, { COLOR } from './styles';
import { Actions } from 'react-native-router-flux';
import AppHeader from '../../components/AppHeader'
import DatePicker from 'react-native-datepicker'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import AsyncStorage from '@react-native-community/async-storage';


var token = ""    

class History extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            vanue: props.cityname,
            lat: props.lat,
            long: props.lng,
            start_date: '',
            end_date: '',
            title: '',
            description: '',
            pan_list:[],
            itr_list:[],
            id:''
        };
       // console.log(props)
       this.getData = this.getData.bind(this);
       this.history = this.history.bind(this);


    };
   componentWillMount(){
      // this.setState({id:AsyncStorage.getItem('@agent_id')})
      console.log('componentWillMount')

      this.getData()
     // this.history(1)
      
    }
    getData = async () => {
        try {
          const value = await AsyncStorage.getItem('@storage_Key')
          if(value !== null) {
            // value previously stored
            console.log('aid',value)
            this.history(value)
          }
        } catch(e) {
          // error reading value
          console.log('error',e)
        }
      }  
    openSearchModal() {
        Actions.SearchPlaces("AddEvent")
    }
    async history(id) {
       // console.log(this.state.code)
        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/agentHistory', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "agent_id" : id

            })
        }).then((res) => res.json())
            .then(res => {
                console.log(res)
                if (res.result === "true" || res.result === true) {
                        console.log('hdata',res.data.PAN_list)
                        this.setState({
                            pan_list: res.data.PAN_list,
                        });
                        this.setState({
                            itr_list: res.data.ITR_list,
                        });

                 } else {
                     alert(res.msg)
                    // dispatch(loginIsLoading(false));
                    // setTimeout(() => {
                    //     dispatch(loginHasError(res.message));
                    //     dispatch(loginHasError(undefined));
                    // }, 1000);
                }
            })
            .catch((e) => {
                console.log(e)
            });
    
    };

    render() {
    //  const items = [
    //         { name: 'Mayank Nave', email: 'mayank@gmail.com', id: 1, status: "approved",image: require('./../../Images/pan-card.png')},
    //          { name: 'Shreyash singh',email: 'Shreyash@gmail.com', id: 2, status: "reject",image: require('./../../Images/ITR.png')},
    //         { name: 'shubham rai', email: 'Shubham@gmail.com',  id: 3, status: "pending",image: require('./../../Images/loan-enquiry.png')},
    //         { name: 'Ramesh ji', email: 'Ramesh@gmail.com',  id: 4, status: "approved",image: require('./../../Images/history.png')},
    //     ];

        return (
            <SafeAreaView style={styles.container}>
                <AppHeader
                    goBack={() => this.props.navigation.goBack()}
                    addevent={() => this.validate()}
                    leftImg={require('./../../components/Images/assets/icn_back.png')}
                    // rightImg={require('./../../components/Images/checked.png')}
                    headerTitle="History"
                />

                {/* <ScrollView bounces={false} style={{ top: 0, }}> */}

                <KeyboardAwareScrollView
                    style={{ backgroundColor: 'transparent' }}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    contentContainerStyle={{flex:1}}
                    scrollEnabled={true}
                    bounces={false}
                >
                <View style={styles.eventcontainer} >

                    <Tabs tabBarUnderlineStyle={{
                        backgroundColor: 'transparent',
                        height: 0.8
                    }}>

                        <Tab heading="PAN Card"
                            activeTextStyle={{ color: '#fff', fontWeight: 'bold' }}
                            textStyle={{ color: 'gray', }}
                            tabStyle={{ backgroundColor: "#fff" }}
                            activeTabStyle={{ backgroundColor: "#012b72" }}>
                          
                            <FlatList
                            data={this.state.pan_list}
                            renderItem={({ item }) =>     
                            <View style={{
                                flex: 1,
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}>
                       
                                <View style={styles.view1}>
                                    <View style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between'
                                    }}>
                                        <View style={{
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                        }}>
                                        
                                            <Image  style={styles.imgg1}
                                              source={require('./../../Images/logom.png')} />
                                        </View>
                                        <View style={{
                                            flexDirection: 'column',
                                            marginLeft: 10, marginBottom: 15
                                        }}>
                                            <View style={styles.view2}>
                                                <Text style={{
                                                    color: '#fff',
                                                    marginLeft: 10,
                                                    fontWeight: 'bold',
                                                    fontSize: 14
                                                }}>
                                                    {item.name}
                                                </Text>
                                            
                                            </View>
                                            <View>
                                                <Text style={{
                                                    color: '#fff',
                                                    marginLeft: 10,
                                                    fontSize: 12
                                                }}>
                                                    {item.email}
                                                </Text>
                                                <Text style={{
                                                    color: '#fff',
                                                    marginLeft: 10,
                                                    fontSize: 12
                                                }}>
                                                   Reference No. - {item.pan_application_id}
                                                </Text>
                                            </View>
                                        </View>
                                        <View>
                                            <Text style={{
                                                color: '#fff',
                                                marginLeft: 10,
                                                fontSize: 12
                                            }}>
                                            </Text>
                                        </View>
                                    </View>
                              
                                </View>
                            </View>
                        }
                        />
                        </Tab>

                        <Tab heading="ITR"
                            activeTextStyle={{ color: '#fff', fontWeight: 'bold' }}
                            textStyle={{ color: 'gray', }}
                            tabStyle={{ backgroundColor: "#fff" }}
                            activeTabStyle={{ backgroundColor: "#012b72" }}>

                            <FlatList
                            data={this.state.itr_list}
                            renderItem={({ item }) =>     
                            <View style={{
                                flex: 1,
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}>
                       
                                <View style={styles.view1}>
                                    <View style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between'
                                    }}>
                                        <View style={{
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                        }}>
                                        
                                            <Image  style={styles.imgg1}
                                              source={require('./../../Images/logom.png')} />
                                        </View>
                                        <View style={{
                                            flexDirection: 'column',
                                            marginLeft: 10, marginBottom: 15
                                        }}>
                                            <View style={styles.view2}>
                                                <Text style={{
                                                    color: '#fff',
                                                    marginLeft: 10,
                                                    fontWeight: 'bold',
                                                    fontSize: 14
                                                }}>
                                                    {item.name}
                                                </Text>
                                            
                                            </View>
                                            <View>
                                                <Text style={{
                                                    color: '#fff',
                                                    marginLeft: 10,
                                                    fontSize: 12
                                                }}>
                                                    {item.email}
                                                </Text>
                                                <Text style={{
                                                    color: '#fff',
                                                    marginLeft: 10,
                                                    fontSize: 12
                                                }}>
                                                    Reference No. - {item.itr_application_id}
                                                </Text>
                                            </View>
                                        </View>
                                        <View>
                                            <Text style={{
                                                color: '#fff',
                                                marginLeft: 10,
                                                fontSize: 12
                                            }}>
                                               
                                            </Text>
                                        </View>
                                    </View>
                              
                                </View>
                            </View>
                        }
                        />
                        </Tab>

                    </Tabs>
                </View>
                </KeyboardAwareScrollView>
                {/* </ScrollView> */}
            </SafeAreaView>
        );
    }
}


export default (History)