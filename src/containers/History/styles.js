import { StyleSheet,Dimensions } from 'react-native';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
export const COLOR = {

    BLUE: "#1e244a", // email box color
    RED: "#ea4335",
    YELLOW: "#fff673",
    LIGHTBLUE:"#0096da",
    GREEN:"#58d263"

};

export default StyleSheet.create({
    container: {
        flex: 1,
       
      },
      eventcontainer: {
        flex: 1,
        backgroundColor:'transparent'
      // marginLeft:10,
      // marginRight:10
      },
      cardView: {
        width: width-20,  
        height: height / 2 - 100 ,
        marginTop:30
      },
      eventImgSize: {
        minHeight: 120,
        width:width - 20
      },
      inputView: {
        width : width-20, 
       
        justifyContent:'center',
        height:60,
        //alignItems: 'center',
       
        borderWidth:1,
        paddingLeft:10,
        marginVertical:10
 
    },
    inputView1: {
      width : width-20, 
      justifyContent:'center',
      minHeight:100,
      //alignItems: 'center',
      borderWidth:1,
      paddingLeft:10,
      marginVertical:10

  },

  ViewImage: {
    // justifyContent: 'center',
    alignSelf: 'center',
    marginTop: 20,
    height:(width * 0.15) + 25,
    marginLeft:10,
    marginRight:10,
    width:width / 1.1,
    borderWidth:1,
    borderColor:"gray",
    borderRadius:5,
    flexDirection:'row',
 },

  textInputContainer: {
    // position: "absolute",
    alignSelf: "center",
    top: 20,
    backgroundColor: "transparent",
    height: (width * 0.15) + 20,
    width: width / 1.1,
},

  imageContainer: {
  // position: "absolute",
  alignSelf: "center",
  top: "3%",
  backgroundColor: "transparent",
  height: (width * 0.15) + 25,
  width: width / 1.1,
  borderWidth:.8,
  borderColor:"gray",
  borderRadius:5
},

  textInputStyle: {
    position: "absolute",
    top: 10,
    left:5,
    height: (width * 0.15),
    width: width / 1.1,
    borderColor: 'rgba(204,204,204,1)',
    borderWidth: 1,
    borderRadius: 5,
    padding: 10,
    fontSize: 18,
},

floatingText: {
    position: "absolute",
     backgroundColor: "white",
    color: "#012b72",
    fontWeight: "normal",
    fontSize: 16,
    // height: 20,
    top: -2,
    left: 10,
    paddingLeft: 3,
    paddingRight: 3,
},
floatingImage: {
  position: "absolute",
   backgroundColor: "white",
  color: "#012b72",
  height: 10,
  width:15,
  bottom: 0,
  right: 10,
  paddingLeft: 3,
  paddingRight: 3,
},

View3: {
  borderRadius: 7,
  height: "15%", alignSelf: 'center', top:5,
 width: width / 2.0, marginTop: "15%",
  justifyContent: 'center', backgroundColor: '#012b72'
},
View4: {
  borderRadius: 20,
  height: 40, alignSelf: 'center', top:5,
 width: width / 2.0, marginTop: "10%", bottom:10,
  justifyContent: 'center', backgroundColor: '#012b72', 
},

View5: {
  borderRadius: 20,
  height: 40, alignSelf: 'center', top:5,
 width: width / 2.0, marginTop: "10%", bottom:10,
  justifyContent: 'center', backgroundColor: '#00b9f5', 
},

SignText: {
  fontSize: 20, color: '#fff',
  textAlign: 'center', 
  alignSelf:'center',
  top:5,
  backgroundColor: 'transparent'
},
view1: {
  flexDirection: 'row',
  justifyContent: 'space-between',
  backgroundColor: 'gray',
  height: (width * 0.23),
  width: width / 1.07,
  marginTop: 10,
  paddingHorizontal:10
},
imgg1: {
  width: 50,
  height: 50,
 
},
view2: {
  flexDirection: 'row',
  marginTop: 20,
  marginRight: 50
},
imgg2: {
  width: 90,
  height: 13,
  marginLeft: 10,
  // marginTop: 5
},
imgg3: {
  width: 20,
  height: 20,
  marginTop: 30
},
 
   
});