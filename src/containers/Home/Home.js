import * as React from 'react'
import {
    StyleSheet,
    Text,
    Dimensions,
    View,
    Image,StatusBar,
    Alert,
    BackHandler,
    AsyncStorage, ImageBackground, TextInput, TouchableOpacity, SafeAreaView, ScrollView, FlatList, Platform
} from 'react-native';
import { Footer, Card, Header, Container, Body, Title, Left, Right } from 'native-base';
import EventListActions from '../../actions/ActionEventList'
import EventFilterActions from '../../actions/ActionEventFilter'
import * as shape from 'd3-shape'
import { connect } from 'react-redux';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import styles, { COLOR } from './styles';
import { Actions } from 'react-native-router-flux';
import CalendarStrip from 'react-native-calendar-strip';
import AppHeader from '../../components/AppHeader';
import { FlatGrid } from 'react-native-super-grid';

var token = ""

//var elist: []
class HomeComponent extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            Name: '',
            vanue: props.cityname,
            lat: props.lat,
            long: props.lng,
            start_date: ''
        };
        console.log(props)
    };

    // async componentWillMount() {
    //     this.willFocus = this.props.navigation.addListener('willFocus', () => {
    //         this.eventFuc()
    //     });

    // }
    Navigate (item){
               console.log(item)                   
                  if(item.id ==1 ){
                        // Actions.PAN()
                       this.props.navigation.navigate("PAN") 
                    }
                    if(item.id ==2 ){
                        // Actions.PAN()
                       this.props.navigation.navigate("ITR") 
                    }
                    if(item.id ==3 ){
                        // Actions.PAN()
                       this.props.navigation.navigate("LoanEnquiry") 
                    }
                    if(item.id ==4 ){
                        // Actions.PAN()
                       this.props.navigation.navigate("History") 
                    }
                    if(item.id ==5 ){
                        // Actions.PAN()
                       this.props.navigation.navigate("ContactUs") 
                    }
                    if(item.id ==6 ){
                        // Actions.PAN()
                       this.props.navigation.navigate("Photogallary") 
                    }
                    if(item.id ==7 ){
                        // Actions.PAN()
                       this.props.navigation.navigate("Searchform") 
                    }
                    if(item.id ==8 ){
                        // Actions.PAN()
                       this.props.navigation.navigate("HowToUse") 
                    }
                    else{
                    //    alert("somthing wrong")
                    }   

    }
    handleBackButton = () => {
        Alert.alert(
            'PAN SJBT',
            'Are you sure you want to exit from App?',
            [
                { text: 'cancel', onPress: () => console.log('Cancel Pressed') },
                { text: 'OK', onPress: () => BackHandler.exitApp() },
            ],
            { cancelable: false }
        )
        return true;
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
       
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    // componentWillMount() {
    //     BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    //   }
    //   componentWillUnmount() {
    //     BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    //   }
    //   handleBackButtonClick() {
    //     BackHandler.exitApp();
    //     return true;
    //   }

    render() {
        const items = [
            { name: 'PAN', code: '#012b72', id: 1, image: require('./../../Images/pan-card.png')},
             { name: 'ITR', code: '#012b72', id: 2, image: require('./../../Images/ITR.png')},
            { name: 'Loan Enquiry', code: '#012b72',  id: 3, image: require('./../../Images/loan-enquiry.png')},
            { name: 'My History', code: '#012b72',  id: 4, image: require('./../../Images/history.png')},
            //  { name: 'About Us', code: '#012b72',  id: 4, image: require('./../../Images/about-us.png')},
            { name: 'Contact Us', code: '#012b72',  id: 5, image: require('./../../Images/contact-us.png')},
             { name: 'Photo Gallary', code: '#012b72',  id: 6, image: require('./../../Images/gallary.png')},
             { name: 'How to use', code: '#012b72',  id: 8, image: require('./../../Images/how-to-use.png')},

             { name: 'Search Acknowledgement', code: '#012b72',  id: 7, image: require('./../../Images/search.png')},

        ];

        return (
            <SafeAreaView style={styles.container}>
       
            <View style={styles.ViewImage}>
            <Image resizeMode="cover" style={{height:100, width:100}}
                source={require('./../../Images/logom.png')} />
        </View>
            <ScrollView bounces={false} style={{ top: 0, }}>
             <View style={{flexDirection:'row', marginTop:20}}>
              <Text style={{fontSize:13, position:'absolute', left:10, color:'gray', textAlign:'center'}}>Customer Care No.{"\n"}+91-9993574282
              </Text>
              <Text style={{fontSize:13, position:'absolute', right:10, color:'gray', textAlign:'center'}}>Customer Support Email {"\n"}Support@gmail.com
              </Text>
             </View>
                <View style={styles.eventcontainer} >

                    <FlatGrid
                        itemDimension={150}
                        items={items}
                        style={styles.gridView}
                        // staticDimension={300}
                        // fixed
                        // spacing={20}
                        renderItem={({ item, index }) => (
                            <TouchableOpacity onPress= {() => this.Navigate(item)} >

                            <View style={[styles.itemContainer, { backgroundColor: item.code }]}>
                                <Image resizeMode="center" style={styles.itemimage}
                                source={item.image} />
                                <Text style={styles.itemName}>{item.name}</Text>
                            </View>
                           </TouchableOpacity>  
                        )}
                    />
                </View>
             </ScrollView>   
            </SafeAreaView>
        );
    }
}


const mapStateToProps = (state) => {
    return {

        eventData: state.Eventlist.eventData,
        //eventFilterData: state.eventFilterData.eventFilterData
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        eventApi: () => dispatch(EventListActions.eventApi()),
        EventFilterApi: (start_date, latLng) => dispatch(EventFilterActions.EventFilterApi(start_date, latLng))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeComponent)