import { StyleSheet,Dimensions } from 'react-native';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
export const COLOR = {

    BLUE: "#1e244a", // email box color
    RED: "#ea4335",
    YELLOW: "#fff673",
    LIGHTBLUE:"#0096da",
    GREEN:"#58d263"
};

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'#F8F9F9'
      },
      eventcontainer: {
        flex: 1,
        alignItems:'center',
        marginTop: (height * 0.10),       
      },
      cardView: {
        width: width-20, 
        height: height / 2 - 100 ,
        marginTop:30
      },
      eventImgSize: {
        minHeight: 120,
        width:width - 20
      },
      faltlist: {
        alignSelf: 'center', width: '100%' 
      },
      gridView: {
        flex: 1,
      },
      itemContainer: {
        justifyContent: "center",
        borderRadius: 5,
        padding: 10,
        height: 100,
      },
      itemName: {
        fontSize: 16,
        color: '#fff',
        textAlign: 'center',
        fontWeight: '600',
      },
      itemCode: {
        fontWeight: '600',
        fontSize: 12,
        color: '#fff',
      },
      ViewImage: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: "5%",
    },
    itemimage: {
      height:30,
      width: 40,
      alignSelf:'center'
    },
   
});