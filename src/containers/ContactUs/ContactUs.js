import * as React from 'react'
import {
    StyleSheet,
    Text,
    Dimensions,
    View,
    Image, Platform,
    TouchableOpacity,
    AsyncStorage, ImageBackground, ScrollView
} from 'react-native';
import styles, { COLOR } from './styles';
import { Actions } from 'react-native-router-flux';
import AppHeader from '../../components/AppHeader';

// import console = require('console');

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

class ContactUs extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user_id:'',
        };
    };

    render() {
        return (
            <View style={styles.container}>
    
             <AppHeader
             goBack={() => this.props.navigation.goBack()}
             addevent={() => this.validate()}
             leftImg={require('./../../components/Images/assets/icn_back.png')}
             headerTitle="Contact Us"
           />
           <ScrollView bounces={false} style={{ top: 0, }}>

                <View style={[styles.container,]}>

                <View style={styles.ViewImage}>
                    <Image  style={{ 
                         height: 100,
                        width: 100,
                     }}
                     resizeMode="cover"
                        source={require('./../../Images/logom.png')} />
                </View>

                <Text style={styles.Text}>Trust Name	:	SHRI JAIN BANDHU TRUST {"\n"}
                Registration No.	:	94/102/1949{"\n"}
                
                PAN	:	AAKTS5990H{"\n"}
                
                Contact Person	:	RAJEEV JAIN{"\n"}
                
                Mob No.	:	09303530886{"\n"}
                
                Phone No.	:	0731-4278691{"\n"}
               
                Address	:	Off. No.C2 Block-C 2nd Floor BCM City, Navlakha Square Indore (M.P.){"\n"}
                 
                Web Site	:	www.jainbandhutrust.com{"\n"}
                Email	:	support@jainbandhutrust.com /  jainbandhutrust@gmail.com{"\n"}
                 
                SHRI JAIN BANDHU TRUST CA A/C	:	14057620000023{"\n"}
                IFSC CODE	:	HDFC0001405{"\n"}
                HOLDER NAME	:	RAJEEV KUMAR JAIN{"\n"}
                SBI BANK ACCOUNT NO.	:	11133338527{"\n"}
                IFSC CODE	:	SBIN0003432{"\n"}
                PHONE PE MOBILE NO.	:	9303530886{"\n"}
                PAYTM MOBILE NO.	:	9303530886
                 </Text>                        
                </View>
                <View style={styles.View4}>
                        <TouchableOpacity onPress={() => { this.props.navigation.navigate('Bussinessequiry') }}>
                            <Text style={styles.SignText}>Business Enquiry</Text>
                        </TouchableOpacity>
                    </View>
             </ScrollView>
            </View>
        );
    }
}
export default ContactUs;

