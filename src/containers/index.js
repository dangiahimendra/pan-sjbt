import React, { Component } from 'react';
// import * as React from 'react'
import { Router, Scene } from "react-native-router-flux";
import { connect } from "react-redux";
import { AsyncStorage, Text } from "react-native";
import Loader from '../constants/Loader';
import Splash from './Splash'
import Login from './Login';
import HomeComponent from './../containers/Home'
import ITR from './../containers/ITR'
import Payment from './Payment'
import Paymentitr from './Paymentitr'
import Bussinessequiry from './Bussinessequiry'
import Photogallary from './Photogallary'



import Paymentweb from './Paymentweb'

import PAN from './../containers/PAN'
import Single from './../containers/Single'
import Multiple from './../containers/Multiple'
import History from './../containers/History'

import ContactUs from './../containers/ContactUs'
import HowToUse from './../containers/HowToUse'
import LoanEnquiry from './../containers/LoanEnquiry'
import SpinnerComponent from './Spinner'
import SearchPlaces from './../containers/SearchPlaces'
import Searchform from './../containers/Searchform'


const RouterWithRedux = connect()(Router);

class Root extends Component {
    constructor(props) {
        super(props);

        this.state = {
            token: null,
            isStorageLoaded: false,
            loginState: true,
            homeState: false
        }
    }

    async  componentDidMount() {
        AsyncStorage.getItem('token').then((token) => {
            this.setState({
                token: token !== null,
                isStorageLoaded: true
            })
        });

    }

    render() {
        let { isLogged } = this.props.login;
        let { token, isStorageLoaded } = this.state;
        if (!isStorageLoaded) {
            return (
                <Loader loading={true} />
            )
        } else {
            return (
                <RouterWithRedux>
                    <Scene key='root'>
                        <Scene
                            component={Splash}
                            initial={true}
                            hideNavBar={true}
                           
                            key='Splash'
                            title='Splash'
                        />
                        <Scene
                            component={Login}
                            initial={false}
                            hideNavBar={true}
                            key='Login'
                            title='Login'
                        />
                        <Scene
                            component={HomeComponent}
                            initial={false}
                            hideNavBar={true}
                            key='Home'
                            title='Home'
                        />
                        <Scene
                            component={SpinnerComponent}
                            initial={false}
                            hideNavBar={true}
                            key='Spinner'
                            title='Spinner'
                        />
                        
                        <Scene
                            component={ITR}
                            initial={false}
                            hideNavBar={true}
                            key='ITR'
                            title='ITR'
                        />
                        <Scene
                            component={PAN}
                            initial={false}
                            hideNavBar={true}
                            key='PAN'
                            title='PAN'
                        />
                        <Scene
                            component={Single}
                            initial={false}
                            hideNavBar={true}
                            key='Single'
                            title='Single'
                        />
                        <Scene
                            component={Multiple}
                            initial={false}
                            hideNavBar={true}
                            key='Multiple'
                            title='Multiple'
                        />
                        <Scene
                            component={History}
                            initial={false}
                            hideNavBar={true}
                            key='History'
                            title='History'
                        />
                        <Scene
                            component={LoanEnquiry}         
                            initial={false}
                            hideNavBar={true}
                            key='LoanEnquiry'
                            title='LoanEnquiry'
                        />
                        <Scene
                            component={ContactUs}
                            initial={false}
                            hideNavBar={true}
                            key='ContactUs'
                            title='ContactUs'
                        />
                        <Scene
                            component={HowToUse}
                            initial={false}
                            hideNavBar={true}
                            key='HowToUse'
                            title='HowToUse'
                        />
                        <Scene
                            component={SearchPlaces}
                            initial={false}
                            hideNavBar={true}
                            key='SearchPlaces'
                            title='SearchPlaces'
                        />
                         <Scene
                            component={Payment}
                            initial={false}
                            hideNavBar={true}
                            key='Payment'
                            title='Payment'
                        />
                         <Scene
                            component={Paymentitr}
                            initial={false}
                            hideNavBar={true}
                            key='Paymentitr'
                            title='Paymentitr'
                        />
                        <Scene
                            component={Paymentweb}
                            initial={false}
                            hideNavBar={true}
                            key='Paymentweb'
                            title='Paymentweb'
                        />
                        <Scene
                            component={Searchform}
                            initial={false}
                            hideNavBar={true}
                            key='Searchform'
                            title='Searchform'
                        />
                        <Scene
                            component={Bussinessequiry}
                            initial={false}
                            hideNavBar={true}
                            key='Bussinessequiry'
                            title='Bussinessequiry'
                        />
                        <Scene
                            component={Photogallary}
                            initial={false}
                            hideNavBar={true}
                            key='Photogallary'
                            title='Photogallary'
                        />
                        
                        
                    </Scene>
                </RouterWithRedux>
            )
        }
    }
}

const mapStateToProps = (state) => {
    return {
        login: state.login
    }
};

export default connect(mapStateToProps)(Root)
