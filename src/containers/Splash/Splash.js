import * as React from 'react'
import {
    StyleSheet,
    Text,
    Dimensions,
    View,
    Image,
    AsyncStorage, ImageBackground
} from 'react-native';
import styles, { COLOR } from './styles';
import { Actions } from 'react-native-router-flux';

// import console = require('console');

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

class Splash extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user_id:''

        };
    };
    getData = async () => {
        try {
          const value = await AsyncStorage.getItem('@storage_Key')
          if(value !== null) {
            // value previously stored
            console.log('aid',value)
            this.setState({
                user_id: value
          });           
         }
        } catch(e) {
          // error reading value
          console.log('error',e)
        }
      }  
    async componentDidMount() {
        // const user_id =  await AsyncStorage.getItem('user_id')
        //  console.log(user_id)
     this.getData()
            try {
              
                setTimeout(() => {
                    if(this.state.user_id != ''){
                        Actions.Home();
                        console.log('Home')

                    }
                    else{

                    Actions.Login();
                    console.log('Login')
                    }                          
                }, 1000);
            
        }
            catch (error) {
                console.log('error' + error)
            }
                
    }

    render() {
        return (
            <View style={styles.container}>
                    <Image 
                        source={require('./../../Images/splashm.png')}
                        // resizeMode="contain"
                        style={{height, width}}
                        />
                        
            </View>
        );
    }
}
export default Splash;

