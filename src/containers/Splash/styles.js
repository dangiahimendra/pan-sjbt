import { StyleSheet } from 'react-native';
// import { Colors } from '../../Theme';

export const COLOR = {

    RED: "#b71221",
    BLUE: '#ff6f61',
    BLACK: "#ffffff",
    WHITE: "#fff",

};

export default StyleSheet.create({
    container: {
                flex: 1,
              
            
    },
    Images:{
        alignItems: 'center',
        height: "100%", width: "100%",
    },
    Text:{ fontSize: 20, color: '#fff', fontWeight: 'bold' }
   
});
