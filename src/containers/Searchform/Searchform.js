import * as React from 'react'
import {
    StyleSheet,
    Text,
    Dimensions,
    View,
    Image,
    AsyncStorage,PermissionsAndroid, Right, ImageBackground, TextInput, TouchableOpacity, SafeAreaView, AlertIOS, ToastAndroid, ScrollView, FlatList, Platform
} from 'react-native';
import { Footer, Card, Content, Tab, Tabs, Header } from 'native-base';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import styles, { COLOR } from './styles';
import { Actions } from 'react-native-router-flux';
import AppHeader from '../../components/AppHeader'

import RNFetchBlob from 'react-native-fetch-blob'


var token = ""
// import NEW from './../containers/NEW'        
// import Duplicate from './../containers/Duplicate'        

class Searchform extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            vanue: props.cityname,
            lat: props.lat,
            long: props.lng,
            start_date: '',
            end_date: '',
            title: '',
            description: '',
            adhar_front_image: null,
            adhar_front_image_path: '',

            adhar_back_image: null,
            adhar_back_image_path: '',

            dlicencef_image: null,
            signature_image_path: '',

            dlicenceb_image: null,
            other_image_path: '',
            pimage: null,
            pimage_path: '',
            name: '',
            email: '',
            contact: '',
            id: '',
            count: '',
            status: '',
            download: false,
            comment:'',
            iditr: '',
            statusitr: '',
            downloaditr: false,
            commentitr:'',
            panid:'',
            itrid:''
        };

    };

    openSearchModal() {
        Actions.SearchPlaces("AddEvent")
    }
    validate = () => {
        if (this.state.id == "" || this.state.id == null || this.state.id == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter Reference No.'); },
                android: () => { ToastAndroid.show('Please enter Reference No.', ToastAndroid.SHORT); }
            })();
            return false;
        }


        else {
            this.Search()
        }
    }
    validateitr = () => {
        if (this.state.iditr == "" || this.state.iditr == null || this.state.iditr == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter Reference No.'); },
                android: () => { ToastAndroid.show('Please enter Reference No.', ToastAndroid.SHORT); }
            })();
            return false;
        }


        else {
            this.SearchforITR()
        }
    }
     download (url, name)  {
        const { config, fs } = RNFetchBlob;
        let PictureDir = fs.dirs.DocumentDir;
        let options = {
          fileCache: true,
          addAndroidDownloads: {
            useDownloadManager: true,
            notification: true,
            path: PictureDir + name,
            description: t('downloading_file')
          }
        };
        config(options)
          .fetch('GET', url)
          .then(res => {
            if (res.data) {
              alert(t('download_success'));
            } else {
              alert(t('download_failed'));
            }
          });
      };
      async  requestCameraPermission(id,type) {
        try {
          const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            {
              title: 'Storage Permission',
              message:
                'PAN-SJBT App needs access to your Storage ' +
                'for store files.',
              buttonNeutral: 'Ask Me Later',
              buttonNegative: 'Cancel',
              buttonPositive: 'OK',
            },
          );
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            this.pdf(id,type)
            console.log('Storage permission granted');
          } else {
            console.log('Storage permission denied');
          }
        } catch (err) {
          console.warn(err);
        }
      }
    

    pdf(applicationid,type) {
        Platform.select({
            ios: () => { AlertIOS.alert('PDF is Downloading....'); },
            android: () => { ToastAndroid.show('PDF is Downloading', ToastAndroid.SHORT); }
        })();
            const { config, fs } = RNFetchBlob
            let DownloadDir = fs.dirs.DownloadDir // this is the Downloads directory.
            let options = {
            fileCache: true,
            // appendExt : extension, //Adds Extension only during the download, optional
            addAndroidDownloads : {
                notification : true,
            useDownloadManager : true, //uses the device's native download manager.
          
             mime: 'application/pdf',
            title : "Acknowledgement", // Title of download notification.
            path: DownloadDir + "/Acknowledgement"+ '.' + "pdf", // this is the path where your download file will be in
            description : 'Downloading file.'
            }
            }
            config(options)
            .fetch('GET',"http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/AcknowledgementPDF?reference_id="+applicationid+"&type="+type)
            .then((res) => {
            console.log("Success");
            Platform.select({
                ios: () => { AlertIOS.alert('Download complete'); },
                android: () => { ToastAndroid.show('Download complete', ToastAndroid.SHORT); }
            })();
            })
            .catch((err) => {
                console.log('error',err)
            }) // To execute when download cancelled and other errors
            
    }

    async Search() {
        // console.log(this.state.code)
        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/searchPANreference', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "reference_id": this.state.id

            })
        }).then((res) => res.json())
            .then(res => {
                console.log(res)
                if (res.result === "true" || res.result === true) {
                    console.log('hdata', res.data[0].status)
                    this.setState({
                        panid: res.data[0].pan_application_id
                    });
                    this.setState({
                        status: res.data[0].status
                    });
                    this.setState({
                        comment: res.data[0].comment
                    });
                    this.setState({
                        download: true
                    });

                } else {
                    alert(res.msg)
                    // dispatch(loginIsLoading(false));
                    // setTimeout(() => {
                    //     dispatch(loginHasError(res.message));
                    //     dispatch(loginHasError(undefined));
                    // }, 1000);
                }
            })
            .catch((e) => {
                console.log(e)
            });

    };

    async SearchforITR() {
        // console.log(this.state.code)
        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/searchITRreference', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "reference_id": this.state.iditr

            })
        }).then((res) => res.json())
            .then(res => {
                console.log(res)
                if (res.result === "true" || res.result === true) {
                    console.log('hdata', res.data[0].status)
                    this.setState({
                        itrid: res.data[0].itr_application_id
                    });
                    this.setState({
                        statusitr: res.data[0].status
                    });
                    this.setState({
                        commentitr: res.data[0].comment
                    });
                    this.setState({
                        downloaditr: true
                    });

                } else {
                    alert(res.msg)
                    // dispatch(loginIsLoading(false));
                    // setTimeout(() => {
                    //     dispatch(loginHasError(res.message));
                    //     dispatch(loginHasError(undefined));
                    // }, 1000);
                }
            })
            .catch((e) => {
                console.log(e)
            });

    };
    render() {
        console.log('uploadimg', this.state.download)

        return (
            <SafeAreaView style={styles.container}>
                <AppHeader
                    goBack={() => this.props.navigation.goBack()}
                    addevent={() => this.validate()}
                    leftImg={require('./../../components/Images/assets/icn_back.png')}
                    // rightImg={require('./../../components/Images/checked.png')}
                    headerTitle="Search"
                />

                <View style={styles.eventcontainer} >

                    <Tabs tabBarUnderlineStyle={{
                        backgroundColor: '#1e244a',
                        height: 0.8
                    }}>

                        <Tab heading="PAN"
                            activeTextStyle={{ color: '#fff', fontWeight: 'bold' }}
                            textStyle={{ color: 'gray', }}
                            tabStyle={{ backgroundColor: "#fff" }}
                            activeTabStyle={{ backgroundColor: "#012b72" }}>
                            <View style={{ flex: 1, alignContent: 'center' }}>
                                <View style={styles.textInputContainer} >

                                    <TextInput style={styles.textInputStyle}
                                        placeholder={'Enter Reference No.'}
                                        placeholderTextColor={'gray'}
                                        returnKeyType={'default'}
                                        autoCapitalize={'none'}
                                        underlineColorAndroid={'transparent'}
                                        onChangeText={(id) => this.setState({ id })}
                                        editable={this.state.TextInputDisableStatus}
                                        value={this.state.id}
                                    />
                                </View>


                                <View style={styles.View3}>
                                    <TouchableOpacity onPress={() => { this.validate() }}>
                                        <Text style={styles.SignText}>Search</Text>
                                    </TouchableOpacity>
                                    <View style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between'
                                    }}>


                                        {/* <TextInput style={styles.textInputStyle}
                                                placeholder={'Enter Reference No.'}
                                                placeholderTextColor={'gray'}
                                                returnKeyType={'default'}
                                                autoCapitalize={'none'}
                                                underlineColorAndroid={'transparent'}
                                                onChangeText={(id) => this.setState({ id })}
                                                editable={this.state.TextInputDisableStatus}
                                                value={this.state.id}
                                            />
                                            
                                           
                                            
                                            <View style={styles.View4}>
                        <TouchableOpacity onPress={() => { this.Search() }}>
                            <Text style={styles.SignText}>search</Text>
                        </TouchableOpacity>
                    </View> */}
                                    </View>

                                </View>

                                {/* <FlatList
                                data={this.state.formsMulti}
                                renderItem={({ item, index }) => (



                                    <View style={{ flex: 1, }}> */}

                                <View style={styles.view1}>
                                    <View style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        marginTop:15
                                    }}>

                                        <View style={{
                                            flexDirection: 'column',
                                            marginLeft: 10, marginBottom: 15
                                        }}>
                                            <View style={styles.view2}>
                                                <Text style={{
                                                    color: '#000',
                                                    marginLeft: 12,
                                                    fontWeight: 'bold',
                                                    fontSize: 20
                                                }}>
                                                    Status
                                                </Text>

                                            </View>
                                            <View>
                                                {
                                                    this.state.status
                                                        ?
                                                        <Text style={{
                                                            color: '#000',
                                                            marginLeft: 15,
                                                            fontSize: 18,
                                                            marginTop: 10
                                                        }}>
                                                            {this.state.status}
                                                        </Text>
                                                        : <Text style={{
                                                            color: '#000',
                                                            marginLeft: 10,
                                                            fontSize: 18,
                                                            marginTop: 5
                                                        }}>

                                                        </Text>
                                                }
                                            </View>
                                        </View>
                                        <View>
                                            <Text style={{
                                                color: '#000',
                                                marginLeft: 10,
                                                marginRight: 20,
                                                fontSize: 20,
                                                fontWeight: 'bold',
                                            }}>
                                                Acknowledgement
                                            </Text>
                                            {
                                                this.state.status=="Verify"
                                                    ?
                                                    <View style={styles.View4}>
                                                        <TouchableOpacity onPress={() => { this.requestCameraPermission(this.state.panid,"PAN")}}>
                                                            <Text style={styles.SignText}>Download</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                    :
                                                    null
                                            }
                                        </View>
                                      
                                    </View>
                                    <View style={styles.view2}>
                                                <Text style={{
                                                    color: '#000',
                                                    marginLeft: 21,
                                                    marginRight: 21,
                                                    fontWeight: 'bold',
                                                    fontSize: 20
                                                }}>
                                                    Comments
                                                </Text>
                                                <Text style={{
                                                    color: '#000',
                                                    marginLeft: 21,
                                                    marginRight: 21,
                                                    fontWeight: 'bold',
                                                    fontSize: 20
                                                }}>
                                                    {this.state.comment}
                                                </Text>

                                            </View>

                                </View>
                            </View>
                            {/* </View>
                                )}
                                keyExtractor={(item, index) => index}
                            /> */}

                        </Tab>

                        <Tab heading="ITR"
                            activeTextStyle={{ color: '#fff', fontWeight: 'bold' }}
                            textStyle={{ color: 'gray', }}
                            tabStyle={{ backgroundColor: "#fff" }}
                            activeTabStyle={{ backgroundColor: "#012b72" }}>
                            <View style={{ flex: 1, alignContent: 'center' }}>
                                <View style={styles.textInputContainer} >

                                    <TextInput style={styles.textInputStyle}
                                        placeholder={'Enter Reference No.'}
                                        placeholderTextColor={'gray'}
                                        returnKeyType={'default'}
                                        autoCapitalize={'none'}
                                        underlineColorAndroid={'transparent'}
                                        onChangeText={(iditr) => this.setState({ iditr })}
                                        editable={this.state.TextInputDisableStatus}
                                        value={this.state.iditr}
                                    />
                                </View>


                                <View style={styles.View3}>
                                    <TouchableOpacity onPress={() => {this.validateitr() }}>
                                        <Text style={styles.SignText}>Search</Text>
                                    </TouchableOpacity>
                                    <View style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                       
                                    }}>


                                        {/* <TextInput style={styles.textInputStyle}
                                                placeholder={'Enter Reference No.'}
                                                placeholderTextColor={'gray'}
                                                returnKeyType={'default'}
                                                autoCapitalize={'none'}
                                                underlineColorAndroid={'transparent'}
                                                onChangeText={(id) => this.setState({ id })}
                                                editable={this.state.TextInputDisableStatus}
                                                value={this.state.id}
                                            />
                                            
                                           
                                            
                                            <View style={styles.View4}>
                        <TouchableOpacity onPress={() => { this.Search() }}>
                            <Text style={styles.SignText}>search</Text>
                        </TouchableOpacity>
                    </View> */}
                                    </View>

                                </View>

                                {/* <FlatList
                                data={this.state.formsMulti}
                                renderItem={({ item, index }) => (



                                    <View style={{ flex: 1, }}> */}

                                <View style={styles.view1}>
                                    <View style={{
                                        flexDirection: 'row',
                                        justifyContent: 'space-between',
                                        marginTop:15
                                    }}>

                                        <View style={{
                                            flexDirection: 'column',
                                            marginLeft: 10, marginBottom: 15
                                        }}>
                                            <View style={styles.view2}>
                                                <Text style={{
                                                    color: '#000',
                                                    marginLeft: 12,
                                                    fontWeight: 'bold',
                                                    fontSize: 20
                                                }}>
                                                    Status
                                                </Text>

                                            </View>
                                            <View>
                                                {
                                                    this.state.statusitr
                                                        ?
                                                        <Text style={{
                                                            color: '#000',
                                                            marginLeft: 15,
                                                            fontSize: 18,
                                                            marginTop: 10
                                                        }}>
                                                            {this.state.statusitr}
                                                        </Text>
                                                        : <Text style={{
                                                            color: '#000',
                                                            marginLeft: 10,
                                                            fontSize: 18,
                                                            marginTop: 5
                                                        }}>

                                                        </Text>
                                                }
                                            </View>
                                        </View>
                                        <View>
                                            <Text style={{
                                                color: '#000',
                                                marginLeft: 15,
                                                marginRight: 20,
                                                fontSize: 20,
                                                fontWeight: 'bold',
                                            }}>
                                                Acknowledgement
                                            </Text>
                                            {this.state.statusitr=="Verify"
                                            ? <View style={styles.View4}>
                                            <TouchableOpacity onPress={() => { this.requestCameraPermission(this.state.itrid,"ITR")}}>
                                                <Text style={styles.SignText}>Download</Text>
                                            </TouchableOpacity>
                                        </View>
                                        :null}
                                           
                                        </View>
                                    </View>
                                    <View style={styles.view2}>
                                                <Text style={{
                                                    color: '#000',
                                                    marginLeft: 21,
                                                    marginRight: 21,
                                                    fontWeight: 'bold',
                                                    fontSize: 20
                                                }}>
                                                    Comments
                                                </Text>
                                                <Text style={{
                                                    color: '#000',
                                                    marginLeft: 21,
                                                    marginRight: 21,
                                                    fontWeight: 'bold',
                                                    fontSize: 20
                                                }}>
                                                    {this.state.commentitr}
                                                </Text>

                                            </View>


                                </View>
                            </View>

                        </Tab>

                    </Tabs>
                </View>

            </SafeAreaView>
        );
    }
}


export default (Searchform)