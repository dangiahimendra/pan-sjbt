import * as React from 'react'
import {
    StyleSheet,
    Text,
    Dimensions,
    View,
    Image,
    CheckBox,
    AsyncStorage, ImageBackground, TextInput, TouchableOpacity, SafeAreaView, AlertIOS, ToastAndroid, ScrollView, FlatList, Platform
} from 'react-native';
import { Footer, Card } from 'native-base';
import AddEventActions from '../../actions/ActionAddEvent'
import * as shape from 'd3-shape'
import { connect } from 'react-redux';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import styles, { COLOR } from './styles';
import { Actions } from 'react-native-router-flux';
import AppHeader from '../../components/AppHeader'
import DatePicker from 'react-native-datepicker'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import ImagePicker from 'react-native-image-picker';

var token = ""

class Bussinessequiry extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            vanue: props.cityname,
            lat: props.lat,
            long: props.lng,
            start_date: '',
            end_date: '',
            title: '',
            description: '',
            adhar_front_image: null,
            adhar_front_image_path: '',
            adhar_back_image: null,
            adhar_back_image_path: '',
            Bpassbook_image: null,
            Bpassbook_image_path: '',
            pancard_image: null,
            pancard_image_path: '',
            name: '',
            email: '',
            contact: '',
            id: '',
            formsMulti: [],
            count: '',
            uploadimg: false,
            addf: false,
            termsAccepted: false,
            checked: false,
            indexcount: null
        };
    };

    
    
    
    validate = () => {

           if (this.state.name == "" || this.state.name == null || this.state.name== undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter name'); },
                android: () => { ToastAndroid.show('Please enter name', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.email == "" || this.state.email == null || this.state.email == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter email'); },
                android: () => { ToastAndroid.show('Please enter email', ToastAndroid.SHORT); }
            })();
            return false;
        }
        let rege = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (rege.test(this.state.email) === false) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter valid email'); },
                android: () => { ToastAndroid.show('Please enter valid email', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.contact == "" || this.state.contact == null || this.state.contact == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter contact'); },
                android: () => { ToastAndroid.show('Please enter contact', ToastAndroid.SHORT); }
            })();
            return false;
        }
        // /^[0]?[789]\d{9}$/
        // const reg =/^\d{10}$/;
        // if (reg.test(this.state.contact) === false) {
        //     Platform.select({
        //         ios: () => { AlertIOS.alert('Please enter valid contact'); },
        //         android: () => { ToastAndroid.show('Please enter valid contact', ToastAndroid.SHORT); }
        //     })();
        //     return false;
        // }
        
      
        else {
            this.businessequiry()
        }
    }
    

   

    getData = async () => {
        try {
            const value = await AsyncStorage.getItem('@storage_Key')
            if (value !== null) {
                // value previously stored
                console.log('aid', value)
                this.setState({
                    id: value
                });
            }
        } catch (e) {
            // error reading value
            console.log('error', e)
        }
    }
    async businessequiry() {
        console.log(this.state.id, this.state.formsMulti.name)
        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/businessEnquiry', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "name"			 : this.state.name,
	"email" 		 : this.state.email,
	"mobile"		 :this.state.contact,
	"message" 		 : "I want know about bussiness loan."
            })
        }).then((res) => res.json())
            .then(res => {
                console.log(res)
                if (res.result === "true" || res.result === true) {
                    Platform.select({
                        ios: () => { AlertIOS.alert(res.msg); },
                        android: () => { ToastAndroid.show(res.msg, ToastAndroid.SHORT); }
                    })();
                    //this.props.navigation.navigate("Paymentweb")

                } else {
                    alert("error")

                }
            })
            .catch((e) => {
                console.log(e)
            });

    };
   

    render() {
        console.log('uploadimg', this.state.uploadimg)
        console.log('addf', this.state.addf)
        console.log('Response = ', this.state.checked)
        return (
            <SafeAreaView style={styles.container}>
                <AppHeader
                    goBack={() => this.props.navigation.goBack()}
                    addevent={() => this.validate()}
                    leftImg={require('./../../components/Images/assets/icn_back.png')}
                    // rightImg={require('./../../components/Images/checked.png')}
                    headerTitle="Business Enquiry"
                />

                <ScrollView bounces={false} style={{ top: 0, }}>
                   
                            <KeyboardAwareScrollView
                                style={{ backgroundColor: 'transparent' }}
                                resetScrollToCoords={{ x: 0, y: 0 }}
                                contentContainerStyle={{ flex: 1 }}
                                scrollEnabled={true}
                                bounces={false}
                            >
                                <View style={{ flex: 1, margin: 6 }}>

                                    <View style={styles.textInputContainer} >

                                        <TextInput style={styles.textInputStyle}
                                            placeholder={'Enter Your Name'}
                                            placeholderTextColor={'gray'}
                                            returnKeyType={'default'}
                                            autoCapitalize={'none'}
                                            underlineColorAndroid={'transparent'}
                                            onChangeText={(name) => this.setState({ name })}
                                            editable={this.state.TextInputDisableStatus}
                                            value={this.state.name}
                                        />
                                        <Text style={styles.floatingText}>
                                            Enter Name
                            </Text>
                                    </View>

                                    <View style={styles.textInputContainer} >

                                        <TextInput style={styles.textInputStyle}
                                            placeholder={'Enter Your Email'}
                                            placeholderTextColor={'gray'}
                                            returnKeyType={'default'}
                                            autoCapitalize={'none'}
                                            underlineColorAndroid={'transparent'}
                                            onChangeText={(email) => this.setState({ email })}
                                            editable={this.state.TextInputDisableStatus}
                                            value={this.state.email}
                                        />
                                        <Text style={styles.floatingText}>
                                            Enter Email
</Text>
                                    </View>

                                    <View style={styles.textInputContainer} >

                                        <TextInput style={styles.textInputStyle}
                                            placeholder={'Enter Your Contact'}
                                            placeholderTextColor={'gray'}
                                            returnKeyType={'default'}
                                            autoCapitalize={'none'}
                                            maxLength={10}
                                            keyboardType='phone-pad'
                                            underlineColorAndroid={'transparent'}
                                            onChangeText={(contact) => this.setState({ contact })}
                                            editable={this.state.TextInputDisableStatus}
                                            value={this.state.contact}
                                        />
                                        <Text style={styles.floatingText}>
                                            Enter Contact
                                    </Text>

                                    </View>


                                   


                                    <View style={{ height: 20 }} />
                                </View>
                            </KeyboardAwareScrollView>
                        

                    

                    <View style={styles.View4}>
                        <TouchableOpacity onPress={() => { this.validate() }}>
                            <Text style={styles.SignText}>Submit</Text>
                        </TouchableOpacity>
                    </View>

                   
                </ScrollView>
                {/* <View style={styles.View3}>
                                <TouchableOpacity onPress={() => { this.validate() }}>
                                    <Text style={styles.SignText}>Submit</Text>
                                </TouchableOpacity>
                            </View> */}
            </SafeAreaView>


        );
    }
}


export default (Bussinessequiry)