import { StyleSheet, Dimensions } from 'react-native';
// import { Colors } from '../../Theme';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

export const COLOR = {
    RED: "#b71221",
    BLUE: '#ff6f61',
    BLACK: "#ffffff",
    WHITE: "#fff",
    MUDWHITE: "#F7F6F6",
    LIGHTGreen: "#d1f1fd",

};

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'transparent'
        // alignSelf:'center'
    },
    ViewImage: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 15,
    },
    View3: {
        borderRadius: 15,
        height: 40, alignSelf: 'center', top:5,
       width: width / 2.0, marginTop: (height * 0.26),
        justifyContent: 'center', backgroundColor: '#00b9f5'
    },
    SignText: {
        fontSize: 20, color: '#fff',
        textAlign: 'center', 
        backgroundColor: 'transparent'
    },
    textInputContainer: {
        // position: "absolute",
        alignSelf: "center",
        alignItems:"center",
        top: "10%",
        backgroundColor: "transparent",
        height: (width * 0.15) + 20,
        width: width / 1.1,
        marginLeft:10,
        marginRight:10
    },
    
    textInputStyle: {
        position: "absolute",
        top: 10,
        left:5,
        height: (width * 0.15),
        width: width / 1.1,
        borderColor: 'rgba(204,204,204,1)',
        borderWidth: 1,
        borderRadius: 5,
        padding: 10,
        fontSize: 18,
    },

    floatingText: {
        position: "absolute",
         backgroundColor: '#E6FDF8',
        color: "#012b72",
        fontWeight: "normal",
        fontSize: 16,
        fontWeight:'bold',
        height: 20,
        top: 0,
        left: 10,
        paddingLeft: 3,
        paddingRight: 3,
    },

});
