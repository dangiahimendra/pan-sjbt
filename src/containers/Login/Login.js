// import React, { Component } from 'react';
import * as React from 'react'
import LoginActions from '../../actions/ActionLogin';
import { connect } from 'react-redux';
import {
    StyleSheet,
    Text,
    Dimensions,
    View,
    Image,
    ImageBackground, TextInput, TouchableOpacity, Platform, AlertIOS, ToastAndroid, KeyboardAvoidingView, Alert
} from 'react-native';
import Spinner from '../Spinner'

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import styles, { COLOR } from './styles';
import Loader from '../../constants/Loader';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-community/async-storage';
//import Spinner from '../../Components/Spinner';


//import console = require('console');

class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            code:"",
            isLoading: false,

        };

    };


    validate = () => {
        if (this.state.code == "" || this.state.code == null || this.state.code == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter Agent Code'); },
                android: () => { ToastAndroid.show('Please enter Agent Code', ToastAndroid.SHORT); }
            })();
            return false;
        }
        // var text = this.state.email;
        // let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        // if (reg.test(text) === false) {
        //     Platform.select({
        //         ios: () => { AlertIOS.alert('Agent Code is Not Correct'); },
        //         android: () => { ToastAndroid.show('Agent Code is Not Correct', ToastAndroid.SHORT); }
        //     })();
        //     return false;
        // }
        // if (this.state.password == "" || this.state.password == null || this.state.password == undefined) {
        //     Platform.select({
        //         ios: () => { AlertIOS.alert('Please enter Agent Code'); },
        //         android: () => { ToastAndroid.show('Please enter Agent Code', ToastAndroid.SHORT); }
        //     })();
        //     return false;
        // }

        else {
            this.login()
        }
    }
    storeData = async (data) => {
        try {
          await AsyncStorage.setItem('@storage_Key', data)
        } catch (e) {
          // saving error
        }
      }
      storeEmail = async (data) => {
        try {
          await AsyncStorage.setItem('@storage_Email', data)
        } catch (e) {
          // saving error
        }
      }
    //---this function check for difficulty steps----//
    async login() {
        console.log(this.state.code)
        this.setState({ isLoading: true })

        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/login', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "agent_code"    : this.state.code,
                "device_type"   : "android",
                "device_token"  : "cwjwpt44jwfn20wmgspwrt39ow5jw42jdow934kfng835wdkk34gdxf"

            })
        }).then((res) => res.json())
            .then(res => {
                console.log(res)
                if (res.result === "true" || res.result === true) {
                    this.setState({
                        isLoading: false
                      })
                   console.log('id',res.data.agent_id)
                   console.log('id',res.data.agent_email)

                    this.storeData(res.data.agent_id)
                    this.storeEmail(res.data.agent_email)

                    this.props.navigation.navigate("Home") 
                    //Actions.Spinner(false)
                    // dispatch(loginIsLoading(false));
                    // dispatch(loginHasError(false));
                    // dispatch(isLogged(true));
                    // dispatch(loginData(res));  
                    // Actions.Home();

                 } else {
                     alert(res.msg)
                     this.setState({ isLoading: false })

                    // dispatch(loginIsLoading(false));
                    // setTimeout(() => {
                    //     dispatch(loginHasError(res.message));
                    //     dispatch(loginHasError(undefined));
                    // }, 1000);
                }
            })
            .catch((e) => {
                console.log(e)
                this.setState({ isLoading: false })
            });
    
    };

    render() {
        const { hasError, isLogged, isLoading, logindata } = this.props;
        console.log('status1111', logindata)
        const { email, password } = this.state
        if (hasError !== false && hasError !== undefined) {
            alert(hasError);
            
        }

        return (
          
                <LinearGradient colors={['#d1f1fd', '#fff']} style={{ flex: 1, height: '30%' }}>
                <KeyboardAvoidingView style={styles.container} >
                    <View style={styles.container}>
                        <Loader loading={isLoading} />
                        <View style={styles.ViewImage}>
                        <Image  style={{ 
                            height: 110,
                           width: 110,
                           marginTop:"20%"
                        }}
                        resizeMode="cover"
                           source={require('./../../Images/logom.png')} />
                        </View>

                        <View style={styles.textInputContainer} >

                            <TextInput style={styles.textInputStyle}
                                placeholder={'Agent Code'}
                                placeholderTextColor={'gray'}
                                returnKeyType={'default'}
                                autoCapitalize={'none'}
                                underlineColorAndroid={'transparent'}
                                onChangeText={(text) => this.setState({ code:text })}
                               // editable={this.state.TextInputDisableStatus}
                                value={this.state.code}
                            />
                            <Text style={styles.floatingText}>
                                
                                Enter Agent Code
                            </Text>

                        </View>

                            <View style={styles.View3}>
                                <TouchableOpacity
                                //  onPress={() => {  this.props.navigation.navigate("Home")  }}
                                 onPress={() => {  this.validate()}}
                                 >
                                <Text style={styles.SignText}>Sign In</Text>
                                </TouchableOpacity>
                            </View>

                      

                    </View>
                    </KeyboardAvoidingView>
                    {this.state.isLoading &&
          <Spinner />
        }
                </LinearGradient>
           
        );
    }
}
const mapStateToProps = (state) => {
    return {
        isLogged: state.login.isLogged,
        hasError: state.login.hasError,
        isLoading: state.login.isLoading,
        logindata: state.login.logindata,


    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        loginApi: (code) => dispatch(LoginActions.loginApi(code))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);