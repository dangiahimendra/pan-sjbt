import * as React from 'react'
import {
    StyleSheet,
    Text,
    Dimensions,
    View,
    Image,
    Button,
    AsyncStorage, ImageBackground, TextInput, Fragment, TouchableOpacity, SafeAreaView, AlertIOS, ToastAndroid, ScrollView, FlatList, Platform
} from 'react-native';
import { Footer, Card } from 'native-base';
import AddEventActions from '../../actions/ActionAddEvent'
import * as shape from 'd3-shape'
import { connect } from 'react-redux';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import styles, { COLOR } from './styles';
import { Actions } from 'react-native-router-flux';
import AppHeader from '../../components/AppHeader'
import DatePicker from 'react-native-datepicker'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import ImagePicker from 'react-native-image-picker';

var token = ""

class Multiple extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            vanue: props.cityname,
            lat: props.lat,
            long: props.lng,
            start_date: '',
            end_date: '',
            title: '',
            description: '',
            adhar_front_image: null,
            adhar_front_image_path: '',
            adhar_back_image: null,
            adhar_back_image_path: '',
            dlicencef_image: null,
            dlicencef_image_path: '',
            dlicenceb_image: null,
            dlicenceb_image_path: '',
            pimage: null,
            pimage_path: '',
            name: '',
            email: '',
            contact: '',
            id: '',
            formsMulti: [],
            count: '',
            image: ''
        };

        this.selectPhotoTappedAadhar_f = this.selectPhotoTappedAadhar_f.bind(this);
        this.uploadImageAf = this.uploadImageAf.bind(this);
        this.selectPhotoTappedAdhar_b = this.selectPhotoTappedAdhar_b.bind(this);
        this.selectPhotoTappedDriving_l_f = this.selectPhotoTappedDriving_l_f.bind(this);
        this.selectPhotoTappedDriving_l_b = this.selectPhotoTappedDriving_l_b.bind(this);
        this.selectPhotoTappedpimage = this.selectPhotoTappedpimage.bind(this);



    };

    componentDidMount() {
        // this.addForms();
        console.log('componentDidMount')

    }

    addForms(count) {
        for (i = 1; i <= count; i++) {
            let formData = {
                "aadhar_front": "",
                "aadhar_back": "",
                "d_licence_front": "",
                "d_licence_back": "",
                "image": "",
                "name": "",
                "email": "",
                "mobile": ""
            }

            this.state.formsMulti.push(formData);

        }
        console.log('datalist', this.state.formsMulti)


    }

    openSearchModal() {
        Actions.SearchPlaces("AddEvent")
    }
    selectPhotoTappedAadhar_f(index) {
        console.log(index);
        ImagePicker.showImagePicker((response) => {
            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                console.log('source', source)
                console.log('source', response.uri)
                this.uploadImageAf(response.uri, index)
            }
        });
    }
    selectPhotoTappedAdhar_b(index) {
        ImagePicker.showImagePicker((response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                this.uploadImageAb(response.uri,index)

                this.setState({
                    adhar_back_image: response.uri
                });
                console.log("hsdjyafsduya")

            }
        });
    }
    selectPhotoTappedDriving_l_f(index) {
        ImagePicker.showImagePicker((response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                this.uploadImagedrivel_f(response.uri,index)

                this.setState({
                    dlicencef_image: response.uri
                });
                console.log("hsdjyafsduya")
            }
        });
    }
    selectPhotoTappedDriving_l_b(index) {
        ImagePicker.showImagePicker((response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                this.uploadImagedrivel_b(response.uri,index)

                this.setState({
                    dlicenceb_image: response.uri
                });
                console.log("hsdjyafsduya")
                // this.Image()
            }
        });
    }
    selectPhotoTappedpimage(index) {
        ImagePicker.showImagePicker((response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                this.uploadImagePimage(response.uri,index)

                this.setState({
                    pimage: response.uri
                });
                console.log("hsdjyafsduya")
            }
        });
    }

    validate = () => {
        // for (i = 0; i <= this.state.formsMulti.length; i++) {
        // //     if(this.state.formsMulti[i].name!=undefined){
        // //   console.log('datalist_name', this.state.formsMulti[i].name)
        // //     }
        //     if (this.state.formsMulti[i].name == "" || this.state.formsMulti[i].name == null || this.state.formsMulti[i].name == undefined) {
        //         Platform.select({
        //             ios: () => { AlertIOS.alert('Please enter name'); },
        //             android: () => { ToastAndroid.show('Please enter name', ToastAndroid.SHORT); }
        //         })();
        //         return false;
        //     }
        //   }
        if (this.state.name == "" || this.state.name == null || this.state.formsMulti.name == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter name'); },
                android: () => { ToastAndroid.show('Please enter name', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.email == "" || this.state.email == null || this.state.email == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter email'); },
                android: () => { ToastAndroid.show('Please enter email', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.contact == "" || this.state.contact == null || this.state.contact == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter contact'); },
                android: () => { ToastAndroid.show('Please enter contact', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.adhar_front_image_path == "" || this.state.adhar_front_image_path == null || this.state.adhar_front_image_path == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload Aadhar Card'); },
                android: () => { ToastAndroid.show('Please upload Aadhar Card', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.dlicenceb_image_path == "" || this.state.dlicenceb_image_path == null || this.state.dlicenceb_image_path == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload DrivingLicense'); },
                android: () => { ToastAndroid.show('Please upload DrivingLicense', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.dlicencef_image_path == "" || this.state.dlicencef_image_path == null || this.state.dlicencef_image_path == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload DrivingLicense'); },
                android: () => { ToastAndroid.show('Please DrivingLicense', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.pimage_path == "" || this.state.pimage_path == null || this.state.pimage_path == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload Photo'); },
                android: () => { ToastAndroid.show('Please upload Photo', ToastAndroid.SHORT); }
            })();
            return false;
        }

        else {
            this.applyForPAN()
        }
    }
    async uploadImageAf(image, index) {
        var file = {
            uri: image,
            type: 'image/png',
            name: 'profile_image'
        };
        var data = new FormData();
        data.append('file', file);
        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/upload_documents?type=aadhar_card', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body: data
        }).then((res) => res.json())
            .then(res => {
                console.log('', res)
                this.state.formsMulti[index].aadhar_front = res.data;
                //this.state.formsMulti[index].adhar_front_show = res.data;
                console.log(this.state.formsMulti[index].aadhar_front);

                // this.setState({
                //     adhar_front_image_path: res.path
                // });
                // this.setState({
                //     adhar_front_image: res.data
                // });
            })
            .catch((e) => {
                console.log(e)
                Platform.select({
                    ios: () => { AlertIOS.alert('Uploading Failed'); },
                    android: () => { ToastAndroid.show('Uploading Failed', ToastAndroid.SHORT); }
                })();
            });

    };
    async uploadImageAb(image,index) {
        console.log('imagef', image)

        var file = {
            uri: image,
            type: 'image/png',
            name: 'profile_image'
        };
        var data = new FormData();
        data.append('file', file);


        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/upload_documents?type=aadhar_card', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body: data
        }).then((res) => res.json())
            .then(res => {
                console.log('', res)
                this.state.formsMulti[index].aadhar_back=res.data
                // this.setState({
                //     adhar_back_image_path: res.path
                // });

            })
            .catch((e) => {
                console.log(e)
                Platform.select({
                    ios: () => { AlertIOS.alert('Uploading Failed'); },
                    android: () => { ToastAndroid.show('Uploading Failed', ToastAndroid.SHORT); }
                })();
            });

    };
    async uploadImagedrivel_f(image,index) {
        console.log('imagef', image)

        var file = {
            uri: image,
            type: 'image/png',
            name: 'profile_image'
        };
        var data = new FormData();
        data.append('file', file);


        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/upload_documents?type=driving_licence', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body: data
        }).then((res) => res.json())
            .then(res => {
                console.log('', res)
                this.state.formsMulti[index].d_licence_front=res.data

                // this.setState({
                //     dlicencef_image_path: res.path
                // });

            })
            .catch((e) => {
                console.log(e)
                Platform.select({
                    ios: () => { AlertIOS.alert('Uploading Failed'); },
                    android: () => { ToastAndroid.show('Uploading Failed', ToastAndroid.SHORT); }
                })();
            });

    };
    async uploadImagedrivel_b(image,index) {
        console.log('imagef', image)

        var file = {
            uri: image,
            type: 'image/png',
            name: 'profile_image'
        };
        var data = new FormData();
        data.append('file', file);


        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/upload_documents?type=driving_licence', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body: data
        }).then((res) => res.json())
            .then(res => {
                console.log('', res)
                this.state.formsMulti[index].d_licence_back=res.data

                // this.setState({
                //     dlicenceb_image_path: res.path
                // });

            })
            .catch((e) => {
                console.log(e)
                Platform.select({
                    ios: () => { AlertIOS.alert('Uploading Failed'); },
                    android: () => { ToastAndroid.show('Uploading Failed', ToastAndroid.SHORT); }
                })();
            });

    };
    async uploadImagePimage(image,index) {
        console.log('imagef', image)

        var file = {
            uri: image,
            type: 'image/png',
            name: 'profile_image'
        };
        var data = new FormData();
        data.append('file', file);


        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/upload_documents?type=image', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body: data
        }).then((res) => res.json())
            .then(res => {
                console.log('', res)
                this.state.formsMulti[index].image=res.data
                // this.setState({
                //     pimage_path: res.path
                // });

            })
            .catch((e) => {
                console.log(e)
                Platform.select({
                    ios: () => { AlertIOS.alert('Uploading Failed'); },
                    android: () => { ToastAndroid.show('Uploading Failed', ToastAndroid.SHORT); }
                })();
            });

    };

    AddEvent() {
        const params = { vanue, lat, long, start_date, end_date, title, description } = this.state
        this.props.AddeventApi(params);
        Actions.Home()
    }
    componentWillMount() {
        // this.setState({id:AsyncStorage.getItem('@agent_id')})
        // count:this.props.navigation.state.params.num
        this.getData()
        // console.log('componentWillMount',this.props.navigation.state.params.num)

        if (this.props.navigation.state.params.num != null) {
            // console.log('componentWillMount',this.props.navigation.state.params.num)
            this.addForms(this.props.navigation.state.params.num);
        }


        console.log('componentWillMount')


    }
    getData = async () => {
        try {
            const value = await AsyncStorage.getItem('@storage_Key')
            if (value !== null) {
                // value previously stored
                console.log('aid', value)
                this.setState({
                    id: value
                });
            }
        } catch (e) {
            // error reading value
            console.log('error', e)
        }
    }
    async applyForPAN() {
        console.log('call api', this.state.id,
            this.state.formsMulti)
        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/applyForPAN', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "agent_id": this.state.id,
                "amount": "410",
                "documents": this.state.formsMulti

            })
        }).then((res) => res.json())
            .then(res => {
                console.log(res)
                if (res.result === "true" || res.result === true) {
                    Platform.select({
                        ios: () => { AlertIOS.alert(res.msg); },
                        android: () => { ToastAndroid.show(res.msg, ToastAndroid.SHORT); }
                    })();
                    this.props.navigation.navigate("Home")

                } else {
                    alert("error")

                }
            })
            .catch((e) => {
                console.log(e)
            });

    };
    fillForm1(index) {
        // console.log(value + " " + index);

        this.state.formsMulti[index].aadhar_front = this.state.adhar_front_image_path;

        console.log('fillf', this.state.formsMulti);


    }

    render() {

        console.log('render')
      
        return (
            <SafeAreaView style={styles.container}>
                <AppHeader
                    goBack={() => this.props.navigation.goBack()}
                    addevent={() => this.validate()}
                    leftImg={require('./../../components/Images/assets/icn_back.png')}
                    headerTitle="Multiple"
                />


                <ScrollView bounces={false} style={{ top: 0, }}>
                    <FlatList
                        data={this.state.formsMulti}
                        renderItem={({ item, index }) => (

                            <KeyboardAwareScrollView
                                style={{ backgroundColor: 'transparent' }}
                                resetScrollToCoords={{ x: 0, y: 0 }}
                                contentContainerStyle={{ flex: 1 }}
                                scrollEnabled={true}
                                bounces={false}
                            >

                                <View style={{ flex: 1, }}>
                                    <Text style={{ color: 'black', justifyContent: 'center', alignItems: 'center', textAlign: 'center', marginTop: 5 }}> FORM {index + 1}</Text>

                                    <View style={styles.textInputContainer} >

                                        <TextInput style={styles.textInputStyle}
                                            placeholder={'Enter Your Name'}
                                            placeholderTextColor={'gray'}
                                            returnKeyType={'default'}
                                            autoCapitalize={'none'}
                                            underlineColorAndroid={'transparent'}
                                            onChangeText={(name) => { this.state.formsMulti[index].name = name }}
                                            value={this.state.formsMulti.name}
                                            editable={this.state.TextInputDisableStatus}
                                        />
                                        <Text style={styles.floatingText}>
                                            Enter Name
                            </Text>
                                    </View>

                                    <View style={styles.textInputContainer} >

                                        <TextInput style={styles.textInputStyle}
                                            placeholder={'Enter Your Email'}
                                            placeholderTextColor={'gray'}
                                            returnKeyType={'default'}
                                            autoCapitalize={'none'}
                                            underlineColorAndroid={'transparent'}
                                            onChangeText={(email) => { this.state.formsMulti[index].email = email }}
                                            editable={this.state.TextInputDisableStatus}
                                            value={this.state.formsMulti.email}
                                        />
                                        <Text style={styles.floatingText}>
                                            Enter Email
                            </Text>
                                    </View>

                                    <View style={styles.textInputContainer} >

                                        <TextInput style={styles.textInputStyle}
                                            placeholder={'Enter Your Contact'}
                                            placeholderTextColor={'gray'}
                                            returnKeyType={'default'}
                                            autoCapitalize={'none'}
                                            underlineColorAndroid={'transparent'}
                                            onChangeText={(mobile) => { this.state.formsMulti[index].mobile = mobile }}
                                            editable={this.state.TextInputDisableStatus}
                                            value={this.state.formsMulti.mobile}
                                        />
                                        <Text style={styles.floatingText}>
                                            Enter Contact
                                    </Text>

                                    </View>

                                    <View style={styles.imageContainer} >

                                        <Text style={styles.floatingText}>
                                            Upload Your AddharCard
                                    </Text>

                                        <View style={{ flexDirection: 'row' }}>

                                            <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop: "2%", position: 'absolute', left: 20 }} >
                                                {
                                                    (this.state.formsMulti[index].aadhar_front)
                                                        ? <TouchableOpacity onPress={() => this.selectPhotoTappedAadhar_f(index)}>
                                                            <Image source={{ uri: this.state.formsMulti[index].aadhar_front }} style={{ width: 100, height: 50, borderRadius: 2 }} />
                                                        </TouchableOpacity>
                                                        : <TouchableOpacity style={styles.icon} onPress={() => this.selectPhotoTappedAadhar_f(index)}>
                                                            <Image
                                                                resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                                source={require('./../../Images/camra.png')} />
                                                        </TouchableOpacity>
                                                }


                                            </View>

                                            <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop: "2%", position: 'absolute', right: 20 }} >
                                                {
                                                    this.state.formsMulti[index].aadhar_back
                                                        ? <TouchableOpacity onPress={() => this.selectPhotoTappedAdhar_b(index)}>
                                                            <Image source={{ uri: this.state.formsMulti[index].aadhar_back }} style={{ width: 100, height: 50, borderRadius: 2, marginRight: 20 }} />
                                                        </TouchableOpacity>
                                                        : <TouchableOpacity style={styles.icon} onPress={() => this.selectPhotoTappedAdhar_b(index)}>
                                                            <Image
                                                                resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                                // source={{ uri: this.state.sendImageUrl }} />
                                                                source={require('./../../Images/camra.png')} />
                                                        </TouchableOpacity>
                                                }
                                            </View>

                                        </View>

                                    </View>

                                    <View style={styles.imageContainer2} >

                                        <Text style={styles.floatingText}>
                                            Upload Your DrivingLicense
                                    </Text>

                                        <View style={{ flexDirection: 'row' }}>

                                            <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop: "2%", position: 'absolute', left: 20 }} >


                                                {
                                                    this.state.formsMulti[index].d_licence_front
                                                        ? <TouchableOpacity onPress={() => this.selectPhotoTappedDriving_l_f(index)}>
                                                            <Image source={{ uri: this.state.formsMulti[index].d_licence_front }} style={{ width: 100, height: 50, borderRadius: 2 }} />
                                                        </TouchableOpacity>
                                                        : <TouchableOpacity style={styles.icon} onPress={() => this.selectPhotoTappedDriving_l_f(index)}>
                                                            <Image
                                                                resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                                // source={{ uri: this.state.sendImageUrl }} />
                                                                source={require('./../../Images/camra.png')} />
                                                        </TouchableOpacity>
                                                }


                                            </View>

                                            <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop: "2%", position: 'absolute', right: 20 }} >
                                                {
                                                    this.state.formsMulti[index].d_licence_back
                                                        ? <TouchableOpacity onPress={() => this.selectPhotoTappedDriving_l_b(index)}>
                                                            <Image source={{ uri: this.state.formsMulti[index].d_licence_back }} style={{ width: 100, height: 50, borderRadius: 2, marginRight: 20 }} />
                                                        </TouchableOpacity>
                                                        : <TouchableOpacity style={styles.icon} onPress={() => this.selectPhotoTappedDriving_l_b(index)}>
                                                            <Image
                                                                resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                                // source={{ uri: this.state.sendImageUrl }} />
                                                                source={require('./../../Images/camra.png')} />
                                                        </TouchableOpacity>
                                                }
                                            </View>

                                        </View>

                                    </View>

                                    <View style={styles.imageContainer3} >

                                        <Text style={styles.floatingText}>
                                            Upload Your Photo
                                </Text>
                                        <View style={{ height: 15, width: 10, justifyContent: 'center', alignSelf: 'center', marginTop: "7%", alignItems: 'center' }} >
                                            {
                                                this.state.formsMulti[index].image
                                                    ? <TouchableOpacity onPress={() => this.selectPhotoTappedpimage(index)}>
                                                        <Image source={{ uri: this.state.formsMulti[index].image }} style={{ width: 100, height: 50, borderRadius: 2, marginBottom: 6 }} />
                                                    </TouchableOpacity>
                                                    : <TouchableOpacity style={styles.icon} onPress={() => this.selectPhotoTappedpimage()}>
                                                        <Image
                                                            resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                            // source={{ uri: this.state.sendImageUrl }} />
                                                            source={require('./../../Images/camra.png')} />
                                                    </TouchableOpacity>
                                            }
                                        </View>
                                    </View>



                                    <View style={{ height: 20 }} />
                                </View>
                            </KeyboardAwareScrollView>
                        )}
                        keyExtractor={(item, index) => index}
                    />
                    {/* <View style={styles.View4}>
                                             <TouchableOpacity onPress={() => { this.addForms() }}>
                                                <Text style={styles.SignText}>Add More</Text>
                                             </TouchableOpacity>
                                         </View> */}

                    <View style={styles.View4}>
                        <TouchableOpacity onPress={() => { this.applyForPAN() }}>
                            <Text style={styles.SignText}>Upload</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.View5}>
                        <TouchableOpacity onPress={() => { this.props.navigation.navigate("Home") }}>
                            <Text style={styles.SignText}>Proceed To Pay</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                {/* <View style={styles.View4}>
                                             <TouchableOpacity onPress={() => { this.addForms() }}>
                                                <Text style={styles.SignText}>Add More</Text>
                                             </TouchableOpacity>
                                         </View>

                <View style={styles.View4}>
                                             <TouchableOpacity onPress={() => { this.validate() }}>
                                                <Text style={styles.SignText}>Upload</Text>
                                             </TouchableOpacity>
                                         </View>

                                         <View style={styles.View5}>
                                             <TouchableOpacity onPress={() => { this.props.navigation.navigate("Home") }}>
                                                 <Text style={styles.SignText}>Proceed To Pay</Text>
                                             </TouchableOpacity>
                                         </View> */}

            </SafeAreaView>


        );
    }
}


export default (Multiple)
