import * as React from 'react'
import {
    StyleSheet,
    Text,
    Dimensions,
    View,
    Image,
    AsyncStorage, ImageBackground,
    TextInput, TouchableOpacity,
    SafeAreaView, AlertIOS, 
    ToastAndroid, ScrollView,
     FlatList, Platform
} from 'react-native';
import { Footer, Card } from 'native-base';
import AddEventActions from '../../actions/ActionAddEvent'
import * as shape from 'd3-shape'
import { connect } from 'react-redux';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import styles, { COLOR } from './styles';
import { Actions } from 'react-native-router-flux';
import AppHeader from '../../components/AppHeader'
import DatePicker from 'react-native-datepicker'

var token = ""
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Dropdown } from 'react-native-material-dropdown';


let Employ = [{
    value: 'Salaried',
  }, {
    value: 'Self Employed',
  }, {
    value: 'Self-Employed DR/CA',
  }];

//   let Loan = [{
//     value: ' Personal Loan',
//   }, {
//     value: 'Home Loan',
//   }, {
//     value: 'Mortgage Loan',
//   }, {
//     value: 'Business Loan',
//   },
// ];

class LoanEnquiry extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            vanue: props.cityname,
            lat: props.lat,
            long: props.lng,
            start_date: '',
            end_date: '',
            title: '',
            description: '',
            loan_name: '',
            Loan: [],
            Employee_type: [],
            loan:'',
            ename:'',

            name:'',
            email:'',
            contact:'',
            pincode:'',
            company_name:'',
            salary_in_hand:'',
            address:''

        };
        console.log(props),
        this.getLoanType();
        this.getEmployeeType();
    };

    openSearchModal() {
        Actions.SearchPlaces("AddEvent")
    }

    validate = () => {
        if (this.state.name == "" || this.state.name == null || this.state.name == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter name'); },
                android: () => { ToastAndroid.show('Please enter name', ToastAndroid.SHORT); }
            })();
            return false;
        }
        
        if (this.state.email == "" || this.state.email == null || this.state.email == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter email'); },
                android: () => { ToastAndroid.show('Please enter email', ToastAndroid.SHORT); }
            })();
            return false;
        }
        let rege = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (rege.test(this.state.email) === false) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter valid email'); },
                android: () => { ToastAndroid.show('Please enter valid email', ToastAndroid.SHORT); }
            })();
            return false;
        }
       
        if (this.state.contact == "" || this.state.contact == null || this.state.contact == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter contact'); },
                android: () => { ToastAndroid.show('Please enter contact', ToastAndroid.SHORT); }
            })();
            return false;
        }
        // const reg = /^[0]?[789]\d{9}$/;
        // if (reg.test(this.state.contact) === false ) {
        //     Platform.select({
        //         ios: () => { AlertIOS.alert('Please enter valid contact'); },
        //         android: () => { ToastAndroid.show('Please enter valid contact', ToastAndroid.SHORT); }
        //     })();
        //     return false;
        // }
        // if (this.state.pincode == "" || this.state.pincode == null || this.state.pincode == undefined) {
        //     Platform.select({
        //         ios: () => { AlertIOS.alert('Please enter pincode'); },
        //         android: () => { ToastAndroid.show('Please enter pincode', ToastAndroid.SHORT); }
        //     })();
        //     return false;
        // }
        // if (this.state.company_name == "" || this.state.company_name == null || this.state.company_name == undefined) {
        //     Platform.select({
        //         ios: () => { AlertIOS.alert('Please enter company name'); },
        //         android: () => { ToastAndroid.show('Please enter company name', ToastAndroid.SHORT); }
        //     })();
        //     return false;
        // }
        // if (this.state.salary_in_hand == "" || this.state.salary_in_hand == null || this.state.salary_in_hand == undefined) {
        //     Platform.select({
        //         ios: () => { AlertIOS.alert('Please enter salary'); },
        //         android: () => { ToastAndroid.show('Please enter salary', ToastAndroid.SHORT); }
        //     })();
        //     return false;
        // }
    
        else {
            this.loanEquiry()
        }
    }
    async loanEquiry() {
        console.log(this.state.name,this.state.email,this.state.contact,this.state.company_name
            ,this.state.salary_in_hand,this.state.pincode,this.state.loan,this.state.ename)
        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/loanEnquiry', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "name"			 :this.state.name,
                "email" 		 : this.state.email,
                "mobile"		 : this.state.contact,
                "company_name"   : this.state.company_name,
                "salary_in_hand" : this.state.salary_in_hand,
                "pincode"        : this.state.pincode,
                "loan_type"      : this.state.loan,
                "employee_type"  : this.state.ename,
                "message" 		 : "I want know about bussiness loan.",
                "address"        : this.state.address

            })
        }).then((res) => res.json())
            .then(res => {
                console.log(res)
                if (res.result === "true" || res.result === true) {
                    Platform.select({
                        ios: () => { AlertIOS.alert(res.msg); },
                        android: () => { ToastAndroid.show(res.msg, ToastAndroid.SHORT); }
                    })();
                    this.props.navigation.navigate("Home") 
                  

                 } else {
                     alert(res.msg)
                  
                }
            })
            .catch((e) => {
                console.log(e)
            });
    
    };
    AddEvent() {
        const params = { vanue, lat, long, start_date, end_date, title, description } = this.state
        this.props.AddeventApi(params);
        Actions.Home()
    }

    async getLoanType() {
        console.log(this.state.code)
        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/getLoanType', {
            method: 'GET',
        }).then((res) => res.json())
            .then(res => {
                console.log(res)
                if (res.result === "true" || res.result === true) {
                    // alert("fine good")
                    console.log(res.data)
                    let respo = JSON.stringify(res.data);
                    let mylons = [];
                    for(let item of JSON.parse(respo)){
                        mylons.push({id: item.id, value: item.loan_name});
                    }
                    console.log(mylons)
                    this.setState({
                        Loan: mylons,
                    });

                 } else {
                     alert("error")
                }
            })
            .catch((e) => {
                console.log(e)
            });
    };
    
    async getEmployeeType() {
        console.log(this.state.code)
        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/getEmployeeType', {
            method: 'GET',
        }).then((res) => res.json())
            .then(res => {
                console.log(res)
                if (res.result === "true" || res.result === true) {
                    // alert("fine good")
                    console.log(res.data)
                    let respo = JSON.stringify(res.data);
                    let mylons = [];
                    for(let item of JSON.parse(respo)){
                        mylons.push({id: item.id, value: item.employee_type_name});
                    }
                    console.log(mylons)
                    this.setState({
                        Employee_type: mylons,
                    });

                 } else {
                     alert("error")
                }
            })
            .catch((e) => {
                console.log(e)
            });
    };
    render() {
          
        return (
            <SafeAreaView style={styles.container}>
                <AppHeader
                    goBack={() => this.props.navigation.goBack()}
                    addevent={() => this.validate()}
                    leftImg={require('./../../components/Images/assets/icn_back.png')}
                    // rightImg={require('./../../components/Images/checked.png')}
                    headerTitle="Loan Enquiry"
                />
                 <ScrollView bounces={false} style={{ top: 0, }}>

                <KeyboardAwareScrollView
                    style={{ backgroundColor: '#transparent' }}
                    resetScrollToCoords={{ x: 0, y: 0 }}
                    contentContainerStyle={styles.container}
                    scrollEnabled={true}
                    bounces={false}
                >
                    <View style={styles.eventcontainer} >
                        <View style={styles.textInputContainer} >

                            <TextInput style={styles.textInputStyle}
                                placeholder={'Full name'}
                                placeholderTextColor={'gray'}
                                returnKeyType={'default'}
                                autoCapitalize={'none'}
                                underlineColorAndroid={'transparent'}
                                onChangeText={(name) => this.setState({ name })}
                                editable={this.state.TextInputDisableStatus}
                                value={this.state.name}
                            />
                            <Text style={styles.floatingText}>
                               Name*
                   </Text>

                        </View>

                        <View style={styles.textInputContainer} >

                            <TextInput style={styles.textInputStyle}
                                placeholder={'Email'}
                                placeholderTextColor={'gray'}
                                returnKeyType={'default'}
                                autoCapitalize={'none'}
                                underlineColorAndroid={'transparent'}
                                onChangeText={(email) => this.setState({ email })}
                                editable={this.state.TextInputDisableStatus}
                                value={this.state.email}
                            />
                            <Text style={styles.floatingText}>
                                Email* 
                            </Text>

                        </View>

                        <View style={styles.textInputContainer} >

                            <TextInput style={styles.textInputStyle}
                                placeholder={'Current residence pincode optional'}
                                placeholderTextColor={'gray'}
                                returnKeyType={'default'}
                                autoCapitalize={'none'}
                                underlineColorAndroid={'transparent'}
                                onChangeText={(pincode) => this.setState({ pincode })}
                                editable={this.state.TextInputDisableStatus}
                                value={this.state.pincode}
                            />
                            <Text style={styles.floatingText}>
                                Current residence pincode
                            </Text>

                        </View>

                        <View style={styles.textInputContainer} >

                            <TextInput style={styles.textInputStyle}
                                placeholder={'Current company name optional'}
                                placeholderTextColor={'gray'}
                                returnKeyType={'default'}
                                autoCapitalize={'none'}
                                underlineColorAndroid={'transparent'}
                                onChangeText={(company_name) => this.setState({ company_name })}
                                editable={this.state.TextInputDisableStatus}
                                value={this.state.company_name}
                            />
                            <Text style={styles.floatingText}>
                                Current company name
                            </Text>

                        </View>

                        <View style={styles.textInputContainer} >

                            <TextInput style={styles.textInputStyle}
                                placeholder={'Monthly in-hand salary optional'}
                                placeholderTextColor={'gray'}
                                returnKeyType={'default'}
                                autoCapitalize={'none'}
                                underlineColorAndroid={'transparent'}
                                onChangeText={(salary_in_hand) => this.setState({ salary_in_hand })}
                                editable={this.state.TextInputDisableStatus}
                                value={this.state.salary_in_hand}
                            />
                            <Text style={styles.floatingText}>
                                Monthly in-hand salary
                        </Text>

                        </View>

                        <View style={styles.textInputContainer} >

                            <TextInput style={styles.textInputStyle}
                                placeholder={'Mobile number'}
                                placeholderTextColor={'gray'}
                                returnKeyType={'default'}
                                autoCapitalize={'none'}
                                keyboardType='phone-pad'
                                maxLength={10}
                                underlineColorAndroid={'transparent'}
                                onChangeText={(contact) => this.setState({ contact })}
                                editable={this.state.TextInputDisableStatus}
                                value={this.state.contact}
                            />
                            <Text style={styles.floatingText}>
                                Mobile number*
                            </Text>

                        </View>
                        <View style={styles.textInputContainer} >

                            <TextInput style={styles.textInputStyle}
                                placeholder={'Address optional'}
                                placeholderTextColor={'gray'}
                                returnKeyType={'default'}
                                autoCapitalize={'none'}
                                underlineColorAndroid={'transparent'}
                                onChangeText={(address) => this.setState({ address })}
                                editable={this.state.TextInputDisableStatus}
                                value={this.state.address}
                            />
                            <Text style={styles.floatingText}>
                                Address
                            </Text>

                        </View>
                   
                      <View style={{ height: (width * 0.14) + 2, width: width / 1.1, borderWidth:.8,
                                     borderColor:'gray',justifyContent:"center",
                                     borderRadius:5, marginTop:30, alignSelf:'center'}}>  
                                      {
                            (this.state.Employee_type) &&
                        <Dropdown style={{marginLeft:10}}
                        dropdownOffset={{top:0}}
                        placeholder={"Select type"}
                        data={this.state.Employee_type}
                        onChangeText={(ename) => this.setState({ ename })}
                        value={this.state.ename}
                        dropdownMargins={{min:16, max:16} }

                inputContainerStyle={{ borderBottomColor: 'transparent' }} 
                        
                    />
                }
                <Text style={styles.floatingText1}>
                Employee Type
                            </Text>
                      </View>

                      <View style={{ height: (width * 0.14) + 2, width: width / 1.1, borderWidth:.8,
                        borderColor:'gray',justifyContent:"center",
                        borderRadius:5, marginTop:20, alignSelf:'center'}}>  
                        {
                            (this.state.Loan) &&
                        
                        <Dropdown style={{marginLeft:10}}
                                                dropdownOffset={{top:0}}
                                                dropdownMargins={{min:16, max:16} }
                                                placeholder={"Select type"}

                        data={this.state.Loan}
                        onChangeText={(loan) => this.setState({ loan })}
                        value={this.state.loan}
                        inputContainerStyle={{ borderBottomColor: 'transparent' }} 

                        />
                        }
                          <Text style={styles.floatingText1}>
                Loan Type
                            </Text>
                    </View>

                        <View style={styles.View3}>
                            <TouchableOpacity onPress={() => { this.validate() }}>
                                <Text style={styles.SignText}>Submit</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{height:20}}/>

                    </View>
                    
                </KeyboardAwareScrollView>
              </ScrollView>
            </SafeAreaView>
        );
    }
}


export default (LoanEnquiry)