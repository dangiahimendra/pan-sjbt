import { StyleSheet,Dimensions } from 'react-native';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
export const COLOR = {

    BLUE: "#1e244a", // email box color
    RED: "#ea4335",
    YELLOW: "#fff673",
    LIGHTBLUE:"#0096da",
    GREEN:"#58d263"

};

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'#ffffff'

       
      },
      eventcontainer: {
        flex: 1,
        backgroundColor:'#ffffff',
        justifyContent:'center',

      // marginLeft:10,
      // marginRight:10
      },
      cardView: {
        width: width-20,  
        height: height / 2 - 100 ,
        marginTop:30
      },
      eventImgSize: {
        minHeight: 120,
        width:width - 20
      },
      inputView: {
        width : width-20, 
       
        justifyContent:'center',
        height:60,
        //alignItems: 'center',
       
        borderWidth:1,
        paddingLeft:10,
        marginVertical:10
 
    },
    inputView1: {
      width : width-20, 
      justifyContent:'center',
      minHeight:100,
      //alignItems: 'center',
      borderWidth:1,
      paddingLeft:10,
      marginVertical:10

  },

  textInputContainer: {
    // position: "absolute",
    alignSelf: "center",
    justifyContent:'center',
    top: 20,
    backgroundColor: "transparent",
    height: (width * 0.15) + 20,
    width: width / 1.1,
},

  textInputStyle: {
    position: "absolute",
    top: 10,
    left:0,
    height: (width * 0.15),
    width: width / 1.1,
    borderColor: 'rgba(204,204,204,1)',
    borderWidth: 1,
    borderRadius: 5,
    padding: 10,
    fontSize: 18,
},

 dropDownStyle: {
  position: "absolute",
  top: 10,
  left:5,
  height: (width * 0.15),
  width: width / 1.1,
  borderColor: 'rgba(204,204,204,1)',
  borderWidth: 1,
  borderRadius: 5,
  padding: 10,
  fontSize: 18,
},

floatingText: {
    position: "absolute",
     backgroundColor: "white",
    color: "#012b72",
    fontWeight: "normal",
    fontSize: 16,
    fontWeight:'bold',
    height: 20,
    top: 0,
    left: 10,
    paddingLeft: 3,
    paddingRight: 3,
},
floatingText1: {
  position: "absolute",
   backgroundColor: "white",
  color: "#012b72",
  fontWeight: "normal",
  fontSize: 16,
  fontWeight:'bold',
  height: 20,
  top: -12,
 
  
  left: 10,
  paddingLeft: 3,
  paddingRight: 3,
},

View3: {
  borderRadius: 15,
  height: 40, alignSelf: 'center', top:5,
 width: width / 2.0, marginTop: 30,
  justifyContent: 'center', backgroundColor: '#00b9f5'
},
SignText: {
  fontSize: 20, color: '#fff',
  textAlign: 'center', 
  backgroundColor: 'transparent'
},
 
   
});