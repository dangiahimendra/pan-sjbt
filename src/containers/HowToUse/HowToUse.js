import * as React from 'react'
import {
    StyleSheet,
    Text,
    Dimensions,
    View,
    Image, Platform,
    AsyncStorage, ImageBackground
, ScrollView} from 'react-native';
import styles, { COLOR } from './styles';
import { Actions } from 'react-native-router-flux';
import AppHeader from '../../components/AppHeader';

// import console = require('console');

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

class HowToUse extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            user_id:''

        };
    };
    // async componentDidMount() {
    //     // const user_id =  await AsyncStorage.getItem('user_id')
    //     //  console.log(user_id)
     
    //         try {
              
    //             setTimeout(() => {
    //                     Actions.Login();
    //                   //  Actions.letsplay('Letsplay');      
    //             }, 1500);
            
    //     }
    //         catch (error) {
    //             console.log('error' + error)
    //         }
                
    // }

    render() {
        return (
            <View style={styles.container}>
    
             <AppHeader
             goBack={() => this.props.navigation.goBack()}
             addevent={() => this.validate()}
             leftImg={require('./../../components/Images/assets/icn_back.png')}
             headerTitle="How To Use"
           />
           <ScrollView bounces={false} style={{ top: 0, }}>
                <View style={[styles.container, ]}>
                <View style={styles.ViewImage}>
                <Image resizeMode="cover"  style={{ 
                    height: 100,
                   width: 100,
                }}
                    source={require('./../../Images/logom.png')} />
               </View>
                <Text style={styles.Text}>Contact Form Best Practices
                Review the following list of elements shared by successful contact pages to learn about the features and best practices you should remember to include in your own web form. Great contact forms typically... 
                
                Are easy to find so a visitor can quickly get in touch should they need it.
                Explain why someone should contact them, and describe how they can help solve their visitors' problems.
                Include an email and phone number so visitors can quickly find the right information.
                Include a short form using fields that'll help the business understand who's contacting them.
                Include a call-to-action to provide visitors with another option if they choose not to complete the form.
                Showcase the company's thought leadership, whether that's by including a list of recent blog posts or articles about the company in the press.
                Link to active social media accounts like Twitter, Facebook, Instagram and LinkedIn to give visitors a way to engage with the business.
                Redirect to a thank you page that explains when and how you'll be contacting them and links to helpful content and resources.
                Are creative and memorable so visitors associate contacting your brand with a positive or funny memory.
                Show off what your brand does so visitors and potential customers can get a sense of the work you do before they even get in touch.
                Avoid unnecessary fields and words so your page remains as straightforward and simple as possible — no fluff!
                Now that we've gone over best practices, let's review examples of some of the most effective contact pages on the Internet.
                
                Time to get inspired.
                 </Text>                        
                </View>
            </ScrollView>
            </View>
        );
    }
}
export default HowToUse;

