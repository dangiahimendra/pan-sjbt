import { StyleSheet, Dimensions } from 'react-native';
// import { Colors } from '../../Theme';
var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
export const COLOR = {

    RED: "#b71221",
    BLUE: '#ff6f61',
    BLACK: "#ffffff",
    WHITE: "#fff",

};

export default StyleSheet.create({
    container: {
                flex: 1,
                // justifyContent: 'center',
                // alignItems: 'center'
    },
    Images:{
        justifyContent: 'center',
        alignItems: 'center',
    },
    ViewImage: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 15,
        flex:1
    },
    Text:{ fontSize: 15, color: '#000', textAlign:'center', marginTop:20  }
   
});
