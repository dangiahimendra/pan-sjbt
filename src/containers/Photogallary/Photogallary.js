import * as React from 'react'
import {
    StyleSheet,
    Text,
    Dimensions,
    View,
    Image, Platform,
    TouchableOpacity,
    AsyncStorage, ImageBackground, ScrollView
} from 'react-native';
import styles, { COLOR } from './styles';
import AppHeader from '../../components/AppHeader';
import Swiper from "react-native-custom-swiper";



var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height

class Photogallary extends React.Component {

    constructor(props) {
        super(props);
     
        this.state = {
          position: 0,
          imageArr:[
              {image:require('./../../Images/logom.png'),title:"image1"},
              {image:require('./../../Images/logom.png'),title:"image2"},
              {image:require('./../../Images/logom.png'),title:"image3"}
          ],
          interval: null,
          currentIndex: 0,
           imageArray:[]
        };
      }
     
      componentWillMount() {
       
        this.photoDec()
      }
     
     
      async photoDec() {
        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/gallery', {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
           
        }).then((res) => res.json())
            .then(res => {
                console.log(res)
                if (res.result === "true" || res.result === true) {
                    console.log('gallery image',res.data)
                   
                    this.setState({
                        imageArray:res.data,
                    });
                    console.log('gallery image1',this.state.imageArray)

                } else {
                    alert("error")

                }
            })
            .catch((e) => {
                console.log(e)
            });

    };
   
    screenChange = index => {
        console.log("index when change :=> \n", index);
        this.setState({ currentIndex: index });
    };
    renderSwipeItem = item => {
        return (
            <View style={{flex:1,justifyContent:'center'}}>
                <Image style={{ height: 340, width: width }} source={item.image}  resizeMode="contain" />
           <Text style={{ textAlign:'center',color:'black' }}>{item.title}</Text>
           <Text style={{ textAlign:'center',color:'black',marginTop:10 }}>{item.description}</Text>

            </View>
        );
    };

    render() {
        console.log('gallery image1',this.state.imageArray)
        return (
            <View style={styles.container}>
    
             <AppHeader
             goBack={() => this.props.navigation.goBack()}
             addevent={() => this.validate()}
             leftImg={require('./../../components/Images/assets/icn_back.png')}
             headerTitle="Photo Gallary"
           />
           <ScrollView bounces={false} style={{ top: 0, }}>
<View style={{flex:1,justifyContent:'center'}}>
    
           <Swiper
     style={{ flex: 1,justifyContent:'center',textAlign:'center' }}
     currentSelectIndex={0}
     swipeData={this.state.imageArr}
     renderSwipeItem={this.renderSwipeItem}
     onScreenChange={this.screenChange}
 />
 </View>
             </ScrollView>
            </View>
        );
    }
}
export default Photogallary;

