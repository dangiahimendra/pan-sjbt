import * as React from 'react'
import {
    StyleSheet,
    Text,
    Dimensions,
    View,
    Image,
    CheckBox,
    AsyncStorage, ImageBackground, TextInput, TouchableOpacity, SafeAreaView, AlertIOS, ToastAndroid, ScrollView, FlatList, Platform
} from 'react-native';


import styles, { COLOR } from './styles';
import { Actions } from 'react-native-router-flux';
import AppHeader from '../../components/AppHeader'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import ImagePicker from 'react-native-image-picker';

var token = ""

class ITR extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            vanue: props.cityname,
            lat: props.lat,
            long: props.lng,
            start_date: '',
            end_date: '',
            title: '',
            description: '',
            adhar_front_image: null,
            adhar_front_image_path: '',
            adhar_back_image: null,
            adhar_back_image_path: '',
            Bpassbook_image: null,
            Bpassbook_image_path: '',
            pancard_image: null,
            pancard_image_path: '',
            name: '',
            email: '',
            contact: '',
            id: '',
            formsMulti: [],
            count: '',
            uploadimg: false,
            addf: false,
            termsAccepted: false,
            checked:false,
            indexcount:null
        };
    };

    openSearchModal() {
        Actions.SearchPlaces("AddEvent")
    }
    componentWillMount() {

        this.getData()
        this.addForms();
    }
    handleCheckBox = () =>{
         this.setState({ termsAccepted: !this.state.termsAccepted })
         console.log('Response = ', this.state.termsAccepted);
}
    selectPhotoTapped(uploadType, checkValue, index) {
        ImagePicker.showImagePicker((response) => {
            console.log('Response = ', response);
            if (response.didCancel) {
                console.log('User cancelled photo picker');
            }
            else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            }
            else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            }
            else {
                let source = { uri: response.uri };
                console.log('source', source)
                console.log('source', response.uri)
                this.uploadImage(response.uri, uploadType, checkValue, index)


            }
        });
    }

    validate = () => {
        if(this.state.formsMulti.length!=0){

        console.log('length', this.state.formsMulti.length)
        const count=this.state.formsMulti.length-1
        console.log('length', count)


       
       
        
        
        if (this.state.formsMulti[count].mobile == "" || this.state.formsMulti[count].mobile == null || this.state.formsMulti[count].mobile == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter contact'); },
                android: () => { ToastAndroid.show('Please enter contact', ToastAndroid.SHORT); }
            })();
            return false;
        }
        // const reg = /^[0]?[789]\d{9}$/;
        // if (reg.test(this.state.formsMulti[count].mobile) === false ) {
        //     Platform.select({
        //         ios: () => { AlertIOS.alert('Please enter valid contact'); },
        //         android: () => { ToastAndroid.show('Please enter valid contact', ToastAndroid.SHORT); }
        //     })();
        //     return false;
        // }
        if (this.state.formsMulti[count].email == "" || this.state.formsMulti[count].email == null ||this.state.formsMulti[count].email == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter email'); },
                android: () => { ToastAndroid.show('Please enter email', ToastAndroid.SHORT); }
            })();
            return false;
        }
        let rege = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (rege.test(this.state.formsMulti[count].email) === false) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter valid email'); },
                android: () => { ToastAndroid.show('Please enter valid email', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.formsMulti[count].aadhar_front == "" || this.state.formsMulti[count].aadhar_front == null || this.state.formsMulti[count].aadhar_front == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload Aadhar Card (front)'); },
                android: () => { ToastAndroid.show('Please upload Aadhar Card (front)', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.formsMulti[count].pan_card == "" || this.state.formsMulti[count].pan_card == null || this.state.formsMulti[count].pan_card== undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload PAN card'); },
                android: () => { ToastAndroid.show('Please upload PAN card', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.formsMulti[count].bank_passbook == "" || this.state.formsMulti[count].bank_passbook == null || this.state.formsMulti[count].bank_passbook == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload Bank Pass Book'); },
                android: () => { ToastAndroid.show('Please upload Bank Pass Book', ToastAndroid.SHORT); }
            })();
            return false;
        }

        else {
            this.addForms2()
        }
    }
    }
    validate_next = () => {
        if(this.state.formsMulti.length!=0){

        console.log('length', this.state.formsMulti.length)
        const count=this.state.formsMulti.length-1
        console.log('length', count)


       
       
        
        
        if (this.state.formsMulti[count].mobile == "" || this.state.formsMulti[count].mobile == null || this.state.formsMulti[count].mobile == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter contact'); },
                android: () => { ToastAndroid.show('Please enter contact', ToastAndroid.SHORT); }
            })();
            return false;
        }
        // const reg = /^[0]?[789]\d{9}$/;
        // if (reg.test(this.state.formsMulti[count].mobile) === false ) {
        //     Platform.select({
        //         ios: () => { AlertIOS.alert('Please enter valid contact'); },
        //         android: () => { ToastAndroid.show('Please enter valid contact', ToastAndroid.SHORT); }
        //     })();
        //     return false;
        // }
        if (this.state.formsMulti[count].email == "" || this.state.formsMulti[count].email == null ||this.state.formsMulti[count].email == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter email'); },
                android: () => { ToastAndroid.show('Please enter email', ToastAndroid.SHORT); }
            })();
            return false;
        }
        let rege = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (rege.test(this.state.formsMulti[count].email) === false) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please enter valid email'); },
                android: () => { ToastAndroid.show('Please enter valid email', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.formsMulti[count].aadhar_front == "" || this.state.formsMulti[count].aadhar_front == null || this.state.formsMulti[count].aadhar_front == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload Aadhar Card (front)'); },
                android: () => { ToastAndroid.show('Please upload Aadhar Card (front)', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.formsMulti[count].pan_card == "" || this.state.formsMulti[count].pan_card == null || this.state.formsMulti[count].pan_card== undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload PAN card'); },
                android: () => { ToastAndroid.show('Please upload PAN card', ToastAndroid.SHORT); }
            })();
            return false;
        }
        if (this.state.formsMulti[count].bank_passbook == "" || this.state.formsMulti[count].bank_passbook == null || this.state.formsMulti[count].bank_passbook == undefined) {
            Platform.select({
                ios: () => { AlertIOS.alert('Please upload Bank Pass Book'); },
                android: () => { ToastAndroid.show('Please upload Bank Pass Book', ToastAndroid.SHORT); }
            })();
            return false;
        }

        else {
            this.termaccept()
        }
    }
    }
    async uploadImage(image, utype, cvalue, index) {
        console.log('imagef', image)

        var file = {
            uri: image,
            type: 'image/png',
            name: 'profile_image'
        };
        var data = new FormData();
        data.append('file', file);


        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/upload_documents?type=' + utype, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body: data
        }).then((res) => res.json())
            .then(res => {
                console.log('', res)
                if (cvalue == 'aadhar_f') {
                    this.state.formsMulti[index].aadhar_front = res.data
                    this.setState({
                        uploadimg: true
                    });
                    console.log('adharf', this.state.formsMulti[index].aadhar_front)
                }
                if (cvalue == 'aadhar_b') {
                    this.state.formsMulti[index].aadhar_back = res.data
                    this.setState({
                        uploadimg: true
                    });
                    console.log('adharb', this.state.formsMulti[index].aadhar_back)

                }
                if (cvalue == 'pancard') {
                    this.state.formsMulti[index].pan_card = res.data
                    this.setState({
                        uploadimg: true
                    });
                    console.log('pancard', this.state.formsMulti[index].pan_card)

                }
                if (cvalue == 'bankpassbook') {
                    this.state.formsMulti[index].bank_passbook = res.data
                    this.setState({
                        uploadimg: true
                    });
                    console.log('bankp', this.state.formsMulti[index].bank_passbook)

                }
                if (cvalue == 'other_f') {
                    this.state.formsMulti[index].others_front = res.data
                    this.setState({
                        uploadimg: true
                    });
                    console.log('other_f', this.state.formsMulti[index].others_front)

                }
                if (cvalue == 'other_b') {
                    this.state.formsMulti[index].others_back = res.data
                    this.setState({
                        uploadimg: true
                    });
                    console.log('other_b', this.state.formsMulti[index].others_back)

                }
                Platform.select({
                    ios: () => { AlertIOS.alert('Upload Successful'); },
                    android: () => { ToastAndroid.show('Upload Successful', ToastAndroid.SHORT); }
                })();

            })
            .catch((e) => {
                console.log(e)
                Platform.select({
                    ios: () => { AlertIOS.alert('Upload Failed'); },
                    android: () => { ToastAndroid.show('Upload Failed', ToastAndroid.SHORT); }
                })();
            });

    };

    addForms() {

        let formData = {
            "aadhar_front": "",
            "aadhar_back": "",
            "pan_card": "",
            "bank_passbook": "",
            "others_front": "",
            "others_back": "",
            "name": "",
            "email": "",
            "mobile": ""
        }

        this.state.formsMulti.push(formData);


        console.log('datalist', this.state.formsMulti)
    }
    addForms2() {

        let formData = {
            "aadhar_front": "",
            "aadhar_back": "",
            "pan_card": "",
            "bank_passbook": "",
            "others_front": "",
            "others_back": "",
            "name": "",
            "email": "",
            "mobile": ""
        }

        this.state.formsMulti.push(formData);

        Platform.select({
            ios: () => { AlertIOS.alert('New form created'); },
            android: () => { ToastAndroid.show('New form created', ToastAndroid.SHORT); }
        })();
        this.setState({
            addf: true
        });
        console.log('datalist', this.state.formsMulti)
    }
    AddEvent() {
        const params = { vanue, lat, long, start_date, end_date, title, description } = this.state
        this.props.AddeventApi(params);
        Actions.Home()
    }
    fun(index) {
        var checked = this.state.formsMulti;
        var values = checked.indexOf(index)
        checked.splice(values, 1);
        this.setState({formsMulti: checked});
        console.log('remove',this.state.formsMulti)
      }

    getData = async () => {
        try {
            const value = await AsyncStorage.getItem('@storage_Key')
            if (value !== null) {
                // value previously stored
                console.log('aid', value)
                this.setState({
                    id: value
                });
            }
        } catch (e) {
            // error reading value
            console.log('error', e)
        }
    }
    async applyForITR() {
        console.log(this.state.id, this.state.formsMulti.name)
        fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/applyForITR', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "agent_id": this.state.id,
                "comment": "Demo demo demo demo",
                "amount": "1",
                "documents": this.state.formsMulti
            })
        }).then((res) => res.json())
            .then(res => {
                console.log(res)
                if (res.result === "true" || res.result === true) {
                    Platform.select({
                        ios: () => { AlertIOS.alert(res.msg); },
                        android: () => { ToastAndroid.show(res.msg, ToastAndroid.SHORT); }
                    })();
                    this.props.navigation.navigate("Paymentweb")

                } else {
                    alert("error")

                }
            })
            .catch((e) => {
                console.log(e)
            });

    };
    termaccept(){
        if(this.state.checked){
            this.props.navigation.navigate("Paymentitr", { data: this.state.formsMulti })
            this.setState({
                checked: false
            });
        }else{
            Platform.select({
                ios: () => { AlertIOS.alert('Please accept terms and conditions.'); },
                android: () => { ToastAndroid.show('Please accept terms and conditions.', ToastAndroid.SHORT); }
            })(); 
        }
    }

    render() {
        console.log('uploadimg', this.state.uploadimg)
        console.log('addf', this.state.addf)
        console.log('Response = ', this.state.checked)
        return (
            <SafeAreaView style={styles.container}>
                <AppHeader
                    goBack={() => this.props.navigation.goBack()}
                    addevent={() => this.validate()}
                    leftImg={require('./../../components/Images/assets/icn_back.png')}
                    // rightImg={require('./../../components/Images/checked.png')}
                    headerTitle="ITR"
                />

                <ScrollView bounces={false} style={{ top: 0, }}>
                    <FlatList
                        data={this.state.formsMulti}
                        extraData={this.state}
                        renderItem={({ item, index }) => (
                            <KeyboardAwareScrollView
                                style={{ backgroundColor: 'transparent' }}
                                resetScrollToCoords={{ x: 0, y: 0 }}
                                contentContainerStyle={{ flex: 1 }}
                                scrollEnabled={true}
                                bounces={false}
                            >
                                <View style={{ flex: 1, borderWidth: 1, borderColor: '#000000', margin: 6 }}>
                                <View style={{alignSelf: 'flex-end',padding:6}}>
                                            <TouchableOpacity style={styles.icon} onPress={() => this.fun( index)}>
                                                                        <Image
                                                                            resizeMode="center" style={{  tintColor: 'black', height: 20, width: 20 }}
                                                                            source={require('./../../Images/cancel1.png')} />
                                                                    </TouchableOpacity>
                                                                    </View>
                                  
                                    <Text style={{ color: 'black', justifyContent: 'center', alignItems: 'center', textAlign: 'center', marginTop: 5 }}> FORM {index + 1}</Text>

                                    {/* <View style={styles.textInputContainer} >

                                        <TextInput style={styles.textInputStyle}
                                            placeholder={'Enter Your Name'}
                                            placeholderTextColor={'gray'}
                                            returnKeyType={'default'}
                                            autoCapitalize={'none'}
                                            underlineColorAndroid={'transparent'}
                                            onChangeText={(name) => { this.state.formsMulti[index].name = name }}
                                            editable={this.state.TextInputDisableStatus}
                                            value={this.state.formsMulti.name}
                                        />
                                        <Text style={styles.floatingText}>
                                            Enter Name
                            </Text>
                                    </View> */}



                                    <View style={styles.textInputContainer} >

                                        <TextInput style={styles.textInputStyle}
                                            placeholder={'Enter Your Contact'}
                                            placeholderTextColor={'gray'}
                                            returnKeyType={'default'}
                                            autoCapitalize={'none'}
                                            maxLength={10}
                                            keyboardType='phone-pad'
                                            underlineColorAndroid={'transparent'}
                                            onChangeText={(mobile) => { this.state.formsMulti[index].mobile = mobile }}
                                            editable={this.state.TextInputDisableStatus}
                                            value={this.state.formsMulti.mobile}
                                        />
                                        <Text style={styles.floatingText}>
                                            Enter Contact*
                                    </Text>

                                    </View>
                                    <View style={styles.textInputContainer} >

                                        <TextInput style={styles.textInputStyle}
                                            placeholder={'Enter Your Email'}
                                            placeholderTextColor={'gray'}
                                            returnKeyType={'default'}
                                            autoCapitalize={'none'}
                                            underlineColorAndroid={'transparent'}
                                            onChangeText={(email) => { this.state.formsMulti[index].email = email }}
                                            editable={this.state.TextInputDisableStatus}
                                            value={this.state.formsMulti.email}
                                        />
                                        <Text style={styles.floatingText}>
                                            Enter Email*
                            </Text>
                                    </View>

                                    <View style={styles.imageContainer} >

                                        <Text style={styles.floatingText1}>
                                            Upload Your AadharCard*
                                    </Text>

                                        <View style={{ flexDirection: 'row' }}>

                                            <View style={{ height: 15, width: 100, justifyContent: 'center', marginTop: "2%", position: 'absolute', left: 5 }} >


                                                {
                                                    this.state.formsMulti[index].aadhar_front
                                                        ? <TouchableOpacity onPress={() => this.selectPhotoTapped("aadhar_card", "aadhar_f", index)}>
                                                            <Image source={{ uri: this.state.formsMulti[index].aadhar_front }} style={{ width: 100, height: 50, borderRadius: 2 }} />
                                                        </TouchableOpacity>
                                                        : <TouchableOpacity style={styles.icon} onPress={() => this.selectPhotoTapped("aadhar_card", "aadhar_f", index)}>
                                                            <Image
                                                                resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                                // source={{ uri: this.state.sendImageUrl }} />
                                                                source={require('./../../Images/camra.png')} />
                                                        </TouchableOpacity>
                                                }


                                            </View>
                                            <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop: "8%", position: 'absolute', left: 37 }} >

                                                <Text style={{ width: 60, color: 'black', }}>Front</Text>
                                            </View>

                                            <View style={{ height: 15, width: 100, justifyContent: 'center', marginTop: "2%", position: 'absolute', right: 5 }} >
                                                {
                                                    this.state.formsMulti[index].aadhar_back
                                                        ? <TouchableOpacity onPress={() => this.selectPhotoTapped("aadhar_card", "aadhar_b", index)}>
                                                            <Image source={{ uri: this.state.formsMulti[index].aadhar_back }} style={{ width: 100, height: 50, borderRadius: 2 }} />
                                                        </TouchableOpacity>
                                                        : <TouchableOpacity style={styles.icon} onPress={() => this.selectPhotoTapped("aadhar_card", "aadhar_b", index)}>
                                                            <Image
                                                                resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                                // source={{ uri: this.state.sendImageUrl }} />
                                                                source={require('./../../Images/camra.png')} />
                                                        </TouchableOpacity>
                                                }
                                            </View>
                                            <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop: "8%", position: 'absolute', right: 90 }} >

                                                <Text style={{ width: 100, color: 'black', }}>Back [optional]</Text>
                                            </View>

                                        </View>

                                    </View>

                                    <View style={styles.imageContainer2} >

                                        <Text style={styles.floatingText1}>
                                            Upload Your PAN Card*
                                </Text>
                                        <View style={{ height: 15, width: 10, justifyContent: 'center', alignSelf: 'center', marginTop: "7%", alignItems: 'center' }} >
                                            {
                                                this.state.formsMulti[index].pan_card
                                                    ? <TouchableOpacity onPress={() => this.selectPhotoTapped("pan_card", "pancard", index)}>
                                                        <Image source={{ uri: this.state.formsMulti[index].pan_card }} style={{ width: 100, height: 50, borderRadius: 2, marginBottom: 6 }} />
                                                    </TouchableOpacity>
                                                    : <TouchableOpacity style={styles.icon} onPress={() => this.selectPhotoTapped("pan_card", "pancard", index)}>
                                                        <Image
                                                            resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                            // source={{ uri: this.state.sendImageUrl }} />
                                                            source={require('./../../Images/camra.png')} />
                                                    </TouchableOpacity>
                                            }
                                        </View>

                                    </View>

                                    <View style={styles.imageContainer3} >

                                        <Text style={styles.floatingText1}>
                                            Upload Your Bank PassBook*
                                </Text>
                                        <View style={{ height: 15, width: 10, justifyContent: 'center', alignSelf: 'center', marginTop: "7%", alignItems: 'center' }} >
                                            {
                                                this.state.formsMulti[index].bank_passbook
                                                    ? <TouchableOpacity onPress={() => this.selectPhotoTapped("bank_passbook", "bankpassbook", index)}>
                                                        <Image source={{ uri: this.state.formsMulti[index].bank_passbook }} style={{ width: 100, height: 50, borderRadius: 2, marginBottom: 6 }} />
                                                    </TouchableOpacity>
                                                    : <TouchableOpacity style={styles.icon} onPress={() => this.selectPhotoTapped("bank_passbook", "bankpassbook", index)}>
                                                        <Image
                                                            resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                            // source={{ uri: this.state.sendImageUrl }} />
                                                            source={require('./../../Images/camra.png')} />
                                                    </TouchableOpacity>
                                            }
                                        </View>
                                    </View>

                                    <View style={styles.imageContainer3} >

                                        <Text style={styles.floatingText1}>
                                            Upload Other Documents [optional]
                                </Text>
                                        <View style={{ flexDirection: 'row' }}>

                                            <View style={{ height: 15, width: 100, justifyContent: 'center', marginTop: "2%", position: 'absolute', left: 5 }} >


                                                {
                                                    this.state.formsMulti[index].others_front
                                                        ? <TouchableOpacity onPress={() => this.selectPhotoTapped("others_front", "other_f", index)}>
                                                            <Image source={{ uri: this.state.formsMulti[index].others_front }} style={{ width: 100, height: 50, borderRadius: 2 }} />
                                                        </TouchableOpacity>
                                                        : <TouchableOpacity style={styles.icon} onPress={() => this.selectPhotoTapped("others_front", "other_f", index)}>
                                                            <Image
                                                                resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                                // source={{ uri: this.state.sendImageUrl }} />
                                                                source={require('./../../Images/camra.png')} />
                                                        </TouchableOpacity>
                                                }


                                            </View>
                                            <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop: "8%", position: 'absolute', left: 37 }} >

                                                <Text style={{ width: 60, color: 'black', }}>Front</Text>
                                            </View>

                                            <View style={{ height: 15, width: 100, justifyContent: 'center', marginTop: "2%", position: 'absolute', right: 5 }} >
                                                {
                                                    this.state.formsMulti[index].others_back
                                                        ? <TouchableOpacity onPress={() => this.selectPhotoTapped("others_back", "other_b", index)}>
                                                            <Image source={{ uri: this.state.formsMulti[index].others_back }} style={{ width: 100, height: 50, borderRadius: 2 }} />
                                                        </TouchableOpacity>
                                                        : <TouchableOpacity style={styles.icon} onPress={() => this.selectPhotoTapped("others_back", "other_b", index)}>
                                                            <Image
                                                                resizeMode="center" style={{ alignSelf: 'center', tintColor: 'gray', }}
                                                                // source={{ uri: this.state.sendImageUrl }} />
                                                                source={require('./../../Images/camra.png')} />
                                                        </TouchableOpacity>
                                                }
                                            </View>
                                            <View style={{ height: 15, width: 10, justifyContent: 'center', marginTop: "8%", position: 'absolute', right: 60 }} >

                                                <Text style={{ width: 60, color: 'black', }}>Back</Text>
                                            </View>

                                        </View>

                                    </View>



                                    <View style={{ height: 20 }} />
                                </View>
                            </KeyboardAwareScrollView>
                        )}
                        keyExtractor={(item, index) => index}
                    />
                   
                        <View style={{ flexDirection: 'row' }}>
                            <CheckBox
                                value={this.state.checked}
                                onValueChange={() => this.setState({ checked: !this.state.checked })}
                            />
                            <Text style={{ marginTop: 5 ,color:'black'}}> Accept Terms & Conditions</Text>
                        </View>
                  

                    <View style={styles.View4}>
                        <TouchableOpacity onPress={() => { this.validate() }}>
                            <Text style={styles.SignText}>Add more form</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={styles.View5}>
                        <TouchableOpacity onPress={() => { this.validate_next() }}>
                            <Text style={styles.SignText}>Next</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                {/* <View style={styles.View3}>
                                <TouchableOpacity onPress={() => { this.validate() }}>
                                    <Text style={styles.SignText}>Submit</Text>
                                </TouchableOpacity>
                            </View> */}
            </SafeAreaView>


        );
    }
}


export default (ITR)