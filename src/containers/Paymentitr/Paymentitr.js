import * as React from 'react'
import {
    StyleSheet,
    Text,
    Dimensions,
    View,
    Image,
    AsyncStorage, ImageBackground, TextInput, TouchableOpacity, SafeAreaView, AlertIOS, ToastAndroid, ScrollView, FlatList, Platform
} from 'react-native';

var width = Dimensions.get('window').width; //full width
var height = Dimensions.get('window').height; //full height
import styles, { COLOR } from './styles';
import AppHeader from '../../components/AppHeader'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'



var token = ""

class Paymentitr extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            vanue: props.cityname,
            lat: props.lat,
            long: props.lng,
            start_date: '',
            end_date: '',
            title: '',
            description: '',
            adhar_front_image: null,
            adhar_front_image_path: '',

            adhar_back_image: null,
            adhar_back_image_path: '',

            Bpassbook_image: null,
            Bpassbook_image_path: '',


            pancard_image: null,
            pancard_image_path: '',

            name: '',
            email: '',
            contact: '',
            id: '',
            count: '',
            payment: 107,
            convencingCharge: 15,
            totalpay: '',
            other:0,
            comment:'',
            result:'',
            result1:''
           



        };
    };
componentWillMount(){
    this.totalPay()
    this.getData()
}
componentDidMount(){
   this.totalPay()

}
    openSearchModal() {
        Actions.SearchPlaces("AddEvent")
    }
    totoalPaywithother(){
    var f1 = this.state.totalpay;
   
    var f4 = this.state.other;


     result1=f1+f4
     console.log("totoalPaywithother", result1)
     this.setState({
        totalpay:result1
    })

}
totalPay(){
    var f1 = this.state.payment;
    var f2 = this.state.convencingCharge ;
    var f3 = this.props.data.length ;

   result=f3*f1+f3*f2
     console.log("totalPay",  result)
     this.setState({
         totalpay:result
     })
     console.log("totalPay",  this.state.totalpay)
}
getData = async () => {
    try {
        const value = await AsyncStorage.getItem('@storage_Key')
        if (value !== null) {
            // value previously stored
            console.log('aid', value)
            this.setState({
                id: value
            });
        }
    } catch (e) {
        // error reading value
        console.log('error', e)
    }
}
async applyForITR() {
    console.log('apply itr',this.state.id, this.state.payment,this.props.data.length,this.state.convencingCharge,
        this.state.other,this.state.comment ,this.props.data)
    fetch('http://jokingfriend.com/pan_sjbt/index.php/Appcontroller/applyForITR', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "agent_id"          : this.state.id,
            "form_amount"		: this.state.payment,
            "form_count"	    : this.props.data.length,
            "convencing_charge"	: this.state.convencingCharge,
            "other_amount"	    : this.state.other,
            "comment"		    : this.state.comment,              
             "documents": this.props.data
        })
    }).then((res) => res.json())
        .then(res => {
            console.log(res)
            if (res.result === "true" || res.result === true) {
                // Platform.select({
                //     ios: () => { AlertIOS.alert(res.msg); },
                //     android: () => { ToastAndroid.show(res.msg, ToastAndroid.SHORT); }
                // })();
                this.props.navigation.navigate("Paymentweb")

            } else {
                alert("error")

            }
        })
        .catch((e) => {
            console.log(e)
            // Platform.select({
            //     ios: () => { AlertIOS.alert('Application not sent, please try again.'); },
            //     android: () => { ToastAndroid.show('Application not sent, please try again.', ToastAndroid.SHORT); }
            // })();
        });

};

    render() {
        console.log("Array data", this.props.data.length)
       
        return (
            <SafeAreaView style={styles.container}>
                <AppHeader
                    goBack={() => this.props.navigation.goBack()}
                    addevent={() => this.validate()}
                    leftImg={require('./../../components/Images/assets/icn_back.png')}
                    // rightImg={require('./../../components/Images/checked.png')}
                    headerTitle="Payment"
                />

                <ScrollView style={{ flex: 1, alignContent: 'center' }}>

                <KeyboardAwareScrollView
                                style={{ backgroundColor: 'transparent' }}
                                resetScrollToCoords={{ x: 0, y: 0 }}
                                contentContainerStyle={{ flex: 1 }}
                                scrollEnabled={true}
                                bounces={false}
                            >
                    <Text style={styles.text}>
                        One form charges - {this.state.payment}
                    </Text>
                    <Text style={styles.text}>
                        Convencing/Handling - {this.state.convencingCharge}
                    </Text>
                    <Text style={styles.text}>
                        Total Form - {this.props.data.length}
                    </Text>
                    <View style={styles.textInputContainer} >

                        <TextInput style={styles.textInputStyle}
                            placeholder={'Other'}
                            placeholderTextColor={'gray'}
                            returnKeyType={'default'}
                            autoCapitalize={'none'}
                            keyboardType={'numeric'}
                            underlineColorAndroid={'transparent'}
                            onChangeText={(text) => this.setState({ other:parseInt(text) })}
                            editable={this.state.TextInputDisableStatus}
                            value={this.state.other}
                        />
                        <Text style={styles.floatingText}>
                            Other Charges
</Text>
                    </View>



                    <View style={styles.View4}>
                        <TouchableOpacity onPress={() => { this.totoalPaywithother() }}>
                            <Text style={styles.SignText}>Total</Text>
                        </TouchableOpacity>
                    </View>
                    <Text style={styles.text}>
                        Total payment - {this.state.totalpay}
                    </Text>
                    <View style={styles.textInputContainer} >

                        <TextInput style={styles.textInputStyle}
                            placeholder={'Any Comments'}
                            placeholderTextColor={'gray'}
                            returnKeyType={'default'}
                            autoCapitalize={'none'}
                            multiline = {true}
                 underlineColorAndroid={'transparent'}
                            onChangeText={(comment) => this.setState({ comment })}
                            editable={this.state.TextInputDisableStatus}
                            value={this.state.comment}
                        />
                        <Text style={styles.floatingText}>
                            Any Comments
</Text>
                    </View>
                    <View style={styles.View4}>
                        <TouchableOpacity onPress={() => {this.applyForITR() }}>
                            <Text style={styles.SignText}>Pay</Text>
                        </TouchableOpacity>
                    </View>
                    </KeyboardAwareScrollView>

                </ScrollView>
            </SafeAreaView>


        );
    }
}


export default (Paymentitr)